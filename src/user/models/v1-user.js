import mongoose from 'mongoose';

const UserSchema = new mongoose.Schema(
  {
    userid: { type: String, default: '' },
    username: { type: String, default: '' },
    firstname: { type: String, default: '' },
    lastname: { type: String, default: '' },
    age: { type: Number, default: 1 },
    email: { type: String, default: '' },
    phone: { type: String, default: '' },
    loc: { type: String, default: 'no' },
    gmt: { type: String, default: 'no' },
    wlan: { type: String, default: '' },
    device: { type: String, default: '' },
    androidId: { type: String, default: '' },
    iosId: { type: String, default: '' },
    fcmToken: { type: String, default: '' },
    unique: { type: String, default: '' },
    isVerify: { type: String, default: 'no' },
    interest: { type: String, default: 'no' },
    int_date: { type: Number, default: 1 },
    updated_at: { type: Number, default: 1 },
    reg: { type: Date, default: new Date() },
    read: { type: Boolean, default: true },
    chatHistory: { type: Date },
    chat_str: { type: String, default: '' },
    chat_int: { type: Number, default: 0 },
    chat_at: { type: Date, default: new Date() },
    chat_unix: { type: String, default: `${new Date().getTime()}` },
    appversion: { type: String, default: '13' },
    image_path: { type: String, default: '' },
    active_str: { type: String, default: '' },
    active_int: { type: Number, default: 0 },

    unix_time: { type: String, default: `${new Date().getTime()}` },
    created_at: { type: Date, default: Date.now },

    // Begin Tracker

    all_count_int: { type: Number, default: 1 },
    all_count_str: { type: String, default: '' },

    user_new_count_int: { type: Number, default: 1 },
    user_new_count_str: { type: String, default: '' },

    user_active_count_int: { type: Number, default: 1 },
    user_active_count_str: { type: String, default: '' },

    video_count_int: { type: Number, default: 1 },
    video_count_str: { type: String, default: '' },

    videogrp_count_int: { type: Number, default: 1 },
    videogrp_count_str: { type: String, default: '' },

    tv_count_int: { type: Number, default: 1 },
    tv_count_str: { type: String, default: '' },

    radio_count_int: { type: Number, default: 1 },
    radio_count_str: { type: String, default: '' },

    music_count_int: { type: Number, default: 1 },
    music_count_str: { type: String, default: '' },

    artist_count_int: { type: Number, default: 1 },
    artist_count_str: { type: String, default: '' },

    web_count_int: { type: Number, default: 1 },
    web_count_str: { type: String, default: '' },

    other_count_int: { type: Number, default: 1 },
    other_count_str: { type: String, default: '' },
  },
  {
    collation: 'users',
  }
);

UserSchema.index({ created_at: 1 });
const user = mongoose.model('User', UserSchema);
export default user;
