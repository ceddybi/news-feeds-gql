'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _expressValidation = require('express-validation');

var _expressValidation2 = _interopRequireDefault(_expressValidation);

var _expressJwt = require('express-jwt');

var _expressJwt2 = _interopRequireDefault(_expressJwt);

var _validator = require('../validator');

var _validator2 = _interopRequireDefault(_validator);

var _auth = require('../ctrl/auth.register');

var _auth2 = require('../ctrl/auth.device');

var _auth3 = require('../ctrl/auth.profile');

var _config = require('../../config');

var _config2 = _interopRequireDefault(_config);

var _multer = require('multer');

var _multer2 = _interopRequireDefault(_multer);

var _auth4 = require('../ctrl/auth.omegas');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// var multer  = require('multer');

// import authCtrl from '../ctrl/auth.controller';
const upload = (0, _multer2.default)({
  dest: `/tmp/public`
});

const router = _express2.default.Router(); // eslint-disable-line new-cap

// ==============================NO AUTH===========================
/** Syncing the users */
router.route('/user/refresh').post((0, _expressValidation2.default)(_validator2.default.userRefresh), _auth2.syncRefreshUser);

// ==============================REQUIRE AUTH===========================
/** POST /api/device/sync - Returns okay */
router.route('/device/sync').post((0, _expressJwt2.default)({ secret: _config2.default.jwtSecret }), _auth2.syncDevice);

// ==============================NO AUTH===========================
/** Syncing the device for anonymous users */
router.route('/device/sync/public').post((0, _expressValidation2.default)(_validator2.default.deviceSync), _auth2.syncDevice);

// ==============================NO AUTH===========================
/** Returns token if correct refreshToken and unique */
router.route('/device/token/refresh').post((0, _expressValidation2.default)(_validator2.default.refreshToken), _auth2.tokenRefresh);

// ==============================NO AUTH===========================
router.post('/image/uploader', upload.any(), _auth3.uploadImage);

// ==============================NO AUTH===========================
router.route('/login').post((0, _expressValidation2.default)(_validator2.default.login), _auth.login);

router.route('/login/phone').post((0, _expressValidation2.default)(_validator2.default.loginByPhone), _auth.loginByPhone);

router.route('/register').post((0, _expressValidation2.default)(_validator2.default.register), _auth.register);

/** Check if phone or email exits */
router.route('/check/phone-email').post((0, _expressValidation2.default)(_validator2.default.checkPhoneEmail), _auth.checkNumberAndEmail);

/** Check if username */
router.route('/check/username').post((0, _expressValidation2.default)(_validator2.default.checkUsername), _auth.checkUsername);

/**  NEW PASSWORD RESET FUNCTION
 * Check if user exits by phone number */
router.route('/check/phone').post((0, _expressValidation2.default)(_validator2.default.checkUserByPhone), _auth.checkUserByPhone);

/**
 * Profile picture redirect
 * */
router.route('/dp').get((0, _expressValidation2.default)(_validator2.default.profileImage), _auth3.profileImage);

/** NO AUTH */
// UPdate any property in the info fields
router.route('/update/profile/info').post((0, _expressValidation2.default)(_validator2.default.updateProfile), _auth3.updateProfileInfo);

/** REQUIRE AUTH */
// Change password
router.route('/update/profile/pw').post((0, _expressValidation2.default)(_validator2.default.changePassword), _auth3.changePassword);

// router.route('/devices').get(getAlldevices);

/** Returns regions supported */
router.route('/omega/regions').post(_auth4.getRegions);

exports.default = router;