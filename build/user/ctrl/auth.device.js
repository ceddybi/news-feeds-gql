'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.syncRefreshUser = exports.syncDevice = exports.tokenRefresh = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

/**
 *
 * @param {*} req
 * unique, refreshToken
 * @param {*} res
 * @param {*} next
 * @returns {*} {token} || 401 unauthorized
 *
 * Return { token }, error 401 unauthorized
 */
let tokenRefresh = exports.tokenRefresh = (() => {
  var _ref = _asyncToGenerator(function* (req, res, next) {
    var _req$body = req.body;
    const unique = _req$body.unique,
          refreshToken = _req$body.refreshToken;

    let userDevice;

    try {
      userDevice = yield _userDevice.UserDeviceModel.findOne({
        'info.unique': unique,
        'auth.refreshToken': refreshToken
      }).exec();

      if (userDevice) {
        // Not suspended;
        if (!userDevice.auth.suspended) {
          // refresh the token
          const refreshed = (0, _userDevice.refreshTokenFromOld)(userDevice.auth.token);

          // update device
          userDevice.auth = _extends({}, userDevice.auth, refreshed);

          // Save the device
          userDevice.save(function (e, saved) {
            if (saved) {
              console.log('User device updated');
            } else {
              console.log('Failed to create device');
            }
          });
          return res.json(refreshed);
        }
        throw new Error('User device suspended');
      }

      throw new Error('User device not found');
    } catch (error) {
      console.log(error);

      // User needs to sign in again
      return res.status(401).end();
    }
  });

  return function tokenRefresh(_x, _x2, _x3) {
    return _ref.apply(this, arguments);
  };
})();

let syncDevice = exports.syncDevice = (() => {
  var _ref2 = _asyncToGenerator(function* (req, res, next) {
    let userDevice;
    var _req$body2 = req.body;
    const userId = _req$body2.userId,
          device = _req$body2.device;

    let update;

    try {
      // Check if we have a session
      if (userId) {
        userDevice = yield (0, _userDevice.uniDeviceSync)({
          userDevice: _extends({}, device, { userId })
        });
      } else {
        // Going anonymouse
        userDevice = yield (0, _userDevice.uniDeviceSync)({ userDevice: _extends({}, device) });
      }

      if (userDevice) {
        // Android versions check here
        update = updateRelease({ appversion: device.appversion, typo: userDevice.typo });
        // Else return the normal object
        return res.json({
          update: update || { expired: false },
          device: userDevice,
          status: 200,
          message: 'Success'
        });
      }

      throw new Error('User device not found');
    } catch (error) {
      console.log(error);
      return res.json({ message: error.message, status: 400 });
    }
  });

  return function syncDevice(_x4, _x5, _x6) {
    return _ref2.apply(this, arguments);
  };
})();

/**
 * Method to refresh the user object
 * @param {*} req
 * @param {*} res
 * @param {*} next
 *
 */


let syncRefreshUser = exports.syncRefreshUser = (() => {
  var _ref3 = _asyncToGenerator(function* (req, res, next) {
    let user;
    const userId = req.body.userId;

    let userToreturn;
    try {
      console.log('User refresh');
      if (!userId) {
        throw new Error('User is not authorized');
      }

      user = yield _userProfile.UserProfileModel.findById(userId).exec();

      user.utime = new Date();

      if (!user) {
        throw new Error('User is not authorized');
      }
      // user.updatedAt = new Date;
      userToreturn = (0, _userDataTypes.returnMyUserObject)(user);
      // await user.save();
      yield new Promise(function (resolve, reject) {
        _userProfile.UserProfileModel.update({ _id: user._id }, user, function (err, raw) {
          if (err) {
            return reject(err);
          }
          return resolve(raw);
        });
      });
      console.log(userToreturn);
      return res.json({ status: 200, user: userToreturn });
    } catch (error) {
      console.log(error);
      return res.json({ status: 400 });
    }
  });

  return function syncRefreshUser(_x7, _x8, _x9) {
    return _ref3.apply(this, arguments);
  };
})();

var _jsonwebtoken = require('jsonwebtoken');

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

var _httpStatus = require('http-status');

var _httpStatus2 = _interopRequireDefault(_httpStatus);

var _jwtDecode = require('jwt-decode');

var _jwtDecode2 = _interopRequireDefault(_jwtDecode);

var _lodash = require('lodash');

var _APIError = require('../helpers/APIError');

var _APIError2 = _interopRequireDefault(_APIError);

var _config = require('../../config');

var _config2 = _interopRequireDefault(_config);

var _passordUtils = require('../../utils/passord-utils');

var _userDataTypes = require('../../utils/user-data-types');

var _userProfile = require('../models/user-profile');

var _userDevice = require('../models/user-device');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

const androidRelease = {
  '1.4.1': {
    // name: '1.4.1',
    changelog: `The new version is faster and more reliable than ever, Go to Play store to get the update and find out what's new`,
    m: true,
    expired: true
  }
};

const iosRelease = {};

function updateRelease({ typo, appversion = 'x' }) {
  if (typo === 'android') {
    return androidRelease[appversion];
  }
  return iosRelease[appversion];
}