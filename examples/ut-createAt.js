/* @flow */

import mongoose from 'mongoose';
import { mongoUri } from './src/config';
// import { NewsArtModel } from './src/news/models/news-article';

mongoose.Promise = Promise;

const opts = {
  autoReconnect: true,
  reconnectTries: Number.MAX_VALUE,
  reconnectInterval: 1000,
  // useMongoClient: true,
};

mongoose.connect(mongoUri, opts);

export const { connection } = mongoose;
connection.on('error', e => {
  if (e.message.code === 'ETIMEDOUT') {
    console.log(e);
    mongoose.connect(mongoUri, opts);
  }
  console.log(e);
});
connection.once('open', async () => {
  console.log(`MongoDB successfully connected to ${mongoUri}`);
  // const { NewsArtModel } = require('./src/news/models/news-article');
  await loop();
});

async function loop(starting = new Date()) {
  const { NewsArtModel } = require('./src/news/models/news-article');
  let des;
  let items;
  let last;
  const limit = 500;
  console.log('Starting now');
  const startDate = new Date(starting);
  try {
    des = await NewsArtModel.find({
      createdAt: { $exists: false },
      'ut.dnum': { $lte: startDate.getTime() },
    })
      .limit(limit + 1)
      .sort({ 'ut.dnum': -1 })
      .exec();

    console.log(`got ${des.length}`);
    if (des.length > 1) {
      console.log('Got 500 items');
      last = des[des.length - 1].ut.dnum;
      console.log(`Got last item ${last}`);
      await Promise.all(
        des.map(async de => {
          // If we don't have createdAt
          if (!de.createdAt) {
            console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
            de.createdAt = new Date(de.ut.dnum);
            de.countryCode = 'RW';
            await new Promise((res, rej) =>
              NewsArtModel.update({ _id: de._id }, de, (er, reso) => {
                if (er) {
                  console.log(er);
                  return rej(er);
                }
                return res(reso);
              })
            );
            console.log('Edited');
            return {};
          }
          // console.log(de.createdAt);
          console.log('🙄');
          return {};
        })
      );
      console.log('Completed the batch');
      if (des.length > limit) {
        console.log('Going to a new batch');
        await loop(last);
      } else {
        console.log('Completed');
        return {};
      }
    }
    console.log('Nothing completed');
    return {};
  } catch (error) {
    console.log(error);
  }
}
