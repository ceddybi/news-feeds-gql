const { isEmpty, find, compact } = require('lodash');
const cron = require('cron');

let crons = {};
module.exports = {
  bind: ({ id, cronTime, cb, timeZone = 'Africa/Kigali' }) => {
    //  console.log('Data', data);
    if (cronTime) {
      console.log('Setting => ', id);

      if (crons[id]) {
        console.log('Cleaning ', id);
        crons[id].job.stop();
        delete crons[id];
      }

      crons[id] = {
        id,
        job: new cron.CronJob({
          cronTime,
          onTick() {
            cb();
          },
          start: true,
          timeZone,
        }),
      };
    }
  },

  destroy: () => {
    crons = null;
  },

  delete: id => {
    try {
      console.log('Cleaning ', id);
      crons[id].job.stop();
      delete crons[id];
    } catch (err) {
      console.log(err);
    }
  },
};
