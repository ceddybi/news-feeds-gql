'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ThreadResolver = exports.typePageInfo = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; /**
                                                                                                                                                                                                                                                                   * GRAPH QL THREADS, USING MONGO
                                                                                                                                                                                                                                                                   * LIKE CONNECTION, Heavily relying on createdAt, direction.
                                                                                                                                                                                                                                                                   */


var _graphqlCompose = require('graphql-compose');

var _dataTypes = require('../data-types');

var _graphql = require('graphql');

var _lodash = require('lodash');

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

const typePageInfo = exports.typePageInfo = _graphqlCompose.TypeComposer.create(`
    type pageInfo {
      hasNext: Boolean!
      direction: String
    }
`);

const pageInfoType = _graphqlCompose.TypeComposer.create({
  name: 'pageInfo',
  fields: {
    direction: _graphql.GraphQLString,
    hasNext: _graphql.GraphQLBoolean
  }
});

const ThreadResolver = exports.ThreadResolver = data => {
  const Model = data.Model,
        ModelTC = data.ModelTC,
        itemsFormater = data.itemsFormater,
        extraArgs = data.extraArgs,
        extraQuery = data.extraQuery,
        extraSort = data.extraSort;


  if (extraArgs) {
    pageInfoType.addFields(_extends({}, extraArgs));
  }

  const resolver = new _graphqlCompose.Resolver({
    name: 'thread',
    args: _extends({}, _dataTypes.typeThreadInput, extraArgs),
    type: _graphqlCompose.TypeComposer.create({
      name: `${ModelTC.getTypeName()}Thread`,
      fields: {
        pageInfo: pageInfoType,
        items: [ModelTC]
      }
    }),
    kind: 'query',
    resolve: (() => {
      var _ref = _asyncToGenerator(function* ({ source, args, context, info }) {
        // console.log(args);
        // console.log(data);
        const start = new Date(args.start) || new Date();
        const direction = args.direction || 'old';
        // const { intentId } = args;
        //let limit = args.limit && args.limit > 24 ? args.limit : 24;
        let limit = args.limit || 12;

        let query = {};
        let sort = { createdAt: -1 };

        let pageInfo = {
          hasNext: false,
          direction
        };

        let items = [];

        // We don't want to stress this server
        if (limit > 1000) {
          limit = 1000;
        }

        // Direction here, TIME ELAPSED
        if (direction === 'old') {
          query.createdAt = { $lte: start };
        } else {
          // Greater than the time provided
          query.createdAt = { $gte: start };
        }

        // Apply extra query if present
        if (extraQuery) {
          query = _extends({}, query, extraQuery({ source, args: _extends({}, args, { direction, start }), context, info }));
        }

        // Apply sort to Sorter if present
        if (extraSort) {
          sort = _extends({}, extraSort({ source, args, context, info }));
        }

        try {
          console.log('From threads query ', query);
          console.log('From threads sort', sort);
          // query the model now
          items = yield Model.find(query).sort(sort).limit(limit + 1).exec();

          console.log('items----------------------------------', items.length);
          // update pageInfo
          if (items && !(0, _lodash.isEmpty)(items)) {
            console.log('items----------------------------------', items.length);
            // pageInfo.hasNext = items.length == limit + 1;

            // Remove last items used for detecting next
            if (items.length > limit) {
              pageInfo.hasNext = true;
              items.pop();
            }

            if (itemsFormater) {
              // Do we have a transformer?
              items = yield itemsFormater(items);
            }
          }
        } catch (error) {
          console.log(error.message);
        }

        pageInfo = _extends({}, pageInfo, args);

        console.log('Page info-------------------', pageInfo);
        return { items, pageInfo };
      });

      return function resolve(_x) {
        return _ref.apply(this, arguments);
      };
    })()
  });

  /**
   if (extraArgs) {
    resolver.addArgs({
      ...extraArgs,
    });
  }
   */

  return resolver;
};