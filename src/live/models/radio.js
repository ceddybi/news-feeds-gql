/**
 * Created by vuga on 9/22/17.
 */
import mongoose from 'mongoose';
import composeWithMongoose from 'graphql-compose-mongoose';
import composeWithRelay from 'graphql-compose-relay';
import { Resolver } from 'graphql-compose';
import { GraphQLList, GraphQLString } from 'graphql';
import _ from 'lodash';

const defPrefCountrys = ['RW', 'KE', 'UG', 'TZ'];

const RadioSchema = new mongoose.Schema(
  {
    rid: { type: String, default: '' },
    title: { type: String, default: '' },
    description: { type: String, default: '' },
    meta: { type: String, default: '' },
    typo: { type: String, default: '' },

    // target category
    countryCode: { type: String, default: '' },

    // isWebView
    isWebView: { type: Boolean, default: false },

    veri: { type: Number, default: 1 },
    featured: { type: Number, default: 1 },
    int_date: { type: Number, default: 0 },
    updated_at: { type: Number, default: 0 },
    link: { type: String, default: '' },
    lock: { type: String, default: '' },
    lockCode: { type: String, default: '' },
    youtubeChannel: { type: String, default: '' },
    image_path: { type: String, default: '' },
    created_at: { type: Date, default: Date.now },
    available: { type: Boolean, default: true },
  },

  {
    collection: 'radios',
    timestamps: true,
  }
);

export const RadioModel = mongoose.model('Radio', RadioSchema);
export const LiveRadioTC = composeWithRelay(composeWithMongoose(RadioModel));

const RadioCategorySchema = new mongoose.Schema(
  {
    radios: { type: [RadioSchema] },
    title: { type: String },
    index: { type: Number },
    countryCode: { type: String },
  },
  {
    collection: 'Radiocategory',
  }
);

const RadioCategoryModel = mongoose.model('RadioCategory', RadioCategorySchema);
const LiveRadioCategoryTC = composeWithRelay(composeWithMongoose(RadioCategoryModel));

const resolverByDate = new Resolver({
  name: 'categoryCountry',
  type: [LiveRadioCategoryTC],
  kind: 'query',
  args: {
    countrys: {
      type: [GraphQLString],
    },
  },
  resolve: async ({ source, args, context, info }) => {
    let radios = [];
    const radioCats = [];
    try {
      radios = await RadioModel.find({ available: true })
        .sort({ featured: -1 })
        .lean();

      // Check if we have Preference countries
      if (args.countrys && !_.isEmpty(args.countrys)) {
        args.countrys.map((code, index) => {
          radioCats.push({
            radios: _.filter(radios, { countryCode: code }),
            countryCode: code,
            index,
            title: code, // TODO Add countries name
          });
          return code;
        });
        return radioCats;
      }

      // Return default countries order
      defPrefCountrys.map((code, index) => {
        radioCats.push({
          radios: _.filter(radios, { countryCode: code }),
          countryCode: code,
          index: `${index}`,
          title: code, // TODO Add countries name
        });
        return code;
      });
      return radioCats;
    } catch (error) {
      console.log(error);

      // Return empty array
      return radioCats;
    }
  },
});

LiveRadioTC.addResolver(resolverByDate);
