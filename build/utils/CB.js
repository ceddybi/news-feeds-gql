'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * TO ALL DATA TYPES USING INTENT TYPE
 */
const TYPE_TVS = exports.TYPE_TVS = 'tvs';
const TYPE_RADIOS = exports.TYPE_RADIOS = 'radios';

const TYPE_POSTS = exports.TYPE_POSTS = 'posts';
const TYPE_ARTICLES = exports.TYPE_ARTICLES = 'articles';

/**
 * Internal types
 */
const TYPE_COMMENTS = exports.TYPE_COMMENTS = 'comments';
const TYPE_NICES = exports.TYPE_NICES = 'nices';
const TYPE_VIEWS = exports.TYPE_VIEWS = 'views';