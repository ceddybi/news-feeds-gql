'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _cors = require('cors');

var _cors2 = _interopRequireDefault(_cors);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _expressGraphql = require('express-graphql');

var _expressGraphql2 = _interopRequireDefault(_expressGraphql);

var _graphqlPlaygroundMiddlewareExpress = require('graphql-playground-middleware-express');

var _graphqlPlaygroundMiddlewareExpress2 = _interopRequireDefault(_graphqlPlaygroundMiddlewareExpress);

var _mainPage = require('./mainPage');

var _config = require('./config');

require('./mongooseConnection');

var _rootSchema = require('./root-schema');

var _rootSchema2 = _interopRequireDefault(_rootSchema);

var _rootApi = require('./root-api');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import userProfileRedirect from './ql/user/express-route/user-profile-pic-redirect';

// import cookieParser from 'cookie-parser';
const server = (0, _express2.default)();

// import userAuth from './user/routes';

server.use((0, _cors2.default)());
server.use(_bodyParser2.default.json());
server.use(_bodyParser2.default.urlencoded({ limit: '30mb', extended: true }));
// server.use(cookieParser());

// $FlowFixMe
server.get('/', (req, res) => {
  res.send((0, _mainPage.mainPage)());
});

// Add version 3
(0, _rootApi.addV3)(server);
// User auth here
// server.use('/v3', userAuth);
// server.use('/profile', userProfileRedirect);

server.listen(_config.expressPort, () => {
  console.log(`The server is running at http://localhost:${_config.expressPort}/`);
});

function single() {
  const uri = '/ql/v3';
  server.use(uri, (0, _expressGraphql2.default)(() => ({
    schema: _rootSchema2.default,
    graphiql: true,
    formatError: error => ({
      message: error.message,
      stack: !error.message.match(/autho/i) ? error.stack.split('\n') : null
    })
  })));
  server.get(`${uri}-playground`, (0, _graphqlPlaygroundMiddlewareExpress2.default)({ endpoint: uri }));
}
single();