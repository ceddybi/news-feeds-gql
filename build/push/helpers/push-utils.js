'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.pushNewChatMessage = undefined;

let pushNewChatMessage = exports.pushNewChatMessage = (() => {
  var _ref = _asyncToGenerator(function* ({ userId, userTo, convoId, message }) {
    let usersDevices;
    let devices;
    let userSender;
    let convo;
    // console.log(userObjects);
    // console.log(convo.users);

    const push = {
      title: 'New message: ',
      message: message.slice(0, 100),
      sound: 'vuga_zing',
      click_action: 'APP',
      data: {
        typo: 'chat'
        /** 
        convo: {
          _id: convo._id,
          convoId: convo._id,
          lastChatId: convo.lastChatId,
          isActivated: convo.isActivated,
          createdAt: convo.createdAt,
          updatedAt: convo.updatedAt,
          // users: chatUsers(),
          // users: convo.users,
          // users,
        }, * */
      }
    };

    try {
      convo = yield _convos.ConvoModel.findById(convoId).lean().exec();

      userSender = yield _userProfile.UserProfileModel.findById(userId);
      if (!userSender) {
        throw { message: 'Sender user not found' };
      }

      const chatUsers = function chatUsers() {
        // console.log(users);
        const convoUsers = convo.users;
        return convoUsers.map(function (co) {
          // We only need the sender right? so that app can save this convo
          if (co.userId === userId) {
            co.firstname = userSender.info.firstname;
            co.lastname = userSender.info.lastname;
            return co;
          }
          // receiver here
          return co;
        });
      };
      // console.log('Users', chatUsers());

      // Update push with convo Object
      // push.data.convoId = convo._id;
      push.data.convo = {
        _id: convo._id,
        convoId: convo._id,
        lastChatId: convo.lastChatId,
        isActivated: convo.isActivated,
        createdAt: convo.createdAt,
        updatedAt: convo.updatedAt,
        users: chatUsers()
      };

      const sendersName = `${userSender.info.firstname} ${userSender.info.lastname}`;

      // Setting title as senders name
      push.title = sendersName.slice(0, 100);
      push.data.user = (0, _userDataTypes.returnUser)(userSender);

      // Get userTo devices
      usersDevices = yield (0, _user.getDevices)({ version: '3', extraQuery: { userId: userTo } });
      if (!usersDevices || (0, _lodash.isEmpty)(usersDevices)) {
        throw { message: 'Receivers devices not found or empty' };
      }

      devices = yield (0, _user.sortNChunk)({ devices: usersDevices });
      if (!devices) {
        throw { message: 'Failed to get sorted devices' };
      }

      // Run the loop now
      yield (0, _push.AsyncPush)({ clientType: 'ios', ids: devices.ios, push });
      yield (0, _push.AsyncPush)({ clientType: 'android', ids: devices.android, push });
      return { done: true };
    } catch (error) {
      console.log(error.message);
      return { done: false };
    }
  });

  return function pushNewChatMessage(_x) {
    return _ref.apply(this, arguments);
  };
})();

var _lodash = require('lodash');

var _userProfile = require('../../user/models/user-profile');

var _user = require('../../user/helpers/user.devices');

var _push = require('./push');

var _userDataTypes = require('../../utils/user-data-types');

var _convos = require('../../chat/models/convos');

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }