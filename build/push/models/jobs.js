'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.JobTC = exports.JobModel = undefined;

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _graphqlComposeMongoose = require('graphql-compose-mongoose');

var _graphqlComposeMongoose2 = _interopRequireDefault(_graphqlComposeMongoose);

var _graphqlComposeRelay = require('graphql-compose-relay');

var _graphqlComposeRelay2 = _interopRequireDefault(_graphqlComposeRelay);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const JobSchema = new _mongoose.Schema({
  name: { type: String, default: '' },
  timezone: { type: String, default: '' },
  utime: { type: String, default: Date.now },

  notificationId: { type: _mongoose.Schema.Types.ObjectId, ref: 'Notification' }, // Notification
  cronJobId: { type: _mongoose.Schema.Types.ObjectId, ref: 'CronJob' }, // Notification

  // status
  status: { type: String, default: '' }
}, {
  collection: 'jobs',
  timestamps: true
});

const JobModel = exports.JobModel = _mongoose2.default.model('Job', JobSchema);
const JobTC = exports.JobTC = (0, _graphqlComposeMongoose2.default)(JobModel);