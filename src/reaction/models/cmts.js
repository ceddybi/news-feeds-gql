/**
 * Created by vuga on 9/22/17.
 */
import mongoose from 'mongoose';
import composeWithMongoose from 'graphql-compose-mongoose';
import composeWithRelay from 'graphql-compose-relay';
import { Resolver } from 'graphql-compose';
import { GraphQLList, GraphQLString, GraphQLInt, GraphQLEnumType } from 'graphql';
import _, { isEmpty } from 'lodash';
import { ThreadResolver } from '../../utils/gql-threads';
import { UserProfileModel, UserProfileTC } from '../../user/models/user-profile';
import { putNewsArtVCN, NewsArticleFeedTC, NewsArtModel } from '../../news/models/news-article';
import { TYPE_ARTICLES, TYPE_COMMENTS } from '../../utils/CB';

import { LiveRadioTC } from '../../live/models/radio';
import { LiveTvTC } from '../../live/models/tv';

function getUt() {
  return new Date().getTime();
}
const commentObject = {
  text: { type: String, default: '' },
  attachments: [
    {
      type: { type: String, default: '' },
      url: { type: String, default: '' },
    },
  ],
};

const userObj = {
  userId: { type: String, default: '' },
  firstname: { type: String, default: '' },
  lastname: { type: String, default: '' },
  photo: { type: String, default: '' },
};

const CommentSchema = new mongoose.Schema(
  {
    intentId: { type: String, default: '' }, // Can be anything post, comment, live comment
    parent: { type: String, default: '' }, // no, or id of parent
    depth: { type: Number, default: 0 }, // 0 = parent, 1 sub reply, 2 sub-reply
    nixCount: { type: Number, default: 0 }, // 0 = parent, 1 sub reply, 2 sub-reply
    // User who created this comment
    user: userObj,
    userId: { type: String, default: '' },

    sent: { type: Boolean, default: true },
    read: { type: Boolean, default: false },

    // Inject current comment
    ...commentObject,

    // Inject edits history
    edits: [commentObject],
    utime: { type: Number, default: getUt() },
  },
  {
    collection: 'cmts',
    timestamps: true,
  }
);

const CommentsModel = mongoose.model('Comments', CommentSchema);
export const CommentTC = composeWithRelay(composeWithMongoose(CommentsModel));

// TODO ADD ARTICLE, LIVETV & LIVERADIO to comment Object
CommentTC.addFields({
  article: [NewsArticleFeedTC],
  tv: [LiveTvTC],
  radio: [LiveRadioTC],
});

const resolverByIntentId = new Resolver({
  name: 'byIntentId',
  type: [CommentTC],
  kind: 'query',
  args: {
    /** custom: [{
      field: { type: GraphQLString },
      value: { type: GraphQLString },
    }], // Can be used to get specif comments using special filters * */
    intentId: { type: GraphQLString }, // Required context
    start: { type: GraphQLInt }, // start from this timestamp, will default to current time
    end: { type: GraphQLInt }, // optional field to end results from here
    limit: { type: GraphQLInt }, // limit number of comments i need, default to 20 if query less than 10
    direction: { type: GraphQLString }, // either new or old
  },

  resolve: async ({ source, args, context, info }) => {
    console.log(info);
    let comments = [];
    const query = {};

    const start = args.start || new Date().getTime();
    const end = args.start || new Date().getTime();
    const direction = args.direction || 'old';

    const limit = args.limit && args.limit > 10 ? args.limit : 20;

    /** if(!isEmpty(args.custom)){
      // Put all customs to query object
      args.custom.map(cus => {
        query[cus.field] = cus.value;
      })
    }* */
    // Check direction
    if (direction === 'old') {
      // Less than the time provided
      query.createdAt = { $lt: start };
    } else {
      // Greater than the time provided
      query.createdAt = { $gt: start };
    }

    try {
      comments = await CommentsModel.find({ intentId: args.intentId, ...query })
        .lean()
        .limit(limit)
        .exec();
      return comments;
    } catch (error) {
      console.log(error);
      return null;
    }
  },
});
CommentTC.addResolver(resolverByIntentId);

const resolverGetRepliesCount = new Resolver({
  name: 'getRepliesCount',
  type: GraphQLInt,
  kind: 'query',
  args: {
    commentId: { type: GraphQLString }, // Required context
  },

  resolve: async ({ source, args, context, info }) => {
    // let replys = {count: 0, replies: []};

    try {
      // Getting all comments with parent id as argument
      comments = await CommentsModel.find({ parent: args.commentId })
        .lean()
        .exec();
      // TODO circulate
      return comments.length;
    } catch (error) {
      console.log(error);
      // return default one
      return 0;
    }
  },
});
CommentTC.addResolver(resolverGetRepliesCount);

const resolverCreateReply = new Resolver({
  name: 'createComment',
  type: GraphQLInt,
  kind: 'mutation',
  args: {
    commentId: { type: GraphQLString }, // Required context
  },

  resolve: async ({ source, args, context, info }) => {
    // let replys = {count: 0, replies: []};

    try {
      // Getting all comments with parent id as argument
      comments = await CommentsModel.find({ parent: args.commentId })
        .lean()
        .exec();
      // TODO circulate
      return comments.length;
    } catch (error) {
      console.log(error);
      // return default one
      return 0;
    }
  },
});

CommentTC.addRelation('replies', {
  resolver: CommentTC.getResolver('getRepliesCount'),
  prepareArgs: {
    _id: source => ({ _id: source._id }),
  },
  projection: { _id: true },
});

// lastmessage
CommentTC.addRelation('fulluser', {
  resolver: UserProfileTC.getResolver('viewUser'),
  prepareArgs: {
    userId: source => source.userId,
  },
  projection: { users: true },
});

CommentTC.setResolver(
  'connection',
  CommentTC.getResolver('connection').addFilterArg({
    name: 'byPage',
    type: `input byIntentId {
      intentId: String!
      start: Date!
      limit: Int
      direction: String
    }`,
    query: (rawQuery, value, resolveParams) => { // eslint-disable-line
      console.log(value);
      console.log(rawQuery);
      if (!value.intentId) return;

      const start = value.start || new Date();
      const direction = value.direction || 'old';
      // const limit = args.limit && args.limit > 10 ? args.limit : 20;
      // Check direction
      if (direction === 'old') {
        // Less than the time provided
        rawQuery.createdAt = { $lt: start };
      } else {
        // Greater than the time provided
        rawQuery.createdAt = { $gt: start };
      }
      rawQuery.intentId = value.intentId;
    },
  })
  // /* FOR DEBUG */
  //   .debug()
  // /* OR MORE PRECISELY */
  //   .debugParams()
  //   .debugPayload()
  //   .debugExecTime()
);

// Comments threads, by convoId = intentId
const CommentThreadsResolver = ThreadResolver({
  ModelTC: CommentTC,
  extraArgs: {
    intentId: { type: GraphQLString, isRequired: true },
  },
  extraQuery: ({ source, args, context, info }) => {
    const { intentId } = args;
    return {
      intentId,
    };
  },
  Model: CommentsModel,
});

CommentTC.addResolver(CommentThreadsResolver);

// create Comment
const createComment = new Resolver({
  name: 'createComment',
  type: CommentTC,
  // output: CommentTC,
  kind: 'mutation',
  args: {
    intentType: { type: GraphQLString }, // when post-article or live-tv, live-radio
    intentId: { type: GraphQLString }, // Required context
    userId: { type: GraphQLString }, // Required context
    text: { type: GraphQLString }, // Required context
  },

  resolve: async ({ source, args, context, info }) => {
    // let replys = {count: 0, replies: []};
    const { intentId, userId, text } = args;

    console.log('create new comment ', args);
    let user;
    const intentType = args.intentType || TYPE_ARTICLES;
    let newComment;
    let article;
    let tv;
    let radio;
    try {
      // Getting user
      user = await UserProfileModel.findById(userId).exec();

      // Create the new comment
      newComment = new CommentsModel({
        userId,
        text,
        intentId,
        user: {
          userId,
          firstname: user.info.firstname,
          lastname: user.info.lastname,
        },
      });

      await newComment.save();

      /** UPDATE INTENT COUNTS */
      // TODO other
      if (intentType === TYPE_ARTICLES) {
        console.log('Putting comments count now');
        article = await putNewsArtVCN({ articleId: intentId, userId, type: TYPE_COMMENTS });
        // console.log('Article was ', article);
        newComment.article = article;
      }

      return newComment;
    } catch (error) {
      console.log(error);
      return null;
    }
  },
});

CommentTC.addResolver(createComment);
