'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.connection = undefined;

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _config = require('./config');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_mongoose2.default.Promise = Promise;

const opts = {
  autoReconnect: true,
  reconnectTries: Number.MAX_VALUE,
  reconnectInterval: 1000
  // useMongoClient: true,
};

_mongoose2.default.connect(_config.mongoUri, opts);

const connection = _mongoose2.default.connection;
exports.connection = connection;

connection.on('error', e => {
  if (e.message.code === 'ETIMEDOUT') {
    console.log(e);
    _mongoose2.default.connect(_config.mongoUri, opts);
  }
  console.log(e);
});
connection.once('open', () => {
  console.log(`MongoDB successfully connected to ${_config.mongoUri}`);
});