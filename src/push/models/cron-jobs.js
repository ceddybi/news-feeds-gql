import mongoose, { Schema } from 'mongoose';
import composeWithMongoose from 'graphql-compose-mongoose';
import composeWithRelay from 'graphql-compose-relay';
import { UniData } from '../../utils/data-types';
import { GraphQLString, GraphQLBoolean } from 'graphql';
import { isEmpty } from 'lodash';
import { Resolver } from 'graphql-compose';
import { pushCronJob } from '../ctrl/pushCron';
import { deleteCronDaemon } from '../ctrl/pushCron-manager';

const CronJobSchema = new Schema(
  {
    ...UniData,

    notificationId: { type: Schema.Types.ObjectId, ref: 'Notification' }, // Notification

    // time, cron logics
    timezone: { type: String, default: '' },
    cronexp: { type: String, default: '' },

    // device targets
    target: [String],
  },

  {
    collection: 'cronjobs',
    timestamps: true,
  }
);

export const CronJobModel = mongoose.model('CronJob', CronJobSchema);
export const CronJobTC = composeWithMongoose(CronJobModel);

// Update cronjob or create new one
CronJobTC.addResolver(
  new Resolver({
    name: 'updateCron',
    kind: 'mutation',
    type: CronJobTC,
    args: {
      _id: { type: GraphQLString },
      name: { type: GraphQLString },
      description: { type: GraphQLString },
      available: { type: GraphQLBoolean },
      target: { type: [GraphQLString] },
      notificationId: { type: GraphQLString },
      timezone: { type: GraphQLString },
      cronexp: { type: GraphQLString },
    },
    resolve: async ({ source, args, context, info }) => {
      const { _id, name, description, available, target, notificationId, timezone, cronexp } = args;
      let cronjob, cronjobOld;

      console.log('Availble = ',available);

      try {
        if (_id && !isEmpty(_id)) {
          // We are updating cronjob now

          // Lets find the mf
          cronjobOld = await CronJobModel.findById(_id).exec();

          if (!cronjobOld) {
            throw new Error('Cronjob not found');
          }

          cronjob = await CronJobModel.findByIdAndUpdate(_id, {
            // ...cronjobOld,
            name,
            description,
            available,
            target,
            notificationId,
            timezone,
            cronexp,
          }).exec();

          console.log('updated cronjob', cronjob);

          if (!available) {
            // Delete this cronjob daemon now
            deleteCronDaemon(_id);
          } else {
            // Update the daemon,

            // Compare expressions if not equal call binder now
            if (cronexp !== cronjobOld.cronexp) {
              console.log(`${cronjobOld.cronexp} => ${cronexp}`)  
              pushCronJob({
                _id,
                name,
                description,
                available,
                target,
                notificationId,
                timezone,
                cronexp,
              }); // Update cronjob in the memory
            }
          }

          return cronjob;

          // Todo bind cron daemon
        }
        cronjob = new CronJobModel({
          name,
          description,
          available,
          target,
          notificationId,
          timezone,
          cronexp,
        });

        // Check if available is set to true or false
        if (available) {
          pushCronJob(cronjob); // Update cronjob in the memory
        }

        await cronjob.save();

        return cronjob;
      } catch (error) {
        console.log(error);
        return null;
      }
    },
  })
);
