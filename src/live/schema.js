/* @flow */

// SINGLE SCHEMA ON SERVER
// import { GQC } from 'graphql-compose';

// MULTI SCHEMA MODE IN ONE SERVER
// create new GQC from ComposeStorage
import { ComposeStorage } from 'graphql-compose';
import composeWithRelay from 'graphql-compose-relay';

import { LiveTvTC } from './models/tv';
import { LiveRadioTC } from './models/radio';

const GQC = new ComposeStorage();

composeWithRelay(GQC.rootQuery());

const ViewerTC = GQC.get('Viewer');
GQC.rootQuery().addFields({
  viewer: {
    type: ViewerTC.getType(),
    description: 'Data under client context',
    resolve: () => ({}),
  },
});

const fields = {
  LiveRadios: LiveRadioTC.getResolver('categoryCountry'),
  Radio: LiveRadioTC.get('$findById'),
  LiveTvs: LiveTvTC.getResolver('categoryCountry'),
  Tv: LiveTvTC.get('$findById'),
};

ViewerTC.addFields(fields);

/**
GQC.rootMutation().addFields({
  createProduct: NewsArtTC.get('$createOne'),
  removeProductById: NewsArtTC.get('$removeById'),
});
* */
export default GQC.buildSchema();
