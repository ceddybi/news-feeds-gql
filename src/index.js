/* @flow */

import express from 'express';
import cors from 'cors';
// import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import graphqlHTTP from 'express-graphql';
import expressPlayground from 'graphql-playground-middleware-express';
import { mainPage, addToMainPage } from './mainPage';
import { expressPort, getExampleNames, resolveExamplePath } from './config';
import './mongooseConnection';
import RootSchema from './root-schema';

// import userAuth from './user/routes';
import { addV3 } from './root-api';
// import userProfileRedirect from './ql/user/express-route/user-profile-pic-redirect';

const server = express();
server.use(cors());
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ limit: '30mb', extended: true }));
// server.use(cookieParser());

// $FlowFixMe
server.get('/', (req, res) => {
  res.send(mainPage());
});

// Add version 3
addV3(server);
// User auth here
// server.use('/v3', userAuth);
// server.use('/profile', userProfileRedirect);

server.listen(expressPort, () => {
  console.log(`The server is running at http://localhost:${expressPort}/`);
});

function single() {
  const uri = '/ql/v3';
  server.use(
    uri,
    graphqlHTTP(() => ({
      schema: RootSchema,
      graphiql: true,
      formatError: error => ({
        message: error.message,
        stack: !error.message.match(/autho/i) ? error.stack.split('\n') : null,
      }),
    }))
  );
  server.get(`${uri}-playground`, expressPlayground({ endpoint: uri }));
}
single();
