'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CronJobTC = exports.CronJobModel = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _graphqlComposeMongoose = require('graphql-compose-mongoose');

var _graphqlComposeMongoose2 = _interopRequireDefault(_graphqlComposeMongoose);

var _graphqlComposeRelay = require('graphql-compose-relay');

var _graphqlComposeRelay2 = _interopRequireDefault(_graphqlComposeRelay);

var _dataTypes = require('../../utils/data-types');

var _graphql = require('graphql');

var _lodash = require('lodash');

var _graphqlCompose = require('graphql-compose');

var _pushCron = require('../ctrl/pushCron');

var _pushCronManager = require('../ctrl/pushCron-manager');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

const CronJobSchema = new _mongoose.Schema(_extends({}, _dataTypes.UniData, {

  notificationId: { type: _mongoose.Schema.Types.ObjectId, ref: 'Notification' }, // Notification

  // time, cron logics
  timezone: { type: String, default: '' },
  cronexp: { type: String, default: '' },

  // device targets
  target: [String]
}), {
  collection: 'cronjobs',
  timestamps: true
});

const CronJobModel = exports.CronJobModel = _mongoose2.default.model('CronJob', CronJobSchema);
const CronJobTC = exports.CronJobTC = (0, _graphqlComposeMongoose2.default)(CronJobModel);

// Update cronjob or create new one
CronJobTC.addResolver(new _graphqlCompose.Resolver({
  name: 'updateCron',
  kind: 'mutation',
  type: CronJobTC,
  args: {
    _id: { type: _graphql.GraphQLString },
    name: { type: _graphql.GraphQLString },
    description: { type: _graphql.GraphQLString },
    available: { type: _graphql.GraphQLBoolean },
    target: { type: [_graphql.GraphQLString] },
    notificationId: { type: _graphql.GraphQLString },
    timezone: { type: _graphql.GraphQLString },
    cronexp: { type: _graphql.GraphQLString }
  },
  resolve: (() => {
    var _ref = _asyncToGenerator(function* ({ source, args, context, info }) {
      const _id = args._id,
            name = args.name,
            description = args.description,
            available = args.available,
            target = args.target,
            notificationId = args.notificationId,
            timezone = args.timezone,
            cronexp = args.cronexp;

      let cronjob, cronjobOld;

      console.log('Availble = ', available);

      try {
        if (_id && !(0, _lodash.isEmpty)(_id)) {
          // We are updating cronjob now

          // Lets find the mf
          cronjobOld = yield CronJobModel.findById(_id).exec();

          if (!cronjobOld) {
            throw new Error('Cronjob not found');
          }

          cronjob = yield CronJobModel.findByIdAndUpdate(_id, {
            // ...cronjobOld,
            name,
            description,
            available,
            target,
            notificationId,
            timezone,
            cronexp
          }).exec();

          console.log('updated cronjob', cronjob);

          if (!available) {
            // Delete this cronjob daemon now
            (0, _pushCronManager.deleteCronDaemon)(_id);
          } else {
            // Update the daemon,

            // Compare expressions if not equal call binder now
            if (cronexp !== cronjobOld.cronexp) {
              console.log(`${cronjobOld.cronexp} => ${cronexp}`);
              (0, _pushCron.pushCronJob)({
                _id,
                name,
                description,
                available,
                target,
                notificationId,
                timezone,
                cronexp
              }); // Update cronjob in the memory
            }
          }

          return cronjob;

          // Todo bind cron daemon
        }
        cronjob = new CronJobModel({
          name,
          description,
          available,
          target,
          notificationId,
          timezone,
          cronexp
        });

        // Check if available is set to true or false
        if (available) {
          (0, _pushCron.pushCronJob)(cronjob); // Update cronjob in the memory
        }

        yield cronjob.save();

        return cronjob;
      } catch (error) {
        console.log(error);
        return null;
      }
    });

    return function resolve(_x) {
      return _ref.apply(this, arguments);
    };
  })()
}));