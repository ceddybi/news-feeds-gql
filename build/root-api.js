'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.addV3 = addV3;

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _routes = require('./user/routes');

var _routes2 = _interopRequireDefault(_routes);

var _routes3 = require('./push/routes');

var _routes4 = _interopRequireDefault(_routes3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function addV3(app) {
  const router = _express2.default.Router();
  router.use('/push', _routes4.default);
  router.use('/auth', _routes2.default);

  app.use('/v3', router);
}