"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UserProfileModel = undefined;

var _mongoose = require("mongoose");

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const UserProfileSchema = new _mongoose2.default.Schema({
  fcmToken: { type: String, default: "" },
  unix_time: { type: String, default: "" + new Date().getTime() }
}, {
  collection: 'users'
  // timestamps: true,
});

const UserProfileModel = exports.UserProfileModel = _mongoose2.default.model('User', UserProfileSchema);