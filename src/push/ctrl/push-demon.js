import { CronJobModel } from '../models/cron-jobs';
import later from 'later';
import { PushModel } from '../models/push-model';
import { asyncForEach } from '../../utils/async-utils';

const cronJobsQueue = [];

const futureNumber = 2;
const timeoutCreator = 5000;

function createOccurences(cronexp = '0 20 * * 1-5') {
  const sched = later.parse.cron(cronexp);
  const occurrences = later.schedule(sched).next(futureNumber);
  return occurrences;
}

/**
 * Create future jobs if they don't exits
 *
 * @param {*} occurrences [] or dates
 * @param {*} cronJobId the cronjob doing this shit
 */
async function createJobs(occurrences = [], cronJob) {
  try {
    if (!cronJob) {
      throw new Error('CronJob not found');
    }

    await asyncForEach(occurrences, async element => {
      let job = await PushModel.findOne({ cronJobId: cronJob._id, utime: element }).exec();

      // console.log('Job', job);
      if (!job) {
        // creating new job
        job = new PushModel({
          name: `${cronJob.name} - ${new Date(element)}`,
          cronJobId: cronJob._id,
          utime: element,
          notificationId: cronJob.notificationId,
          timezone: cronJob.timezone,
        });
        await job.save();

        /** 
        // Save the job now
        await new Promise((res, rej) => {
         
          job.save((er, saved) => {
            if (saved) {
              return res(saved);
            }
            return rej(er);
          });
        }); * */

        return console.log(`Created = ${new Date(element)}`);
      }
      return console.log(`Already created = ${new Date(element)}`);
    });
  } catch (error) {
    return console.log(error);
  }
}

async function getOccurences(cronJobs) {
  const jobs = [];
  try {
    await asyncForEach(cronJobs, async cj => {
      const occurrences = createOccurences(cj.cronexp);
      jobs.push({ cj, occurrences });
      return { done: true };
    });
    // console.log('Occurences', jobs);
    return jobs;
  } catch (error) {
    console.log(error);
    return jobs;
  }
}

async function cronJoblist() {
  let cronjobs;
  try {
    cronjobs = await CronJobModel.find({ available: true }).exec();

    console.log('Cronjobs BEGIN', cronjobs.length);
    if (!cronjobs) {
      throw new Error('CronJobs null');
    }

    cronjobs = await getOccurences(cronjobs);

    await asyncForEach(cronjobs, async job => {
      await createJobs(job.occurrences, job.cj);
      return { done: true };
    });

    console.log('Cronjobs complete');
    return { done: true };
  } catch (error) {
    return console.log(error);
  }
}

// cronJoblist();
setInterval(() => {
  cronJoblist();
}, timeoutCreator);

async function startCronWorker(params) {
  let cronjobs;
  try {
    cronjobs = await CronJobModel.find({ available: true }).exec();

    console.log('Cronjobs BEGIN', cronjobs.length);
    if (!cronjobs) {
      throw new Error('CronJobs null');
    }
  } catch (error) {
    console.log(error);
    return startCronWorker;
  }
}
