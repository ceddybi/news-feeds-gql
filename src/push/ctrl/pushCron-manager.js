const { isEmpty, find, compact } = require('lodash');
const cron = require('cron');

export function isCronsEnabled() {
  return process.env.CRONS && process.env.CRONS > 0;
}

let crons = {};
module.exports = {
  bind: ({ id, cronTime, cb, timeZone = 'Africa/Kigali' }) => {
    if (!isCronsEnabled()) {
      console.log('Crons are not enabled');
      return;
    }
    //  console.log('Data', data);
    if (cronTime) {
      console.log('Setting => ', id);

      if (crons[id]) {
        console.log('Refreshing ', id);
        crons[id].job.stop();
        delete crons[id];
      }

      crons[id] = {
        id,
        job: new cron.CronJob({
          cronTime,
          onTick() {
            cb();
          },
          start: true,
          timeZone,
        }),
      };
    }
  },

  destroy: () => {
    crons = null;
  },

  deleteCronDaemon: id => {
    if (!isCronsEnabled()) {
      console.log('Crons are not enabled');
      return;
    }
    try {
      console.log('Deleting now ', id);
      crons[id].job.stop();
      delete crons[id];
    } catch (err) {
      console.log(err);
    }
  },
};
