'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.circleDevicesByID = exports.getTargetQuery = exports.sortNChunk = exports.getDevices = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

let getDevices = exports.getDevices = (() => {
  var _ref = _asyncToGenerator(function* ({
    version,
    model,
    devices = [],
    lastId = null,
    count = 0,
    extraQuery
  }) {
    let thouDevices;
    console.log(`Count = ${count}`);
    console.log(`Devices = ${devices.length}`);
    // const sort = { createdAt: -1 };
    let query = { _id: { $lte: lastId } };

    // TODO Switch between versions to set models

    if (!model) {
      // IF model is null assign it now
      switch (version) {
        case '1':
          model = _userOld.UserOldModel;
          break;
        case '2':
          model = _userDevice.UserDeviceModel;
          break;
        case '3':
          model = _userDevice.UserDeviceModel;
          break;
        default:
          model = _userDevice.UserDeviceModel;
      }
    }

    // If it's null delete the ID query
    if (lastId === null) {
      delete query._id;
    }

    // If we have more queries
    if (extraQuery) {
      query = _extends({}, query, extraQuery);
    }

    try {
      thouDevices = yield circleDevicesByID({ lastId, model, version, query });

      count += 1;
      if (thouDevices.data.length > 0) {
        devices = (0, _lodash.concat)(devices, thouDevices.data);
        devices = (0, _lodash.compact)(devices);
        // devices = uniqBy(devices, '_id');

        // Recursive
        if (thouDevices.hasNext) {
          return yield getDevices({
            version,
            model,
            devices,
            lastId: thouDevices.lastId,
            count,
            extraQuery
          });
        }
      }

      console.log(`Version ${version} complete now ${devices.length}`);
      // else stop and return devices
      return devices;
    } catch (error) {
      console.log(error.message);
      return [];
    }
  });

  return function getDevices(_x) {
    return _ref.apply(this, arguments);
  };
})();

let sortNChunk = exports.sortNChunk = (() => {
  var _ref2 = _asyncToGenerator(function* ({ devices }) {
    try {
      // filter Android & iOS devices
      const iosDevices = devices.filter(function (device) {
        return device.typo === 'ios';
      });
      const androidDevices = devices.filter(function (device) {
        return device.typo === 'android';
      });

      console.log(`Got iOS ${(0, _lodash.size)(iosDevices)}`);
      console.log(`Got Android ${(0, _lodash.size)(androidDevices)}`);
      // Ids
      let iosIds = (0, _lodash.map)(iosDevices, 'info.pushtoken');
      iosIds = (0, _lodash.uniqWith)(iosIds, _lodash.isEqual); // remove duplicates
      let androidIds = (0, _lodash.map)(androidDevices, 'info.pushtoken');
      androidIds = (0, _lodash.uniqWith)(androidIds, _lodash.isEqual); // remove duplicates

      // 1000 Chunks
      const iosChunks = (0, _lodash.chunk)(iosIds, 1000);
      const androidChunks = (0, _lodash.chunk)(androidIds, 1000);

      return { ios: iosChunks, android: androidChunks };
    } catch (error) {
      console.log(error.message);
      return null;
    }
  });

  return function sortNChunk(_x2) {
    return _ref2.apply(this, arguments);
  };
})();

let getTargetQuery = exports.getTargetQuery = (() => {
  var _ref3 = _asyncToGenerator(function* (target = '') {
    console.log('TARGET DEVICES', target);

    let to;
    const query = {
      $and: []
    };

    if ((0, _lodash.isEmpty)(target)) {
      console.log('Returning ALL devices');
      // returning null query
      return null;
    }

    // If we have all return now
    if ((0, _lodash.includes)(target, 'all')) {
      console.log('Returning ALL devices');
      return {};
    }

    try {
      to = target;

      to.forEach(function (t) {
        const cccode = t.toLocaleUpperCase();
        const countryData = (0, _lodash.find)(_countryWithTZ.countries, { country_code: cccode });

        let tzs = countryData.timezones;
        tzs = tzs.map(function (tx) {
          return {
            'info.timezone': tx
          };
        });

        query.$and.push({
          $or: [{ 'location.countryCode': cccode }, ...tzs]
        });
      });
      return query;
    } catch (err) {
      console.log(err);
      return query;
    }
  });

  return function getTargetQuery() {
    return _ref3.apply(this, arguments);
  };
})();

/**
 * Circular by ID
 */


let circleDevicesByID = exports.circleDevicesByID = (() => {
  var _ref4 = _asyncToGenerator(function* ({ lastId, query, model }) {
    let last;
    let lastObjectId;

    // Will be default to newer models
    if (!model) {
      model = _userDevice.UserDeviceModel;
    }

    let thouDevices;
    let hasNext = false;
    console.log(`Using last ${lastId}`);
    if (lastId === null) {
      delete query._id; // Remove searches with ID
    }

    try {
      thouDevices = yield model.find(query).sort({ _id: -1 }).limit(deviceFetchLimit + 1).lean().exec();

      if (thouDevices) {
        // Check if we have next
        if (thouDevices.length > deviceFetchLimit) {
          hasNext = true;
        }

        // lastItem = Object.assign({}, thouDevices[thouDevices.length - 1]);
        last = thouDevices.slice(-1).pop();
        lastObjectId = new ObjectId(last._id);
        // console.log('Last object ID', lastObjectId);

        return { lastId: lastObjectId, hasNext, data: thouDevices };
      }

      return { lastId: lastObjectId, hasNext, data: thouDevices };
    } catch (error) {
      console.log(error.message);
      return { lastId: null, hasNext, data: [] };
    }
  });

  return function circleDevicesByID(_x3) {
    return _ref4.apply(this, arguments);
  };
})();

var _userDevice = require('../models/user-device');

var _lodash = require('lodash');

var _countryWithTZ = require('./countryWithTZ');

var _userOld = require('../models/user-old');

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

const ObjectId = _mongoose2.default.Types.ObjectId;


const deviceFetchLimit = 10000;