'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _graphqlCompose = require('graphql-compose');

var _graphqlComposeRelay = require('graphql-compose-relay');

var _graphqlComposeRelay2 = _interopRequireDefault(_graphqlComposeRelay);

var _tv = require('./models/tv');

var _radio = require('./models/radio');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// SINGLE SCHEMA ON SERVER
// import { GQC } from 'graphql-compose';

// MULTI SCHEMA MODE IN ONE SERVER
// create new GQC from ComposeStorage
const GQC = new _graphqlCompose.ComposeStorage();

(0, _graphqlComposeRelay2.default)(GQC.rootQuery());

const ViewerTC = GQC.get('Viewer');
GQC.rootQuery().addFields({
  viewer: {
    type: ViewerTC.getType(),
    description: 'Data under client context',
    resolve: () => ({})
  }
});

const fields = {
  LiveRadios: _radio.LiveRadioTC.getResolver('categoryCountry'),
  Radio: _radio.LiveRadioTC.get('$findById'),
  LiveTvs: _tv.LiveTvTC.getResolver('categoryCountry'),
  Tv: _tv.LiveTvTC.get('$findById')
};

ViewerTC.addFields(fields);

/**
GQC.rootMutation().addFields({
  createProduct: NewsArtTC.get('$createOne'),
  removeProductById: NewsArtTC.get('$removeById'),
});
* */
exports.default = GQC.buildSchema();