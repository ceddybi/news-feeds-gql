/**
 * Created by vuga on 9/22/17.
 */
import jwt from 'jsonwebtoken';
import jwtDecode from 'jwt-decode';
import mongoose from 'mongoose';
import composeWithMongoose from 'graphql-compose-mongoose';
import composeWithRelay from 'graphql-compose-relay';
// import { Resolver } from 'graphql-compose';
// import { GraphQLList, GraphQLString, GraphQLInt, GraphQLEnumType } from 'graphql';
// import _ from 'lodash';
// import { returnUser } from '../../utils/user-data-types';
import { UserProfileModel } from '../models/user-profile';
import config from '../../config';
import DataTypes from '../../utils/data-types';

const mobileDeviceLifetime = 100000; // time a device can live

const UserDeviceSchema = new mongoose.Schema(
  {
    // User who created this device
    userId: { type: String, default: '' },

    // android, ios, web
    typo: { type: String, default: '' },

    // User device
    info: {
      wlan: { type: String, default: '' },
      deviceCountry: { type: String, default: '' },
      timezone: { type: String, default: '' },
      imei: { type: String, default: '' },
      osid: { type: String, default: '' },
      osName: { type: String, default: '' },
      pushtoken: { type: String, default: '' },
      unique: { type: String, default: '' },
      appversion: { type: String, default: '13' },
    },

    auth: {
      token: { type: String, default: '' },
      refreshToken: { type: String, default: '' },
      removed: { type: Boolean, default: false },
      suspended: { type: Boolean, default: false },
    },

    prefs: {
      pushNotifications: { type: Boolean, default: false },
    },

    utime: { type: Number, default: new Date().getTime() },

    // Poor old UT
    ut: DataTypes.ut,
  },
  {
    collection: 'userdevices',
    timestamps: true,
  }
);

UserDeviceSchema.index({ createdAt: 1, 'ut.dnum': 1 });

export const UserDeviceModel = mongoose.model('UserDevice', UserDeviceSchema);
export const UuserDeviceTC = composeWithRelay(composeWithMongoose(UserDeviceModel));

/**
 *
 * @param {*} updateDevice { DeviceObject }
 * @param {*} query { Query }
 */
export async function deviceSync({ updateDevice, query }) {
  let device;
  console.log(updateDevice);
  console.log(query);
  try {
    device = await UserDeviceModel.findOneAndUpdate(query, updateDevice, {
      upsert: true,
      new: true,
    }).exec();
    if (device) {
      console.log('Saved device');
      console.log(device);
    } else {
      console.log('Failed to save device');
    }
  } catch (error) {
    console.log(error);
  }

  return device;
}

/**
 *
 * @param {*} req { unique, pushtoken, osName, clientType, appversion, userId  }
 * @returns {*} deviceObject
 */
export async function uniDeviceSync({ userDevice }) {
  const { unique, pushtoken, osName, appversion, clientType, userId } = userDevice;

  let configDevice;
  let query;
  let device;
  if (userId) {
    // We have a userId
    configDevice = {
      userId,
      typo: clientType,

      info: {
        ...userDevice,
        unique,
        pushtoken,
        osName,
        appversion,
      },
    };
    query = { userId, 'info.unique': unique };
  } else {
    // We don't have any user session
    // Going to anonymous device
    query = { 'info.unique': unique };
    configDevice = {
      // userId: req.user._id,
      typo: clientType,

      info: {
        ...userDevice,
        unique,
        pushtoken,
        osName,
        appversion,
      },
    };
  }

  try {
    device = await deviceSync({ updateDevice: configDevice, query });

    if (!device) {
      throw new Error('Error syncing the device');
    }

    return device;
  } catch (error) {
    console.log(error.message);
    return device;
  }
}

/**
 *
 * @param {*} deviceId
 * @param {*} userId
 */
export async function generateNewTokenForDevice({ deviceId, userId }) {
  // let userObject;
  let userDeviceObject;

  try {
    /** 
    userObject = await UserProfileModel.findById(userId).exec();
    if (!userObject) {
      throw new Error('User not found');
    }
    * */

    userDeviceObject = await UserDeviceModel.findById(deviceId).exec();
    if (!userDeviceObject) {
      throw new Error('User device not found');
    }

    // Assign new token for this user's device
    const token = jwt.sign(
      {
        _id: userId,
        exp: (new Date().getTime() + mobileDeviceLifetime) / 1000,
      },
      config.jwtSecret
    );

    const refreshToken = jwt.sign(
      {
        _id: userId,
        exp: new Date().getTime() / 1000,
      },
      config.jwtSecret
    );

    // console.log('User device = ', userDeviceObject);
    userDeviceObject.auth.token = token;
    userDeviceObject.auth.refreshToken = refreshToken;

    await userDeviceObject.save();
    console.log('Added auth to device');
    console.log(userDeviceObject.auth);
    return userDeviceObject;
  } catch (error) {
    console.log(error.message);
    return null;
  }
}

export function refreshTokenFromOld(oldToken) {
  // decode the old token
  const user = jwtDecode(oldToken);

  const token = jwt.sign(
    {
      _id: user._id,
      exp: (new Date().getTime() + mobileDeviceLifetime) / 1000,
    },
    config.jwtSecret
  );

  /** 
  const refreshToken = jwt.sign(
    {
      _id: user._id,
      exp: new Date().getTime() / 1000,
    },
    config.jwtSecret
  );
  * */
  return { token };
}
