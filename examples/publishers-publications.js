/* @flow */

import mongoose from 'mongoose';
// import { mongoUri } from './src/config';
require('dotenv').config();
let mongoUri = process.env.MONGODB_URI;
mongoose.Promise = Promise;


mongoose.connect(mongoUri);

mongoose.connection.on('error', e => {
  if (e.message.code === 'ETIMEDOUT') {
    console.log(e);
    mongoose.connect(mongoUri, opts);
  }
  console.log(e);
});
mongoose.connection.on('connected', async () => {
  console.log(`MongoDB successfully connected to ${mongoUri}`);
  // const { NewsArtModel } = require('./src/news/models/news-article');
  await loop();
});

async function loop() {
  const { NewsPublisherModel } = require('./src/news/models/news-publisher');
  // const { PublicationModel } = require('./src/news/models/news-publication');
  const { UploadFileModel } = require('./src/utils/uploads/uploads-model');
  let publishers;

  try {
    publishers = await NewsPublisherModel.find({}).exec();

    if (publishers.length <= 0) {
      throw new Error('Publishers null');
    }

    await Promise.all(
      publishers.map(publisher => {
        return new Promise((res, rej) => {
          publisher.name = publisher.meta.name;
          publisher.domain = publisher.meta.name;
          publisher.slogan = publisher.meta.slogan || '';
          publisher.description = publisher.meta.des;
          publisher.website = publisher.meta.website;
          publisher.username = publisher.meta.username;

          // Country Code
          // publisher.countryCode = publisher.countryCode;

          publisher.xpath_latest = publisher.meta.xpath_latest;
          publisher.xpath_featured = publisher.meta.xpath_featured;
          publisher.xpath_des = publisher.meta.xpath_des;
          publisher.xpath_author = publisher.meta.xpath_author;
          publisher.xpath_fimg = publisher.meta.xpath_fimg;

          NewsPublisherModel.update({ _id: publisher._id }, publisher, (er, reso) => {
            if (!er) {
              console.log(er);
              console.log('Saved new publication');
              // save uploader
              const awsUrl = publisher.image.logo_white.replace(
                'http://192.168.0.11:4040',
                'https://s3-us-west-2.amazonaws.com/vtvthumb/v2'
              );
              const logoWhite = new UploadFileModel({
                url: awsUrl,
                provider: 'aws-s3',
                name: 'sample.png',
                mime: 'image/png',
                ext: '.png',
                size: '0',
                related: [
                  {
                    ref: publisher._id,
                    kind: 'Newspubs',
                    field: 'logoWhite',
                  },
                ],
              });
              // Save logo
              logoWhite.save((err, s) => {
                if(s){
                  return res(s);
                }
                return rej(er);
              });
              // return rej(er);
            }
            return res(reso);
          });
        });
      })
    );

    return { done: true };
  } catch (error) {
    console.log(error);
    return { done: true };
  }
}
