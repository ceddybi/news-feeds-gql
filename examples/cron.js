const cron = require('cron');
const { bind, update } = require('./cronManager');

/** 
const job1 = new cron.CronJob({
  cronTime: '* * * * *',
  onTick() {
    console.log('job 1 ticked');
  },
  start: true,
  timeZone: 'Africa/Kigali',
});

const job2 = new cron.CronJob({
  cronTime: '* * * * *',
  onTick() {
    console.log('job 2 ticked');
  },
  start: true,
  timeZone: 'Africa/Kigali',
}); **/

// Bind all starters
[
  { id: 'x1', cronTime: '* * * * *', cb: () => console.log('Job 1') },
  { id: 'x2', cronTime: '* * * * *', cb: () => console.log('Job 2') },
].map(x => {
  bind(x);
});

setTimeout(() => {
  const newTime = '* * * * *';
  bind({ id: 'x2', cronTime: newTime, cb: () => console.log('Job 3') });
}, 61000);

// job1.start();
