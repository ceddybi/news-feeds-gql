/* @flow */

/* eslint-disable */

import schema from './schema';

export default {
  uri: "/news",
  schema: schema,
  title: "News API",
  description:'',
};
