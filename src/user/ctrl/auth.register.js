import jwt from 'jsonwebtoken';
import httpStatus from 'http-status';
import jwtDecode from 'jwt-decode';
import { isEmpty } from 'lodash';
import APIError from '../helpers/APIError';
import config from '../../config';
import { argonVerify, generatePwHash } from '../../utils/passord-utils';
import { returnUser, returnMyUserObject } from '../../utils/user-data-types';
import { UserProfileModel } from '../models/user-profile';
import { deviceSync, uniDeviceSync, generateNewTokenForDevice } from '../models/user-device';
import { addFirstChatWithAdmin } from '../../chat/helpers/user.chat.helpers';
// import { uniDeviceSync, generateNewTokenForDevice } from './auth.device';

export const mobileDeviceLifetime = 259200; // time a device can live

async function addAdmin() {
  let adminUser;
  let adminPw;
  try {
    adminPw = await generatePwHash('thebiggerthebill');
    const vg = new UserProfileModel({
      info: {
        // userid: 'vs',
        firstname: 'Vuga',
        lastname: 'Team',
        username: 'vs',
        phone: '80',
        email: 'support@vuga.io',
        a: adminPw,
        image_path: 'http://vuga.s3-website-us-west-2.amazonaws.com/static/vuga_peace_black.jpg',
      },
    });
    vg.save((err, doc) => {
      // d = doc;
      console.log('Created Admin Account');
    });

    return {};
  } catch (error) {
    console.log(error.message);
    return {};
  }
}

async function adminChecker() {
  let admin;
  try {
    admin = await UserProfileModel.findOne({ 'info.username': 'vs' });
    if (!admin) {
      addAdmin();
    } else {
      console.log('Admin check is perfect');
    }
  } catch (error) {
    console.log(error.message);
  }
}
adminChecker();

/**
 * Returns jwt token if valid username and password is provided
 * @param req
 * @param res
 * @param next
 * @returns { }
 */
export async function login(req, res, next) {
  console.log('User Login request');
  console.log(req.body);
  const un = req.body.username;
  const pw = req.body.password;
  let user;

  try {
    user = await UserProfileModel.findOne({
      $or: [
        { 'info.username': un.toLowerCase() },
        { 'info.email': un.toLowerCase() },
        { 'info.phone': un },
      ],
    }).exec();

    // User not found, throw error
    if (!user) {
      throw new Error('Invalid login details');
    }

    console.log(`Found some user ${user.info.username}`);
    // We are very sure field `a` is available
    const argoHash = user.info.a;
    const argonParams = {
      hash: argoHash,
      pw,
    };

    const isPasswordCorrect = await argonVerify(argonParams);

    // if password correct
    if (!isPasswordCorrect) {
      throw new Error('Invalid password or username');
    }

    const userToreturn = returnMyUserObject(user);

    const token = jwt.sign(
      {
        _id: user._id,
        exp: new Date().getTime() + mobileDeviceLifetime / 1000,
      },
      config.jwtSecret
    );

    const loggedDevice = {
      userId: user._id,

      info: {
        ...req.body.device,
      },
      auth: {
        token,
        refreshToken: token,
      },
    };

    // Save or update device
    await uniDeviceSync({
      userDevice: { ...loggedDevice, userId: user._id },
    });

    return res.json({
      message: 'Success Login',
      deviceAuth: loggedDevice.auth,
      status: 200,
      user: userToreturn,
    });

    // throw new Error('Invalid login details');
  } catch (error) {
    console.log(error);

    // return error now
    return res.json({
      status: 400,
      message: error.message,
      user: {},
    });
  }
}

/**
 * Checking if user's email or phone are taken
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
export async function checkNumberAndEmail(req, res, next) {
  const { phone, email } = req.body;
  let userEmail;
  let userPhone;
  try {
    userPhone = await UserProfileModel.findOne({
      $or: [{ 'info.phone': phone }, { 'info.phone': `+${phone}` }],
    }).exec();
    if (userPhone) {
      console.log(userPhone.info);
      throw new Error('Phone number is already used');
    }

    userEmail = await UserProfileModel.findOne({ 'info.email': email }).exec();
    if (userEmail) {
      console.log(userEmail.info);
      throw new Error('Email is already used');
    }

    console.log('User not found');
    return res.json({ message: 'Success', status: 200 });
    // Throw error now,
  } catch (error) {
    console.log(error);
    return res.json({ message: error.message, status: 400 });
  }
}

/**
 * Checking if username exists
 * @param {*} req
 * @param {*} res
 * @param {*} next
 *
 */
export async function checkUsername(req, res, next) {
  const { username } = req.body;
  let userObject;
  try {
    userObject = await UserProfileModel.findOne({ 'info.username': username }).exec();
    if (userObject) {
      console.log(userObject.info);
      throw new Error('Username is already taken');
    }
    // console.log('User not found');
    return res.json({ message: 'Success', status: 200 });
    // Throw error now,
  } catch (error) {
    console.log(error.message);
    return res.json({ message: error.message, status: 400 });
  }
}

/**
 * Checking if user exists by phone number
 * @param {*} req phone
 * @param {*} res
 * @param {*} next
 *
 */
export async function checkUserByPhone(req, res, next) {
  let { phone } = req.body;
  phone = phone.replace('+', '');
  let userObject;

  try {
    userObject = await UserProfileModel.findOne({
      $or: [{ 'info.phone': phone }, { 'info.phone': `+${phone}` }],
    }).exec();
    if (userObject) {
      console.log(userObject.info);
      return res.json({ message: 'Success', status: 200 });
    }
    throw new Error('User does not exist, please signup');
    // console.log('User not found');
  } catch (error) {
    console.log(error.message);
    return res.json({ message: error.message, status: 400 });
  }
}

/**
 * Checking if user exists by phone number
 * @param {*} req phone, device
 * @param {*} res
 * @param {*} next
 *
 */
export async function loginByPhone(req, res, next) {
  const { device } = req.body;
  let { phone } = req.body;
  phone = phone.replace('+', '');
  let userObject;
  try {
    userObject = await UserProfileModel.findOne({
      $or: [{ 'info.phone': phone }, { 'info.phone': `+${phone}` }],
    }).exec();
    if (userObject) {
      console.log(userObject.info);

      const token = jwt.sign(
        {
          _id: userObject._id,
          exp: new Date().getTime() + mobileDeviceLifetime / 1000,
        },
        config.jwtSecret
      );

      const userToreturn = returnMyUserObject(userObject);

      const loggedDevice = {
        userId: userObject._id,

        info: {
          ...device,
        },
        auth: {
          token,
          refreshToken: token,
        },
      };

      // Save or update device
      await uniDeviceSync({
        userDevice: { ...loggedDevice, userId: userObject._id },
      });

      return res.json({
        message: 'Success Login',
        deviceAuth: loggedDevice.auth,
        status: 200,
        user: userToreturn,
      });
    }
    throw new Error('User does not exist, please signup');
    // console.log('User not found');
  } catch (error) {
    console.log(error.message);
    return res.json({ message: error.message, status: 400 });
  }
}

/**
 * Signup/registration
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
export async function register(req, res, next) {
  const { lastname, firstname, username, sex, email, password } = req.body;
  let { phone } = req.body;
  phone = phone.replace('+', '');
  // TODO check email, phone, username
  let userEmail;
  let userPhone;
  let userUsername;
  let newUserDevice;
  let newUser;

  try {
    // Checking username, email, phone.
    userPhone = await UserProfileModel.findOne({
      $or: [{ 'info.phone': phone }, { 'info.phone': `+${phone}` }],
    }).exec();
    if (userPhone) {
      console.log(userPhone.info);
      throw new Error('Phone number is already used');
    }

    userEmail = await UserProfileModel.findOne({ 'info.email': email }).exec();
    if (userEmail) {
      console.log(userEmail.info);
      throw new Error('Email is already used');
    }

    userUsername = await UserProfileModel.findOne({ 'info.username': username }).exec();
    if (userUsername) {
      console.log(userUsername.info);
      throw new Error('Username is already taken');
    }

    // Create account now,

    // Argon password
    const passwordHashed = await generatePwHash(password);
    if (!passwordHashed) {
      throw new Error('Unexpected error occured, please try again');
    }

    newUser = {
      info: {
        a: passwordHashed, // Award winning password argo.
        firstname,
        lastname,
        phone,
        email,
        username: username.toLowerCase(),
        sex,
        // password: pass
      },
    };

    newUser = new UserProfileModel(newUser);

    // create a new device
    newUserDevice = await uniDeviceSync({
      userDevice: { ...req.body.device, userId: newUser._id },
    });

    if (!newUserDevice) {
      throw new Error('Failed to create new device, please try again later');
    }

    // add token to device
    newUserDevice = await generateNewTokenForDevice({
      deviceId: newUserDevice._id,
      userId: newUser._id,
    });

    if (!newUserDevice) {
      throw new Error('Failed to create new device, please try again later');
    }

    // save new user
    await newUser.save();

    // Add first convo with admin, but no need to listen for callback
    await addFirstChatWithAdmin(newUser._id);

    // Sucesss signup now
    return res.json({
      message: 'Success',
      status: 200,
      user: returnMyUserObject(newUser),
      deviceAuth: newUserDevice.auth,
    });
  } catch (error) {
    console.log(error);
    return res.json({ message: error.message, status: 400 });
  }
}
