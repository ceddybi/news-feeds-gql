/**
 * Created by vuga on 9/22/17.
 */
import mongoose from 'mongoose';
import composeWithMongoose from 'graphql-compose-mongoose';
import { TypeComposer, Resolver, typeByPath } from 'graphql-compose';
import composeWithRelay from 'graphql-compose-relay';
import { GraphQLString } from 'graphql';
import { find, uniq, isEmpty } from 'lodash';
import { typePageInfo, typeThreadInput, typeUserIntent } from '../../utils/data-types';
import { NewsPubTC } from './news-publisher';
import { ThreadResolver } from '../../utils/gql-threads';
import { TYPE_NICES, TYPE_COMMENTS, TYPE_VIEWS } from '../../utils/CB';
import { isArray } from 'util';
import { getRandomInt } from '../../utils/math-utils';

// const _DA = require('./static/data');
const reactionCounts = {
  counts: { type: Number, default: 0 },
  userIds: [],
};

export const newsArtSchema = new mongoose.Schema(
  {
    // Publisher ID
    pubid: { type: String, default: '' },

    // Article ID
    artid: { type: String, default: '' },

    // Index
    pos: { type: Number, default: 1 },

    // Featured
    featured: { type: Boolean, default: false },

    // Article banner images
    image: {
      banner: { type: String, default: '' },
      banner_rec: { type: String, default: '' },
      banner_sq: { type: String, default: '' },
    },

    // Basic info
    meta: {
      // twitter, facebook, ig, website,
      title_sm: { type: String, default: '' },
      title_og: { type: String, default: '' },
      des: { type: String, default: '' },

      url: { type: String, default: '' },
      author: { type: String, default: '' },
    },

    // contents of the post
    data: [
      {
        // Media ID, for all types
        medid: { type: String, default: '' },

        // Index
        pos: { type: Number, default: 1 },

        // Typo, txt,img,vid
        typo: { type: String, default: '' },

        // Text
        text: { type: String, default: '' },

        /** Begin OBJECTS ** */
        // Text object
        txt: {
          // TODO, bold,h2,h3,p,it
          style: { type: String, default: '' },

          text: { type: String, default: '' },
          quote: { type: String, default: '' },
        },

        // Image object
        img: [
          {
            // TODO 300x400, XxY
            size: { type: String, default: '' },

            src: { type: String, default: '' },
            cap: { type: String, default: '' },
          },
        ],

        // Video object
        vid: [
          {
            // Typo, raw, yt
            typo: { type: String, default: '' },

            // TODO 300x400, WxH
            size: { type: String, default: '' },

            // Typo, txt,img,vid
            src: { type: String, default: '' },
            cap: { type: String, default: '' },
          },
        ],

        iframe: [
          {
            // Typo, raw, yt
            typo: { type: String, default: '' },

            // TODO 300x400, WxH
            size: { type: String, default: '' },

            // Typo, txt,img,vid
            src: { type: String, default: '' },
            cap: { type: String, default: '' },
          },
        ],
      },
    ],

    // rawhtml, TODO Deprecate
    raw: [
      {
        typo: { type: String, default: '' },
        data: { type: String, default: '' },
      },
    ],

    html: { type: String, default: '' },

    utime: { type: Number, default: new Date().getTime() },

    ut: {
      d: { type: Date, default: Date.now },
      dstr: { type: String, default: `${new Date().getTime()}` },
      dnum: { type: Number, default: new Date().getTime() },
    },

    views: {
      cstr: { type: String, default: '0' },
      cnum: { type: Number, default: 0 }, // Old field
      ...reactionCounts,
    },

    comments: {
      ...reactionCounts,
    },

    nices: {
      ...reactionCounts,
    },

    shares: {
      ...reactionCounts,
    },
    countryCode: { type: String, default: '' },
    // createdAt: { type: Date },
    // updatedAt: { type: Date },
  },
  {
    collection: 'newsarts',
    timestamps: true,
  }
);

export function newsArtsTransformer(items) {
  // console.log('Got these items,', items);
  if (!isArray(items)) {
    items = [items];
  }
  return items.map(item => {
    return {
      _id: item._id,
      imageBanner: item.image.banner,
      html: item.raw[0].data /** Old version here * */ || item.html /** new version here * */,
      author: item.meta.author,
      url_source: item.meta.url,
      title: item.meta.title_og,
      description: item.meta.des,
      createdAt: item.createdAt || new Date(item.ut.dnum) || new Date(),
      updatedAt: item.updatedAt || new Date(item.ut.dnum) || null,
      pubId: item.pubid,

      //  Reactions
      views_counts: item.views.cnum + item.views.counts,
      comments_counts: item.comments.counts,

      // Country code
      countryCode: item.countryCode || 'RW',
    };
  });
}

export const NewsArtModel = mongoose.model('newsArt', newsArtSchema);

export const NewsArtTC = composeWithRelay(composeWithMongoose(NewsArtModel));

const resolverByDate = new Resolver({
  name: 'byArtDate',
  type: [NewsArtTC],
  kind: 'query',
  resolve: ({ source, args, context, info }) => {
    return NewsArtModel.find({})
      .sort({ 'ut.cnum': -1 })
      .limit(20)
      .lean();
  },
});

const resolverTop5 = new Resolver({
  name: 'top5',
  type: [NewsArtTC],
  kind: 'query',
  args: {
    website: {
      type: GraphQLString,
    },
  },
  resolve: ({ source, args, context, info }) => {
    // console.log(args);
    // console.log(source);
    return NewsArtModel.find({ 'meta.url': { $regex: `.*${args.website.url}.*` } })
      .sort({ 'ut.cnum': -1 })
      .limit(5)
      .lean();
  },
});
NewsArtTC.addResolver(resolverTop5);

const resolverBy5Latest = new Resolver({
  name: 'by5Latest',
  type: [NewsArtTC],
  kind: 'query',
  resolve: ({ source, args, context, info }) => {
    return NewsArtModel.find({})
      .sort({ 'ut.cnum': -1 })
      .limit(20)
      .lean();
  },
});

export const NewsArticleFeedTC = TypeComposer.create(`
  type NewsArticleFeed {
    _id: String
    pubId: String
    imageBanner: String
    title: String
    description: String
    html: String
    url_source: String
    author: String
    createdAt: Date
    updatedAt: Date
    views_counts: Int
    comments_counts: Int
    countryCode: String
  }
`);

/** GET ARTICLE BY ID */
const resolverById = new Resolver({
  name: 'byId',
  type: NewsArticleFeedTC,
  kind: 'query',
  args: {
    id: { type: GraphQLString },
  },
  resolve: async ({ source, args, context, info }) => {
    const { id } = args;
    let article;
    try {
      article = await NewsArtModel.findById(id).exec();
      if (!article) {
        throw new Error('Article not found');
      }

      return {
        ...newsArtsTransformer(article)[0],
      };
    } catch (error) {
      console.log(error.message);
      return null;
    }
  },
});
NewsArtTC.addResolver(resolverById);

const NewsArticleFeedThreadTC = TypeComposer.create({
  name: 'NewsArticleFeedThread',
  fields: {
    pageInfo: typePageInfo,
    items: [NewsArticleFeedTC],
  },
});

// publisher relation
NewsArticleFeedTC.addRelation('publisher', {
  resolver: NewsPubTC.getResolver('getOneById'),
  prepareArgs: {
    _id: source => source.pubId,
  },
  projection: { _id: true },
});

NewsArtTC.addResolver(resolverByDate);

// News article threads
// let limit = 48, per feeds fetch
// let limitFeeds = 48;
const NewsArtThreadsResolver = ThreadResolver({
  Model: NewsArtModel,
  ModelTC: NewsArticleFeedTC,
  itemsFormater: items => {
    return newsArtsTransformer(items);
  },
  extraArgs: {
    countryCode: { type: GraphQLString },
  },
  extraQuery: ({ source, args, context, info }) => {
    console.log('-------------------------------------------Arguments ', args);
    const { direction, start } = args;
    let countryCode = args.countryCode || '';
    // start = new Date('2018-04-20T11:01:28.154Z');

    if (countryCode === '*' || isEmpty(countryCode)) {
      // All articles
      countryCode = { $ne: '*' };
    }

    const query = {
      countryCode,
    };

    if (direction === 'old') {
      query.$or = [
        { 'ut.dnum': { $lte: new Date(start).getTime() } },
        // { createdAt: { $lte: new Date(start) } },
      ];
    } else {
      // Greater than the time provided
      query.$or = [
        { 'ut.dnum': { $gte: new Date(start).getTime() } },
        // { createdAt: { $gte: new Date(start) } },
      ];
    }
    // query.countryCode = countryCode;
    console.log('Query parses', query);
    return query;
  },
  extraLimit: ({ source, args, context, info }) => {
    const { limit } = args;
    if (limit < 48) {
      return 48;
    }
    return limit;
  },
  extraSort: ({ source, args, context, info }) => {
    const { start } = args;

    return {
      // createdAt: -1,
      'ut.dnum': -1,
    };

    /**
    if (new Date(start) > new Date('2018-04-19T15:08:58.349Z')) {
      return { createdAt: -1 };
    }
    return { 'ut.dnum': -1 };
    * */
  },
});

NewsArtTC.addResolver(NewsArtThreadsResolver);

/**
 * VIEW-COMMENT-NICES begin from here
 * @param {*} article
 * @param {*} userId
 * @param {*} type
 */
const putUser = async (article, userId, type) => {
  article[type].userIds = article[type].userIds.push(userId);
  article[type].userIds = uniq(article[type].userIds);
  return article;
  // article[type].userIds = includes(article[type].userIds, type)
};

const removeUser = async (article, userId, type) => {
  article[type].userIds = article[type].userIds.filter(id => id !== userId);
  // article[type].userIds = uniq(article[type].userIds);
  return article;
  // article[type].userIds = includes(article[type].userIds, type)
};

/**
 *
 * @param {*} user
 * @param {*} cmt
 * @returns updated articleObject and returns it parsed.
 */
export const putNewsArtVCN = async ({ userId, articleId, type }) => {
  const vcmType = type;
  let article;
  let updateArticle;
  console.log(`Updating this article ${articleId} ${type}`);

  try {
    article = await NewsArtModel.findById(articleId).exec();

    if (!article) {
      throw new Error('Article not found');
    }

    // Overide the VCN counts article counts
    switch (vcmType) {
      case TYPE_COMMENTS:
        article.comments.counts += 1;
        break;
      case TYPE_VIEWS:
        article[vcmType].counts += getRandomInt(400, 1300);
        // if (article.views.counts < 0) {
        // New API CHECKER
        // article.views.counts = article.views.cnum;
        // }
        // article.views.count += 1;
        break;
      case TYPE_NICES:
        article.nices.counts += 1;
        break;
      default:
        break;
    }

    // Add user to VCN userIds to article if present
    if (userId) {
      // Add user action to the object
      article = await putUser(article, userId, vcmType);
      // return article;
    }

    // Save the article now
    updateArticle = await NewsArtModel.findOneAndUpdate({ _id: articleId }, article, {
      upsert: true,
    }).exec();

    // Parse the article before being sent back to client
    article = await newsArtsTransformer(article);

    console.log('Done transforming article');
    // Return the article now
    return article;
  } catch (error) {
    console.log(error);
    return article;
  }

  // return article;
};

export const removeVCN = async (userId, article, vcmType) => {
  switch (vcmType) {
    case TYPE_COMMENTS:
      article.comments.counts -= 1;
      break;
    case TYPE_VIEWS:
      if (article.views.counts < 0) {
        // New API CHECKER
        article.views.counts = article.views.cnum;
      }
      article.views.count -= 1;
      break;
    case TYPE_NICES:
      article.nices.counts -= 1;
      break;
    default:
      break;
  }

  article = await removeUser(article, userId, vcmType);
  return article;
};

// Add views to articles from here
NewsArtTC.addResolver(
  new Resolver({
    name: 'viewArticle',
    args: {
      ...typeUserIntent,
      articleId: GraphQLString, // Article id
    },
    type: [NewsArticleFeedTC],
    kind: 'query',
    resolve: async ({ source, args, context, info }) => {
      const { intentId, userId, articleId } = args;
      let article;
      try {
        article = await NewsArtModel.findById(articleId).exec();
        if (article) {
          // Put Views on this article and return it
          article = await putNewsArtVCN({ articleId, userId, type: TYPE_VIEWS });

          return article;
        }
        return {};
      } catch (error) {
        console.log('View, articles', error.message);
        return {};
      }

      // console.log(start.getTime());
      return {};
    },
  })
);

// New article public article view
NewsArtTC.addResolver(
  new Resolver({
    name: 'ArticleViewMutate',
    args: {
      ...typeUserIntent,
      articleId: GraphQLString, // Article id
    },
    type: NewsArticleFeedTC,
    kind: 'mutation',
    resolve: async ({ source, args, context, info }) => {
      const { userId, articleId } = args;
      let article;
      try {
        article = await NewsArtModel.findById(articleId).exec();
        if (article) {
          // Put Views on this article and return it,
          // TODO it returns an array
          article = await putNewsArtVCN({ articleId, userId, type: TYPE_VIEWS });
          // console.log('ARTICLE', article);
          return article[0];
        }
        return {};
      } catch (error) {
        console.log('View, articles', error.message);
        return {};
      }

      // console.log(start.getTime());
      return {};
    },
  })
);

// New article public article view
NewsArtTC.addResolver(
  new Resolver({
    name: 'ArticleViewQuery',
    args: {
      ...typeUserIntent,
      articleId: GraphQLString, // Article id
    },
    type: NewsArticleFeedTC,
    kind: 'query',
    resolve: async ({ source, args, context, info }) => {
      const { userId, articleId } = args;
      let article;
      try {
        article = await NewsArtModel.findById(articleId).exec();
        if (article) {
          // Put Views on this article and return it,
          // TODO it returns an array
          article = await putNewsArtVCN({ articleId, userId, type: TYPE_VIEWS });
          // console.log('ARTICLE', article);
          return article[0];
        }
        return {};
      } catch (error) {
        console.log('View, articles', error.message);
        return {};
      }

      // console.log(start.getTime());
      return {};
    },
  })
);

