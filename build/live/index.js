"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _schema = require("./schema");

var _schema2 = _interopRequireDefault(_schema);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  uri: "/live",
  schema: _schema2.default,
  title: "Live TV and Radio API",
  description: ''
};

/* eslint-disable */