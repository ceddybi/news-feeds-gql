'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.NewsPubTC = exports.NewsPublisherModel = undefined;

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _graphqlComposeMongoose = require('graphql-compose-mongoose');

var _graphqlComposeMongoose2 = _interopRequireDefault(_graphqlComposeMongoose);

var _graphqlComposeRelay = require('graphql-compose-relay');

var _graphqlComposeRelay2 = _interopRequireDefault(_graphqlComposeRelay);

var _graphql = require('graphql');

var _graphqlCompose = require('graphql-compose');

var _uploadsModel = require('../../utils/uploads/uploads-model');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * Created by vuga on 9/22/17.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            */

// const _DA = require('./static/data');

// import { NewsArtTC, NewsArtModel, newsArtSchema } from './news-article';


const NewsPubSchema = new _mongoose2.default.Schema({
  // Publisher ID
  pubid: { type: String, default: '' },

  // Country Code
  countryCode: { type: String, default: '' },

  // Index
  pos: { type: Number, default: 1 },

  available: { type: Boolean, default: true },

  image: {
    logo_white: { type: String, default: '' },
    logo_black: { type: String, default: '' },
    logo_color: { type: String, default: '' }
  },

  // Basic info
  meta: {
    // twitter, facebook, ig, website,
    name: { type: String, default: '' },
    slogan: { type: String, default: '' },
    des: { type: String, default: '' },
    website: { type: String, default: '' },
    username: { type: String, default: '' },
    xpath_latest: { type: String, default: '' },
    xpath_featured: { type: String, default: '' },
    xpath_des: { type: String, default: '' },
    xpath_author: { type: String, default: '' },
    xpath_fimg: { type: String, default: '' }
  },

  // New API plain
  // Basic info
  name: { type: String, default: '' },
  domain: { type: String, default: '' },
  slogan: { type: String, default: '' },
  description: { type: String, default: '' },
  website: { type: String, default: '' },
  username: { type: String, default: '' },

  // logoWhite: { type: mongoose.Schema.Types.ObjectId, ref: 'Upload_file' },
  // logoBlack: { type: mongoose.Schema.Types.ObjectId, ref: 'Upload_file' },
  // logoColor: { type: mongoose.Schema.Types.ObjectId, ref: 'Upload_file' },

  xpath_latest: { type: String, default: '' },
  xpath_featured: { type: String, default: '' },
  xpath_des: { type: String, default: '' },
  xpath_author: { type: String, default: '' },
  xpath_fimg: { type: String, default: '' }
}, {
  timestamps: true,
  collection: 'newspubs'
});

/** 
const NewsPubFilterArticlsSchema = new mongoose.Schema({
   publisher: {type: NewsPubSchema},
   arts: [newsArtSchema]
}, 
{
  collection: 'pubtop5'
});
* */

const NewsPublisherModel = exports.NewsPublisherModel = _mongoose2.default.model('newsPub', NewsPubSchema);
// const NewsPubFilterArticlesModel = mongoose.model('pubtop5', NewsPubFilterArticlsSchema);

const NewsPubTC = exports.NewsPubTC = (0, _graphqlComposeRelay2.default)((0, _graphqlComposeMongoose2.default)(NewsPublisherModel));
// export const PubTop5TC = composeWithRelay(composeWithMongoose(NewsPubFilterArticlesModel));
NewsPubTC.addResolver(new _graphqlCompose.Resolver({
  name: 'getOneById',
  type: NewsPubTC,
  kind: 'query',
  resolve: (() => {
    var _ref = _asyncToGenerator(function* ({ source, args, context, info }) {
      let pub = {};
      let uploads;
      // console.log(args);

      try {
        pub = yield NewsPublisherModel.findOne(args).exec();

        if (!pub) {
          throw new Error('Publisher not found');
        }
        // transform old and new

        // Checking if it's new api
        if (pub.xpath_featured) {
          uploads = yield (0, _uploadsModel.getRelatedUploads)(pub._id);

          if (uploads && uploads[0]) {
            pub.image = {
              logo_white: uploads[0].url
            };
          }
          /**
          //console.log('RELATIONS', uploads);
          // We have uploads here, apply them now
          if (!isEmpty(uploads)) {
            uploads.map(uploaded => {
              uploaded.related.map(file => {
                if (file.url) {
                  pub[file.field] = uploaded.url;
                }
              });
            });
          }
           console.log('RELATED', pub);
          
          // APPLY new API HERE
          pub.meta = {
            xpath_author: pub.xpath_author,
            xpath_featured: pub.xpath_featured,
            xpath_fimg: pub.xpath_fimg,
            website: pub.website,
          }; * */
        }
        return pub;
      } catch (error) {
        console.log(error.message);

        // Return empty array
        return null;
      }
    });

    return function resolve(_x) {
      return _ref.apply(this, arguments);
    };
  })()
}));