import express from 'express';
import userAuth from './user/routes';
import push from './push/routes';

export function addV3(app) {
  const router = express.Router();
  router.use('/push', push);
  router.use('/auth', userAuth);

  app.use('/v3', router);
}
