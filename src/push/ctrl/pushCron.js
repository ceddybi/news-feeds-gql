import mongoose from 'mongoose';
import { CronJobModel } from '../models/cron-jobs';
import { bind } from './pushCron-manager';
import { SendPushAuto } from './push.controller';
import { NotificationModel } from '../models/notifications';
import { asyncForEach } from '../../utils/async-utils';

require('dotenv').config();

export async function startPushCrons() {
  let cronjobs;
  // let notification;
  try {
    cronjobs = await CronJobModel.find({ available: true })
      // .populate('cronJobId')
      // .populate('notifcationId')
      .exec();

    if (!cronjobs) {
      throw new Error('Cron Jobs not found');
    }

    asyncForEach(cronjobs, async cj => {
      return bind({
        cronTime: cj.cronexp,
        id: cj._id,
        timeZone: cj.timezone,
        cb: async () => {
          const notification = await NotificationModel.findById(cj.notificationId);
          if (!notification) {
            return { done: true };
          }
          const { title, message, typo, typoId } = notification;
          // console.log(cj);
          console.log('Notification', { typo, typoId, message, title });
          if (typo && typoId && title && message) {
            return SendPushAuto({
              title,
              message,
              sound: 'default',
              typo,
              typoId,
              target: cj.target,
            });
          }
          // console.log('Error notification is null');
        },
      });
    });

    console.log('Cronjobs completed = ', cronjobs.length);
  } catch (error) {
    return console.log(error);
  }
}

export async function pushCronJob(cj) {
  if (!process.env.CRONS || process.env.CRONS <= 0) {
    return console.log('CronJobs are not enabled on this server');
    // return;
  }

  console.log('Updating cronjob');

  return bind({
    cronTime: cj.cronexp,
    id: cj._id,
    timeZone: cj.timezone,
    cb: async () => {
      const notification = await NotificationModel.findById(cj.notificationId);
      if (!notification) {
        return { done: true };
      }
      const { title, message, typo, typoId } = notification;
      // console.log(cj);
      console.log('Notification', { typo, typoId, message, title });
      if (typo && typoId && title && message) {
        return SendPushAuto({
          title,
          message,
          sound: 'default',
          typo,
          typoId,
          target: cj.target,
        });
      }
      // console.log('Error notification is null');
    },
  });
}

// Listen for
mongoose.connection.once('open', () => {
  console.log(`MongoDB successfully connected from pushCron`);
  if (process.env.CRONS && process.env.CRONS > 0) {
    console.log('CRONS = ', process.env.CRONS);
    return startPushCrons();
  }
  console.log('Crons are not enabled');
});
