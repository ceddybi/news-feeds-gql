'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UserOldTC = exports.UserOldModel = undefined;

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _graphqlComposeMongoose = require('graphql-compose-mongoose');

var _graphqlComposeMongoose2 = _interopRequireDefault(_graphqlComposeMongoose);

var _graphqlComposeRelay = require('graphql-compose-relay');

var _graphqlComposeRelay2 = _interopRequireDefault(_graphqlComposeRelay);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const UserOldSchema = new _mongoose2.default.Schema({
  fcmToken: { type: String, default: '' }
}, {
  collection: 'users',
  timestamps: true
}); /**
     * Created by vuga on 9/22/17.
     */


UserOldSchema.index({ created_at: 1 });

const UserOldModel = exports.UserOldModel = _mongoose2.default.model('User', UserOldSchema);
const UserOldTC = exports.UserOldTC = (0, _graphqlComposeRelay2.default)((0, _graphqlComposeMongoose2.default)(UserOldModel));