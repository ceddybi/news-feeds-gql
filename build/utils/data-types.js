'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.intentIdentifier = exports.UniData = exports.dataAvailability = exports.typeUserIntent = exports.typeThreadInput = exports.typePageInfo = undefined;

var _graphqlCompose = require('graphql-compose');

var _graphql = require('graphql');

const typePageInfo = exports.typePageInfo = _graphqlCompose.TypeComposer.create(`
    type pageInfo {
      hasNext: Boolean!
      direction: String
    }
`);

const typeThreadInput = exports.typeThreadInput = {
  start: { type: 'Date' },
  direction: _graphql.GraphQLString,
  intentId: _graphql.GraphQLString,
  limit: _graphql.GraphQLInt
};

const typeUserIntent = exports.typeUserIntent = {
  intentId: _graphql.GraphQLString,
  intentType: _graphql.GraphQLString,
  userId: _graphql.GraphQLString
};

/** *This for the initial time data was created */
const initialDate = {
  d: { type: Date, default: Date.now },
  dstr: { type: String, default: `${new Date().getTime()}` },
  dnum: { type: Number, default: new Date().getTime() }
};

const data = {
  /** INITIAL DATE */

  initialDate,

  /** META DATA FOR Posts */

  postmeta: {
    pid: { type: String, default: '' },
    seo: { type: String, default: '' },
    private: { type: Boolean, default: false },
    veri: { type: Number, default: 1 },

    create_on: initialDate
  },

  post_likemeta: {
    pid: { type: String, default: '' },
    unlike: { type: Boolean, default: false },
    veri: { type: Number, default: 1 },

    create_on: initialDate
  },

  post_viewmeta: {
    pid: { type: String, default: '' },
    veri: { type: Number, default: 1 },

    create_on: initialDate
  },

  post_sharemeta: {
    pid: { type: String, default: '' },
    veri: { type: Number, default: 1 },

    create_on: initialDate
  },

  post_commentmeta: {
    pid: { type: String, default: '' },
    commentid: { type: String, default: '' },
    veri: { type: Number, default: 1 },
    create_on: initialDate
  },

  /** META DATA FOR Profile */
  profile_viewmeta: {
    userid: { type: String, default: '' },
    veri: { type: Number, default: 1 },

    create_on: initialDate
  },

  profile_followmeta: {
    userid: { type: String, default: '' },
    veri: { type: Number, default: 1 },

    create_on: initialDate
  },

  /** DATE & COUNTER & AMOUNT */
  ut: {
    dstr: { type: String, default: `${new Date().getTime()}` },
    dnum: { type: Number, default: new Date().getTime() }
  },

  count: {
    cstr: { type: String, default: '0' },
    cnum: { type: Number, default: 0 }
  },

  amount: {
    cur: { type: String, default: '0' },
    cstr: { type: String, default: '0' },
    cnum: { type: Number, default: 0 }
  },

  /** UNIVERSAL USER */

  user: {
    userid: { type: String, default: '' },
    firstname: { type: String, default: '' },
    lastname: { type: String, default: '' },
    username: { type: String, default: '' },
    lastseen: { type: String, default: '' },
    a: { type: Boolean, default: false }
  },

  /** BEGIN FOR TYPO DATA */

  text: {
    title: { type: String, default: '' },
    text: { type: String, default: '' },
    raw: { type: String, default: '' },
    veri: { type: Number, default: 1 }
  },

  video: {
    url: { type: String, default: '' },
    text: { type: String, default: '' },
    tag: [{ type: String, default: '' }],
    place: { type: String, default: '' },
    country: { type: String, default: '' },
    time: { type: String, default: '' }
  },

  image: {
    url: { type: String, default: '' },
    text: { type: String, default: '' },
    tag: [{ type: String, default: '' }],
    place: { type: String, default: '' },
    country: { type: String, default: '' },
    time: { type: String, default: '' }
  },

  audio: {
    url: { type: String, default: '' },
    text: { type: String, default: '' },
    tag: [{ type: String, default: '' }],
    place: { type: String, default: '' },
    country: { type: String, default: '' },
    time: { type: String, default: '' }
  },

  dato: {
    url: { type: String, default: '' },
    text: { type: String, default: '' },
    tag: [''],
    place: { type: String, default: '' },
    country: { type: String, default: '' },
    time: { type: String, default: '' }
  },

  wys: {
    url: { type: String, default: '' },
    text: { type: String, default: '' },
    tag: [{ type: String, default: '' }],
    place: { type: String, default: '' },
    country: { type: String, default: '' },
    time: { type: String, default: '' }
  }
};

const dataAvailability = exports.dataAvailability = {
  /**
   * Availability of course
   * */
  available: {
    type: Boolean,
    default: true
  },

  /**
   * Fake delete
   * */
  deleted: {
    type: Boolean,
    default: false
  },

  /**
   * Fake removed
   * */
  removed: {
    type: Boolean,
    default: false
  }
};

const UniData = exports.UniData = {
  /**
   * Name
   * */
  name: {
    type: String,
    default: ''
  },

  /** Description */
  description: { type: String, default: '' },

  /**
   * Name
   * */
  utime: {
    type: Date,
    default: Date.now
  },

  /**
   * Availability of course
   * */
  available: {
    type: Boolean,
    default: true
  },

  /**
   * Fake delete
   * */
  deleted: {
    type: Boolean,
    default: false
  }
};

const intentIdentifier = exports.intentIdentifier = {
  /**
   * Identifier of intentDataTypes
   * */
  intentType: { type: String, default: '' }, // Can be anything, video, radio, tv, post

  intentParent: { type: String, default: '' } // Must be of the same intentType
};

exports.default = data;