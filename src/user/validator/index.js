import Joi from 'joi';

const device = {
  unique: Joi.string().required(),
  clientType: Joi.string().required(),
  pushtoken: Joi.string(),
  osid: Joi.string(),
  appversion: Joi.string(),
};

export default {
  // POST /user/auth/login
  login: {
    body: {
      username: Joi.string().required(),
      password: Joi.string().required(),
      device,
    },
  },

  // POST /user/auth/login
  loginByPhone: {
    body: {
      phone: Joi.string().required(),
      device,
    },
  },

  checkUserByPhone: {
    body: {
      phone: Joi.string().required(),
    },
  },

  profileImage: {
    query: {
      id: Joi.string().required(),
    },
  },

  refreshToken: {
    body: {
      unique: Joi.string().required(),
      refreshToken: Joi.string().required(),
    },
  },
  deviceSync: {
    body: { device },
  },
  userRefresh: {
    body: {
      userId: Joi.string().required(),
    },
  },
  checkPhoneEmail: {
    body: {
      email: Joi.string().required(),
      phone: Joi.string().required(),
    },
  },
  checkUsername: {
    body: {
      username: Joi.string().required(),
    },
  },
  resetPasswordRequest: {
    body: {
      phone: Joi.string().required(),
    },
  },
  resetPassword: {
    body: {
      password: Joi.string().required(),
      userId: Joi.string().required(),
    },
  },
  changePassword: {
    body: {
      password: Joi.string().required(),
      userId: Joi.string().required(),
    },
  },
  register: {
    body: {
      lastname: Joi.string().required(),
      firstname: Joi.string().required(),
      username: Joi.string().required(),
      sex: Joi.string().required(),
      email: Joi.string().required(),
      phone: Joi.string().required(),
      password: Joi.string().required(),
      device,
    },
  },
  updateProfilePhoto: {
    body: {
      image_path: Joi.string().required(),
      nickname: Joi.string().required(),
    },
  },
  updateProfile: {
    body: {
      userId: Joi.string().required(),
      info: {
        lastname: Joi.string(),
        firstname: Joi.string(),
        username: Joi.string(),
        // nickname: Joi.string(),
        // address: Joi.string(),
        // bio: Joi.string(),
      },
    },
  },
  updatePassword: {
    body: {
      oldPassword: Joi.string().required(),
      newPassword: Joi.string().required(),
    },
  },

  base64Image: {
    body: {
      imageBinary: Joi.string().required(),
      imageType: Joi.string().required(),
      userId: Joi.string().required(),
      fileName: Joi.string().required(),
    },
  },
};
