'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.NotiTypoTC = exports.NotificationTC = exports.NotificationModel = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _graphqlComposeMongoose = require('graphql-compose-mongoose');

var _graphqlComposeMongoose2 = _interopRequireDefault(_graphqlComposeMongoose);

var _graphqlComposeRelay = require('graphql-compose-relay');

var _graphqlComposeRelay2 = _interopRequireDefault(_graphqlComposeRelay);

var _graphqlCompose = require('graphql-compose');

var _graphql = require('graphql');

var _tv = require('../../live/models/tv');

var _radio = require('../../live/models/radio');

var _lodash = require('lodash');

var _dataTypes = require('../../utils/data-types');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

const NotificationSchema = new _mongoose2.default.Schema(_extends({}, _dataTypes.UniData, {
  title: { type: String, default: '' },
  message: { type: String, default: '' },
  typo: { type: String, default: '' }, // Core tv,radio,chat
  typoId: { type: String, default: '' }, // Object('Id)
  typoName: { type: String, default: '' } // name for display
}), {
  collection: 'notifications',
  timestamps: true
});

const NotificationModel = exports.NotificationModel = _mongoose2.default.model('Notification', NotificationSchema, 'notifications');
const NotificationTC = exports.NotificationTC = (0, _graphqlComposeMongoose2.default)(NotificationModel);

const NotiTypoTC = exports.NotiTypoTC = _graphqlCompose.TypeComposer.create({
  name: 'notiType',
  fields: {
    typo: _graphql.GraphQLString,
    typoId: _graphql.GraphQLString,
    typoName: _graphql.GraphQLString
  }
});

NotificationTC.addResolver(new _graphqlCompose.Resolver({
  name: 'notiTypo',
  kind: 'query',
  type: [NotiTypoTC],
  args: {
    typo: {
      type: _graphql.GraphQLString
    }
  },
  resolve: (() => {
    var _ref = _asyncToGenerator(function* ({ source, args, context, info }) {
      const typo = args.typo;

      let item;
      let itemX;
      let items;
      let itemRadio;
      let itemTv;

      try {
        switch (typo) {
          case 'tv':
            item = yield _tv.TvModel.find({}).exec();
            return {
              typo: 'tv',
              typoId: item._id,
              typoName: item.title
            };
          case 'radio':
            item = yield _radio.RadioModel.find({}).exec();
            return {
              typo: 'radio',
              typoId: item._id,
              typoName: item.title
            };
          // case 'article':
          default:
            itemRadio = yield _radio.RadioModel.find({}).exec();
            itemRadio = itemRadio.map(function (ite) {
              return {
                typo: 'radio',
                typoId: ite._id,
                typoName: ite.title
              };
            });
            itemTv = yield _tv.TvModel.find({}).exec();
            itemTv = itemTv.map(function (ite) {
              return {
                typo: 'tv',
                typoId: ite._id,
                typoName: ite.title
              };
            });
            items = (0, _lodash.concat)(itemRadio, itemTv);

            return items;
        }
      } catch (error) {
        return null;
      }
    });

    return function resolve(_x) {
      return _ref.apply(this, arguments);
    };
  })()
}));