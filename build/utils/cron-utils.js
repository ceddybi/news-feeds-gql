'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.rangeHoursToSingle = exports.defaultTo = exports.addLeadingZeroToOption = exports.addLeadingZero = exports.parseCronExpression = exports.replaceEvery = exports.splitMultiple = exports.generateCronExpression = exports.getValue = exports.getValues = exports.ensureMultiple = exports.isMultiple = exports.parseTimeValue = exports.toggleDateType = exports.toOptions = exports.toggleMultiple = exports.EVERY = exports.HOURS = exports.MINUTES = undefined;

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _head = require('lodash/head');

var _head2 = _interopRequireDefault(_head);

var _values2 = require('lodash/values');

var _values3 = _interopRequireDefault(_values2);

var _get = require('lodash/get');

var _get2 = _interopRequireDefault(_get);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const MINUTES = exports.MINUTES = 'MINUTES';
const HOURS = exports.HOURS = 'HOURS';
const EVERY = exports.EVERY = '*';

const toggleMultiple = exports.toggleMultiple = value => {
  if (value instanceof Array) {
    return (0, _head2.default)(value);
  }
  return [value];
};

const toOptions = exports.toOptions = _values => {
  return _values.map(String).map(value => ({
    value,
    label: value
  }));
};

const toggleDateType = exports.toggleDateType = value => {
  return value === MINUTES ? HOURS : MINUTES;
};

const parseTimeValue = exports.parseTimeValue = value => {
  if (value instanceof Array) {
    return value.map(parseTimeValue);
  }
  switch (value) {
    case '*':
      return '1';
    default:
      return value;
  }
};

const isMultiple = exports.isMultiple = value => value instanceof Array;

const ensureMultiple = exports.ensureMultiple = (value, multiple) => {
  if (multiple && !isMultiple(value)) {
    return toggleMultiple(value);
  }
  if (!multiple && isMultiple(value)) {
    return toggleMultiple(value);
  }
  return value;
};

const getValues = exports.getValues = value => value.map(option => option.value);

const getValue = exports.getValue = value => {
  return (0, _get2.default)(value, 'value') || value;
};

const generateCronExpression = exports.generateCronExpression = expression => {
  return (0, _values3.default)(expression).join(' ');
};

const splitMultiple = exports.splitMultiple = (value, field = undefined) => {
  if (value.includes(',')) {
    return value.split(',');
  }
  if (value.includes('/')) {
    return value;
  }
  if (value.includes('-') && field === HOURS) {
    return value;
  }
  if (value === EVERY) {
    return value;
  }
  return [value];
};

const replaceEvery = exports.replaceEvery = value => {
  if (typeof value === 'string') {
    return value.replace('*/', '');
  }
  return value;
};

const parseCronExpression = exports.parseCronExpression = expression => {
  var _expression$split = expression.split(' '),
      _expression$split2 = _slicedToArray(_expression$split, 5);

  const minutes = _expression$split2[0],
        hours = _expression$split2[1],
        dayOfMonth = _expression$split2[2],
        month = _expression$split2[3],
        dayOfWeek = _expression$split2[4];

  const defaultExpression = {
    minutes: EVERY,
    hours: EVERY,
    dayOfMonth: EVERY,
    month: EVERY,
    dayOfWeek: EVERY
  };
  return Object.assign(defaultExpression, {
    minutes: replaceEvery(splitMultiple(minutes)),
    hours: replaceEvery(splitMultiple(hours, HOURS)),
    dayOfMonth: splitMultiple(dayOfMonth),
    month: splitMultiple(month),
    dayOfWeek: splitMultiple(dayOfWeek)
  });
};

const addLeadingZero = exports.addLeadingZero = el => `0${el}`.slice(-2);

const addLeadingZeroToOption = exports.addLeadingZeroToOption = option => {
  const value = option.value,
        label = option.label;

  return {
    label: addLeadingZero(label),
    value
  };
};

const defaultTo = exports.defaultTo = (item, defaultItem) => {
  return item === EVERY || !item ? defaultItem : item;
};

const rangeHoursToSingle = exports.rangeHoursToSingle = hours => {
  if (hours instanceof Array) {
    return hours;
  }
  return hours.split('-')[0];
};