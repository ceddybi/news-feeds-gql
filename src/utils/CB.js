/**
 * TO ALL DATA TYPES USING INTENT TYPE
 */
export const TYPE_TVS = 'tvs';
export const TYPE_RADIOS = 'radios';

export const TYPE_POSTS = 'posts';
export const TYPE_ARTICLES = 'articles';

/**
 * Internal types
 */
export const TYPE_COMMENTS = 'comments';
export const TYPE_NICES = 'nices';
export const TYPE_VIEWS = 'views';
