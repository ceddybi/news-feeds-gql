import { isEmpty, find, clone } from 'lodash';
import { UserProfileModel } from '../../user/models/user-profile';
import { sortNChunk, getDevices } from '../../user/helpers/user.devices';
import { AsyncPush } from './push';
import { returnUser } from '../../utils/user-data-types';
import { ConvoModel } from '../../chat/models/convos';

export async function pushNewChatMessage({ userId, userTo, convoId, message }) {
  let usersDevices;
  let devices;
  let userSender;
  let convo;
  // console.log(userObjects);
  // console.log(convo.users);

  const push = {
    title: 'New message: ',
    message: message.slice(0, 100),
    sound: 'vuga_zing',
    click_action: 'APP',
    data: {
      typo: 'chat',
      /** 
      convo: {
        _id: convo._id,
        convoId: convo._id,
        lastChatId: convo.lastChatId,
        isActivated: convo.isActivated,
        createdAt: convo.createdAt,
        updatedAt: convo.updatedAt,
        // users: chatUsers(),
        // users: convo.users,
        // users,
      }, * */
    },
  };

  try {
    convo = await ConvoModel.findById(convoId)
      .lean()
      .exec();

    userSender = await UserProfileModel.findById(userId);
    if (!userSender) {
      throw { message: 'Sender user not found' };
    }

    const chatUsers = () => {
      // console.log(users);
      const convoUsers = convo.users;
      return convoUsers.map(co => {
        // We only need the sender right? so that app can save this convo
        if (co.userId === userId) {
          co.firstname = userSender.info.firstname;
          co.lastname = userSender.info.lastname;
          return co;
        }
        // receiver here
        return co;
      });
    };
    // console.log('Users', chatUsers());

    // Update push with convo Object
    // push.data.convoId = convo._id;
    push.data.convo = {
      _id: convo._id,
      convoId: convo._id,
      lastChatId: convo.lastChatId,
      isActivated: convo.isActivated,
      createdAt: convo.createdAt,
      updatedAt: convo.updatedAt,
      users: chatUsers(),
    };

    const sendersName = `${userSender.info.firstname} ${userSender.info.lastname}`;

    // Setting title as senders name
    push.title = sendersName.slice(0, 100);
    push.data.user = returnUser(userSender);

    // Get userTo devices
    usersDevices = await getDevices({ version: '3', extraQuery: { userId: userTo } });
    if (!usersDevices || isEmpty(usersDevices)) {
      throw { message: 'Receivers devices not found or empty' };
    }

    devices = await sortNChunk({ devices: usersDevices });
    if (!devices) {
      throw { message: 'Failed to get sorted devices' };
    }

    // Run the loop now
    await AsyncPush({ clientType: 'ios', ids: devices.ios, push });
    await AsyncPush({ clientType: 'android', ids: devices.android, push });
    return { done: true };
  } catch (error) {
    console.log(error.message);
    return { done: false };
  }
}
