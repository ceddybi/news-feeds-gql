import mongoose from 'mongoose';
import composeWithMongoose from 'graphql-compose-mongoose';
import composeWithRelay from 'graphql-compose-relay';
import { TypeComposer, Resolver } from 'graphql-compose';
import { GraphQLString, GraphQLBoolean } from 'graphql';
import { LiveTvTC, TvModel } from '../../live/models/tv';
import { RadioModel } from '../../live/models/radio';
import { concat } from 'lodash';
import { UniData } from '../../utils/data-types';

const NotificationSchema = new mongoose.Schema(
  {
    ...UniData,
    title: { type: String, default: '' },
    message: { type: String, default: '' },
    typo: { type: String, default: '' }, // Core tv,radio,chat
    typoId: { type: String, default: '' }, // Object('Id)
    typoName: { type: String, default: '' }, // name for display
  },

  {
    collection: 'notifications',
    timestamps: true,
  }
);

export const NotificationModel = mongoose.model('Notification', NotificationSchema, 'notifications');
export const NotificationTC = composeWithMongoose(NotificationModel);

export const NotiTypoTC = TypeComposer.create({
  name: 'notiType',
  fields: {
    typo: GraphQLString,
    typoId: GraphQLString,
    typoName: GraphQLString,
  },
});

NotificationTC.addResolver(
  new Resolver({
    name: 'notiTypo',
    kind: 'query',
    type: [NotiTypoTC],
    args: {
      typo: {
        type: GraphQLString,
      },
    },
    resolve: async ({ source, args, context, info }) => {
      const { typo } = args;
      let item;
      let itemX;
      let items;
      let itemRadio;
      let itemTv;

      try {
        switch (typo) {
          case 'tv':
            item = await TvModel.find({}).exec();
            return {
              typo: 'tv',
              typoId: item._id,
              typoName: item.title,
            };
          case 'radio':
            item = await RadioModel.find({}).exec();
            return {
              typo: 'radio',
              typoId: item._id,
              typoName: item.title,
            };
          // case 'article':
          default:
            itemRadio = await RadioModel.find({}).exec();
            itemRadio = itemRadio.map(ite => {
              return {
                typo: 'radio',
                typoId: ite._id,
                typoName: ite.title,
              };
            });
            itemTv = await TvModel.find({}).exec();
            itemTv = itemTv.map(ite => {
              return {
                typo: 'tv',
                typoId: ite._id,
                typoName: ite.title,
              };
            });
            items = concat(itemRadio, itemTv);

            return items;
        }
      } catch (error) {
        return null;
      }
    },
  })
);
