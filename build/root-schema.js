'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _graphqlCompose = require('graphql-compose');

var _graphqlComposeRelay = require('graphql-compose-relay');

var _graphqlComposeRelay2 = _interopRequireDefault(_graphqlComposeRelay);

var _newsArticle = require('./news/models/news-article');

var _newsPublisher = require('./news/models/news-publisher');

var _tv = require('./live/models/tv');

var _radio = require('./live/models/radio');

var _cmts = require('./reaction/models/cmts');

var _nice = require('./reaction/models/nice');

var _graphql = require('graphql');

var _authQql = require('./auth-qql');

var _userProfile = require('./user/models/user-profile');

var _convos = require('./chat/models/convos');

var _chatMsgs = require('./chat/models/chat-msgs');

var _cronJobs = require('./push/models/cron-jobs');

var _jobs = require('./push/models/jobs');

var _notifications = require('./push/models/notifications');

var _pushModel = require('./push/models/push-model');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const GQC = new _graphqlCompose.ComposeStorage();

// SINGLE SCHEMA ON SERVER
// import { GQC } from 'graphql-compose';

// MULTI SCHEMA MODE IN ONE SERVER
// create new GQC from ComposeStorage


(0, _graphqlComposeRelay2.default)(GQC.rootQuery());

const ViewerTC = GQC.get('Viewer');
GQC.rootQuery().addFields({
  viewer: {
    type: ViewerTC.getType(),
    description: 'Data under client context',
    resolve: (obj, args, context) => {
      context.ceddy = {
        title: 'Hello'
      };
      return { obj, args, context };
    },
    args: {
      token: {
        type: _graphql.GraphQLString
      }
    }
  }
});

const ViewerFields = {
  // Live here, Read only no sensitivity
  LiveRadios: _radio.LiveRadioTC.getResolver('categoryCountry'),
  RadioById: _radio.LiveRadioTC.get('$findById'),
  LiveTvs: _tv.LiveTvTC.getResolver('categoryCountry'),
  TvById: _tv.LiveTvTC.get('$findById'),

  // News, Read only no sensitivity
  articlesLatest: _newsArticle.NewsArtTC.getResolver('byArtDate'),

  // View article
  ViewArticle: _newsArticle.NewsArtTC.getResolver('viewArticle'),

  ArticleViewQuery: _newsArticle.NewsArtTC.getResolver('ArticleViewQuery'),
  PublisherConnection: _newsPublisher.NewsPubTC.getResolver('connection'),

  /** ...allowAuthenticatedOnly({
    PublisherTop5: NewsPubTC.getResolver('top5'),
  }), * */

  ArticleConnetion: _newsArticle.NewsArtTC.getResolver('connection'),
  ArticleById: _newsArticle.NewsArtTC.getResolver('byId'),
  ArticleThreads: _newsArticle.NewsArtTC.getResolver('thread'),

  // Reactions sections here
  getComments: _cmts.CommentTC.getResolver('byIntentId'),
  CommentsConnection: _cmts.CommentTC.getResolver('connection'),
  CommentThreads: _cmts.CommentTC.getResolver('thread'),
  getNix: _nice.NiceTC.getResolver('byIntentId'),

  // Users search
  viewUser: _userProfile.UserProfileTC.getResolver('viewUser'),
  searchUsers: _userProfile.UserProfileTC.getResolver('searchUsers'),
  UserProfileConnection: _userProfile.UserProfileTC.getResolver('connection'),
  UserProfileByName: _userProfile.UserProfileTC.getResolver('byName'),
  UserProfileById: _userProfile.UserProfileTC.getResolver('byId'),
  UserProfileByConvoUser: _userProfile.UserProfileTC.getResolver('byConvoUser'),

  // Chat
  ConvoConnection: _convos.ConvoTC.getResolver('connection'),
  ConvoThreads: _convos.ConvoTC.getResolver('thread'),
  // Check if conversation exists
  ConvoCheck: _convos.ConvoTC.getResolver('checkConvo'),

  ChatConnection: _chatMsgs.ChatMsgTC.getResolver('connection'),
  ChatThreads: _chatMsgs.ChatMsgTC.getResolver('thread'),

  // CronJobs, jobs, notifications
  findManyCronJobs: _cronJobs.CronJobTC.get('$findMany'),
  findManyJobs: _jobs.JobTC.get('$findMany'),
  findManyNotifications: _notifications.NotificationTC.get('$findMany'),

  // CronJobs, jobs, notifications
  findOneCronJobs: _cronJobs.CronJobTC.get('$findOne'),
  findOneJobs: _jobs.JobTC.get('$findOne'),
  findOneNotifications: _notifications.NotificationTC.get('$findOne'),
  findManyNotiTypos: _notifications.NotificationTC.getResolver('notiTypo'),
  findManyPushs: _pushModel.PushTC.get('$findMany'),
  findOnePushs: _pushModel.PushTC.get('$findOne')
};

ViewerTC.addFields(ViewerFields);

const CreatorTC = GQC.get('Viewer');
const creatorFields = {
  // View a user
  viewUser: _userProfile.UserProfileTC.getResolver('viewUser'),

  // Create a comment
  createComment: _cmts.CommentTC.getResolver('createComment'),

  ArticleViewMutate: _newsArticle.NewsArtTC.getResolver('ArticleViewMutate'),

  // Create a nice reaction
  createNice: _nice.NiceTC.get('$createOne'),

  // Creact a conversation
  createConvo: _convos.ConvoTC.getResolver('newConvo'),

  // Create a message || send a message
  createMessage: _chatMsgs.ChatMsgTC.getResolver('newChatMsg'),

  // Reads conversations,
  readConvo: _convos.ConvoTC.getResolver('readConvo'),

  // Create and update cron job with listners
  updateCronJob: _cronJobs.CronJobTC.getResolver('updateCron'),

  // System relay methods
  createJobs: _jobs.JobTC.get('$createOne'),
  updateJobs: _jobs.JobTC.get('$updateOne'),
  createNotifications: _notifications.NotificationTC.get('$createOne'),
  updateNotifications: _notifications.NotificationTC.get('$updateOne'),

  // Create and update push with listeners
  updatePush: _pushModel.PushTC.getResolver('updatePush')

};
CreatorTC.addFields(creatorFields);

GQC.rootMutation().addFields({
  creator: {
    type: CreatorTC.getType(),
    description: 'Creator and only creator',
    resolve: (obj, args, context) => {
      context.ceddy = {
        title: 'Hello'
      };
      return { obj, args, context };
    },
    args: {
      token: {
        type: _graphql.GraphQLString,
        isRequired: true
      }
    }
  }
});

exports.default = GQC.buildSchema();