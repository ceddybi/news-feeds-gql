'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UserPublicTC = exports.ConvoUserPublic = exports.PublicUser = exports.UserProfileTC = exports.UserProfileModel = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.parsePublicUser = parsePublicUser;

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _graphqlComposeMongoose = require('graphql-compose-mongoose');

var _graphqlComposeMongoose2 = _interopRequireDefault(_graphqlComposeMongoose);

var _graphqlComposeRelay = require('graphql-compose-relay');

var _graphqlComposeRelay2 = _interopRequireDefault(_graphqlComposeRelay);

var _graphqlCompose = require('graphql-compose');

var _graphql = require('graphql');

var _userDataTypes = require('../../utils/user-data-types');

var _userDataTypes2 = _interopRequireDefault(_userDataTypes);

var _dataTypes = require('../../utils/data-types');

var _dataTypes2 = _interopRequireDefault(_dataTypes);

var _textUtils = require('../../utils/text-utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * Created by vuga on 9/22/17.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            */

// import _ from 'lodash';
// import { ConvoUserTC } from '../../chat/models/convos'

const UserProfileSchema = new _mongoose2.default.Schema({
  // Version updates of profile
  veri: { type: Number, default: 1 },

  // Usermeta, basic info
  info: _userDataTypes2.default.info,
  inbox: _userDataTypes2.default.inbox,

  // Chat, last active, follow, views, post
  lastactive: _dataTypes2.default.ut,

  chatcount: _userDataTypes2.default.count,
  followcount: _userDataTypes2.default.count,
  viewcount: _userDataTypes2.default.count,
  postcount: _userDataTypes2.default.count,

  utime: { type: Date, default: Date.now },
  views: { type: Number, default: 0 },

  // New API
  chatbox: [
    /** {
        convoId, 
        count,
        utime
         } */
  ],

  // New friendship API, friends with more love appear in the news feeds
  friends: [
    /** {
        userId, 
        since: Date(),
        love: Number
        ,
    } */
  ]
}, {
  collection: 'userprofiles',
  timestamps: true
});

const UserProfileModel = exports.UserProfileModel = _mongoose2.default.model('UserProfile', UserProfileSchema);
const UserProfileTC = exports.UserProfileTC = (0, _graphqlComposeRelay2.default)((0, _graphqlComposeMongoose2.default)(UserProfileModel));

UserProfileTC.setResolver('connection', UserProfileTC.getResolver('connection').addFilterArg({
  name: 'byName',
  type: `input byName {
      name: String!
    }`,
  query: (rawQuery, value, resolveParams) => {
    // eslint-disable-line
    console.log(value);
    const name = value.name;

    rawQuery.$or = [{ 'info.firstname': { $regex: `.*${name}.*` } }, { 'info.lastname': { $regex: `.*${name}$.*` } }];
  }
})
// /* FOR DEBUG */
//   .debug()
// /* OR MORE PRECISELY */
//   .debugParams()
//   .debugPayload()
//   .debugExecTime()
);
// UserProfileTC.addResolver(resolverByIntentId);

// Login, checkusername, checkemail, signup, getProfilePic

const resolverByName = new _graphqlCompose.Resolver({
  name: 'byName',
  type: [UserProfileTC],
  kind: 'query',
  args: {
    name: {
      type: [_graphql.GraphQLString]
    }
  },
  resolve: (() => {
    var _ref = _asyncToGenerator(function* ({ source, args, context, info }) {
      const name = args.name;

      let users = [];
      try {
        users = yield UserProfileModel.find({
          $or: [{ 'info.firstname': { $regex: `.*${name}.*` } }, { 'info.lastname': { $regex: `.*${name}$.*` } }]
        }).limit(20).exec();
        return users;
      } catch (error) {
        console.log(error);

        // Return empty array
        return users;
      }
    });

    return function resolve(_x) {
      return _ref.apply(this, arguments);
    };
  })()
});

UserProfileTC.addResolver(resolverByName);

const PublicUser = exports.PublicUser = _graphqlCompose.TypeComposer.create({
  name: 'PublicUser',
  fields: {
    userId: { type: _graphql.GraphQLString },
    firstname: { type: _graphql.GraphQLString },
    lastname: { type: _graphql.GraphQLString },
    username: { type: _graphql.GraphQLString },
    image_path: { type: _graphql.GraphQLString },
    lastactive: { type: _graphql.GraphQLString },
    sex: { type: _graphql.GraphQLString }
  }
});

// Get many users by userIds
UserProfileTC.addResolver(new _graphqlCompose.Resolver({
  name: 'byId',
  type: [PublicUser],
  kind: 'query',
  args: {
    userIds: {
      type: [_graphql.GraphQLString]
    }
  },
  resolve: (() => {
    var _ref2 = _asyncToGenerator(function* ({ source, args, context, info }) {
      const userIds = args.userIds;

      let users = {};
      try {
        users = yield userIds.map((() => {
          var _ref3 = _asyncToGenerator(function* (userId) {
            const user = yield UserProfileModel.findById(userId).exec();
            return (0, _userDataTypes.returnUser)(user);
          });

          return function (_x3) {
            return _ref3.apply(this, arguments);
          };
        })());
        console.log('Got the user');
        return users;
      } catch (error) {
        console.log(error);
        // Return empty array
        return users;
      }
    });

    return function resolve(_x2) {
      return _ref2.apply(this, arguments);
    };
  })()
}));

// Get one user by userId
UserProfileTC.addResolver(new _graphqlCompose.Resolver({
  name: 'byUserId',
  type: PublicUser,
  kind: 'query',
  args: {
    userId: {
      type: _graphql.GraphQLString
    }
  },
  resolve: (() => {
    var _ref4 = _asyncToGenerator(function* ({ source, args, context, info }) {
      const userId = args.userId;

      let user = {};
      try {
        user = yield UserProfileModel.findById(userId).exec();
        return (0, _userDataTypes.returnUser)(user);
      } catch (error) {
        console.log(error);
        // Return empty array
        return user;
      }
    });

    return function resolve(_x4) {
      return _ref4.apply(this, arguments);
    };
  })()
}));

// user stats + public user
const ConvoUserPublic = exports.ConvoUserPublic = _graphqlCompose.TypeComposer.create({
  name: 'ConvoUserPublic',
  fields: {
    userId: { type: _graphql.GraphQLString },
    counts: { type: _graphql.GraphQLInt },

    // Public user
    firstname: { type: _graphql.GraphQLString },
    lastname: { type: _graphql.GraphQLString },
    username: { type: _graphql.GraphQLString },
    image_path: { type: _graphql.GraphQLString },
    photo: { type: _graphql.GraphQLString },
    lastactive: { type: _graphql.GraphQLString },
    sex: { type: _graphql.GraphQLString }
  }
});

// User public user, viewable by anyone
const UserPublicTC = exports.UserPublicTC = _graphqlCompose.TypeComposer.create({
  name: 'UserPublicTC',
  fields: {
    userId: { type: _graphql.GraphQLString },
    // Public user
    firstname: { type: _graphql.GraphQLString },
    lastname: { type: _graphql.GraphQLString },
    username: { type: _graphql.GraphQLString },
    nickname: { type: _graphql.GraphQLString },
    photo: { type: _graphql.GraphQLString },
    lastactive: { type: _graphql.GraphQLString },
    views: { type: _graphql.GraphQLString },
    sex: { type: _graphql.GraphQLString },
    bio: { type: _graphql.GraphQLString },
    address: { type: _graphql.GraphQLString },
    since: { type: _graphql.GraphQLString }
  }
});

// Utility to expose UserPublicTC
function parsePublicUser(user) {
  var _user$info = user.info;
  const firstname = _user$info.firstname,
        lastname = _user$info.lastname,
        nickname = _user$info.nickname,
        sex = _user$info.sex,
        bio = _user$info.bio,
        address = _user$info.address,
        username = _user$info.username,
        image_path = _user$info.image_path;


  return {
    userId: user._id,
    firstname,
    lastname,
    username,
    nickname,
    photo: image_path,
    lastactive: user.utime || user.updatedAt,
    views: `${(0, _textUtils.nKformater)(user.views || 0, 1)}`,
    sex,
    bio,
    address,
    since: user.createdAt
  };
}

const ConvoUserITC = _graphqlCompose.InputTypeComposer.create(`
 input 
 ConvoUser 
  { 
    userId: String!,
    counts: Int!
  }`);

// Get one user by userId
UserProfileTC.addResolver(new _graphqlCompose.Resolver({
  name: 'byConvoUser',
  type: [ConvoUserPublic],
  kind: 'query',
  args: {
    users: {
      type: [ConvoUserITC]
    }
  },
  resolve: (() => {
    var _ref5 = _asyncToGenerator(function* ({ source, args, context, info }) {
      const users = args.users;
      // console.log(users);

      let chatUser = {};
      try {
        chatUser = yield users.map((() => {
          var _ref6 = _asyncToGenerator(function* (user) {
            const userx = yield UserProfileModel.findById(user.userId).exec();
            // console.log(user.counts)
            return _extends({ counts: user.counts }, parsePublicUser(userx));
          });

          return function (_x6) {
            return _ref6.apply(this, arguments);
          };
        })());
        console.log('Got the user');
        return chatUser;
      } catch (error) {
        console.log(error);
        // Return empty array
        return chatUser;
      }
    });

    return function resolve(_x5) {
      return _ref5.apply(this, arguments);
    };
  })()
}));

// View user by userId mutation
UserProfileTC.addResolver(new _graphqlCompose.Resolver({
  name: 'viewUserMutation',
  type: UserPublicTC,
  kind: 'mutation',
  args: {
    userId: {
      type: _graphql.GraphQLString
    }
  },
  resolve: (() => {
    var _ref7 = _asyncToGenerator(function* ({ source, args, context, info }) {
      const userId = args.userId;
      // console.log(users);

      let user = {};
      try {
        user = yield UserProfileModel.findById(userId).exec();

        // Add views to users profile
        user.views += 1;
        // TODO ADD VIEWER COUNT
        user.save(function (er, rawQuery) {});

        if (!user) {
          throw new Error('User not found for viewing');
        }
        return parsePublicUser(user);
      } catch (error) {
        console.log(error);
        // Return empty array
        return user;
      }
    });

    return function resolve(_x7) {
      return _ref7.apply(this, arguments);
    };
  })()
}));

// View user by userId query
UserProfileTC.addResolver(new _graphqlCompose.Resolver({
  name: 'viewUser',
  type: UserPublicTC,
  kind: 'query',
  args: {
    userId: {
      type: _graphql.GraphQLString
    }
  },
  resolve: (() => {
    var _ref8 = _asyncToGenerator(function* ({ source, args, context, info }) {
      const userId = args.userId;
      // console.log(users);

      let user = {};
      try {
        user = yield UserProfileModel.findById(userId).exec();

        if (!user) {
          throw new Error('User not found for viewing');
        }

        // Add views to users profile
        user.views += 1;

        yield user.save();

        return parsePublicUser(user);
      } catch (error) {
        console.log(error);
        // Return empty array
        return null;
      }
    });

    return function resolve(_x8) {
      return _ref8.apply(this, arguments);
    };
  })()
}));

// Search user by name
UserProfileTC.addResolver(new _graphqlCompose.Resolver({
  name: 'searchUsers',
  type: [UserPublicTC],
  kind: 'query',
  args: {
    name: {
      type: _graphql.GraphQLString
    }
  },
  resolve: (() => {
    var _ref9 = _asyncToGenerator(function* ({ source, args, context, info }) {
      // const { name } = args;
      const name = args.name || '';
      // console.log(users);
      let users = [];
      try {
        // user = await UserProfileModel.findById(userId).exec();

        users = yield UserProfileModel.aggregate([{
          $project: {
            fullname: {
              $concat: [{ $toLower: '$info.firstname' }, ' ', { $toLower: '$info.lastname' }, ' ', { $toLower: '$info.username' }]
            },
            info: 1,
            updatedAt: 1,
            utime: 1
          }
        },
        //          { $match: { name: { $regex: `/${name}/i` } } }, /bob j/i
        { $match: { fullname: { $regex: `.*${name.toLowerCase()}.*` } } }, {
          $sort: { updatedAt: -1 }
        }]).limit(20).exec();

        console.log('Users are', users.length);

        if (!users) {
          throw new Error('Users not');
        }

        users = users.map(function (us) {
          return parsePublicUser(us);
        });

        return users;
      } catch (error) {
        console.log(error);
        // Return empty array
        return users;
      }
    });

    return function resolve(_x9) {
      return _ref9.apply(this, arguments);
    };
  })()
}));