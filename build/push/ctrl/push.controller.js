'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SendPushAutoNow = exports.SendPushAuto = exports.GetContentObjects = undefined;

// get All content for push creation
let GetContentObjects = exports.GetContentObjects = (() => {
  var _ref = _asyncToGenerator(function* (req, res) {
    let film;
    let radio;
    let tv;
    const path = req.params.path;
    console.log(`${path} get content`);

    try {
      switch (path) {
        case 'tv':
          tv = yield TvModel.find({}).exec();
          return res.status(200).json(tv);

        case 'radio':
          radio = yield RadioModel.find({}).exec();
          return res.status(200).json(radio);

        /** case 'film':
          film = await FilmModel.find({}).exec();
          return res.status(200).json(film); * */
        default:
      }
    } catch (error) {
      console.log(error);
      return res.status(400).json([]);
    }
  });

  return function GetContentObjects(_x, _x2) {
    return _ref.apply(this, arguments);
  };
})();

/**
 * For newer versions
 * Can be run from anywhere
 */


let SendPushAuto = exports.SendPushAuto = (() => {
  var _ref2 = _asyncToGenerator(function* ({ title, message, sound, typo, typoId, target }) {
    console.log('Send auto push now');
    let v1Devices;
    let v2Devices;
    let v3Devices;
    let dataType;

    let devices;

    const push = {
      title,
      message,
      sound,
      click_action: 'APP'
    };

    const d = {
      typo,
      typoId
    };

    try {
      const extraQuery = yield (0, _user.getTargetQuery)(target);

      console.log('Extra query ', extraQuery);

      v1Devices = yield (0, _user.getDevices)({ version: '1', extraQuery });
      v1Devices = v1Devices.map(function (deva) {
        return {
          typo: 'android',
          info: {
            pushtoken: deva.fcmToken
          }
        };
      }); // Parse Old devices to new version

      v2Devices = yield (0, _user.getDevices)({ version: '2', extraQuery });
      v3Devices = yield (0, _user.getDevices)({ version: '3', extraQuery });

      devices = yield (0, _user.sortNChunk)({ devices: (0, _lodash.concat)(v3Devices, [...v1Devices, ...v2Devices]) });
      if (!devices) {
        throw { message: 'Failed to get sorted devices' };
      }

      // Push data
      dataType = yield (0, _push.getDataTypo)(d);
      if (dataType) {
        push.data = dataType;
      }

      // Run the loop now
      yield (0, _push.AsyncPush)({ clientType: 'ios', ids: devices.ios, push });
      console.log('Sent IOS = ', devices.ios.length);
      yield (0, _push.AsyncPush)({ clientType: 'android', ids: devices.android, push });
      console.log('Sent Android', devices.android.length);

      return { done: true };
      // await AsyncPush(chk, push);
    } catch (error) {
      console.log(error);
      return null;
    }
  });

  return function SendPushAuto(_x3) {
    return _ref2.apply(this, arguments);
  };
})();

/**
 * Pass push object only, then send auto push
 */


let SendPushAutoNow = exports.SendPushAutoNow = (() => {
  var _ref3 = _asyncToGenerator(function* (push) {
    try {
      const notification = yield _notifications.NotificationModel.findById(push.notificationId);
      if (!notification) {
        console.log('ABORT: SENDING NOTIFICATION, Notification not found');
        return { done: true };
      }
      const title = notification.title,
            message = notification.message,
            typo = notification.typo,
            typoId = notification.typoId;
      // console.log(cj);

      console.log('Sending Notification = ', { typo, typoId, message, title, target: push.target });
      if (typo && typoId && title && message) {
        return SendPushAuto({
          title,
          message,
          sound: 'default',
          typo,
          typoId,
          target: push.target
        });
      }
    } catch (error) {
      console.log('ABORT: SENDING NOTIFICATION, Notification not found', error);
      return { done: true };
    }
  });

  return function SendPushAutoNow(_x4) {
    return _ref3.apply(this, arguments);
  };
})();

var _push = require('../helpers/push');

var _userDevice = require('../../user/models/user-device');

var _user = require('../../user/helpers/user.devices');

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

require('./pushCron');

var _notifications = require('../models/notifications');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }
// import './push-demon';


const TvModel = _mongoose2.default.model('Tv');
const RadioModel = _mongoose2.default.model('Radio');

function getContentModel(typo) {
  /** if (typo === 'film') {
    return FilmModel;
  } else * */
  if (typo === 'tv') {
    return TvModel;
  } else if (typo === 'radio') {
    return RadioModel;
  }
}

function getContentObject(typo, obj) {
  /** if (typo === 'film') {
    return {
      title: obj.meta.name,
      id: obj._id,
    };
  } else * */
  if (typo === 'tv') {
    return {
      title: obj.title,
      id: obj._id
    };
  } else if (typo === 'radio') {
    return {
      title: obj.title,
      id: obj._id
    };
  }
}