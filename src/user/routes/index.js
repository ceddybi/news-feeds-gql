import express from 'express';
import validate from 'express-validation';
import expressJwt from 'express-jwt';
import paramValidation from '../validator';
// import authCtrl from '../ctrl/auth.controller';
import {
  login,
  register,
  checkNumberAndEmail,
  checkUsername,
  loginByPhone,
  checkUserByPhone,
} from '../ctrl/auth.register';
import { syncDevice, tokenRefresh, syncRefreshUser } from '../ctrl/auth.device';
import { profileImage, updateProfileInfo, changePassword, uploadImage } from '../ctrl/auth.profile';
import config from '../../config';

import multer from 'multer';
import { getRegions } from '../ctrl/auth.omegas';

// var multer  = require('multer');
const upload = multer({
  dest: `/tmp/public`,
});

const router = express.Router(); // eslint-disable-line new-cap

// ==============================NO AUTH===========================
/** Syncing the users */
router.route('/user/refresh').post(validate(paramValidation.userRefresh), syncRefreshUser);

// ==============================REQUIRE AUTH===========================
/** POST /api/device/sync - Returns okay */
router.route('/device/sync').post(expressJwt({ secret: config.jwtSecret }), syncDevice);

// ==============================NO AUTH===========================
/** Syncing the device for anonymous users */
router.route('/device/sync/public').post(validate(paramValidation.deviceSync), syncDevice);

// ==============================NO AUTH===========================
/** Returns token if correct refreshToken and unique */
router.route('/device/token/refresh').post(validate(paramValidation.refreshToken), tokenRefresh);

// ==============================NO AUTH===========================
router.post('/image/uploader', upload.any(), uploadImage);

// ==============================NO AUTH===========================
router.route('/login').post(validate(paramValidation.login), login);

router.route('/login/phone').post(validate(paramValidation.loginByPhone), loginByPhone);

router.route('/register').post(validate(paramValidation.register), register);

/** Check if phone or email exits */
router
  .route('/check/phone-email')
  .post(validate(paramValidation.checkPhoneEmail), checkNumberAndEmail);

/** Check if username */
router.route('/check/username').post(validate(paramValidation.checkUsername), checkUsername);

/**  NEW PASSWORD RESET FUNCTION
 * Check if user exits by phone number */
router.route('/check/phone').post(validate(paramValidation.checkUserByPhone), checkUserByPhone);

/**
 * Profile picture redirect
 * */
router.route('/dp').get(validate(paramValidation.profileImage), profileImage);

/** NO AUTH */
// UPdate any property in the info fields
router
  .route('/update/profile/info')
  .post(validate(paramValidation.updateProfile), updateProfileInfo);

/** REQUIRE AUTH */
// Change password
router.route('/update/profile/pw').post(validate(paramValidation.changePassword), changePassword);

// router.route('/devices').get(getAlldevices);

/** Returns regions supported */
router.route('/omega/regions').post(getRegions);

export default router;
