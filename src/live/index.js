/* @flow */

/* eslint-disable */

import schema from './schema';

export default {
  uri: "/live",
  schema: schema,
  title: "Live TV and Radio API",
  description:'',
};
