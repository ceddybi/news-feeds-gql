/** STATIC UTILS for getting region info */
export async function getRegions(req, res, next) {
  console.log('-----------------------------Regions');
  return res.json({
    status: 200,
    regions: {
      articles: [
        // { name: 'Global', value: 'GLOBAL' },
        { name: 'Rwanda', value: 'RW' },
        { name: 'Kenya', value: 'KE' },
        { name: 'Uganda', value: 'UG' },
        // { name: 'Tanzania', value: 'TZ' },
        // { name: 'Congo DR', value: 'CD' },
        // { name: 'Burundi', value: 'BI' },
      ],
      tv: [],
      radios: [],
    },
  });
}
