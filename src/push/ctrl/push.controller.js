import { LoopArrayGroups, getDataTypo, AsyncPush } from '../helpers/push';
import { UserDeviceModel } from '../../user/models/user-device';
import { sortNChunk, getTargetQuery, getDevices } from '../../user/helpers/user.devices';
import _, { size, concat } from 'lodash';
import mongoose from 'mongoose';
// import './push-demon';
import './pushCron';
import { NotificationModel } from '../models/notifications';

const TvModel = mongoose.model('Tv');
const RadioModel = mongoose.model('Radio');

function getContentModel(typo) {
  /** if (typo === 'film') {
    return FilmModel;
  } else * */
  if (typo === 'tv') {
    return TvModel;
  } else if (typo === 'radio') {
    return RadioModel;
  }
}

function getContentObject(typo, obj) {
  /** if (typo === 'film') {
    return {
      title: obj.meta.name,
      id: obj._id,
    };
  } else * */
  if (typo === 'tv') {
    return {
      title: obj.title,
      id: obj._id,
    };
  } else if (typo === 'radio') {
    return {
      title: obj.title,
      id: obj._id,
    };
  }
}

// get All content for push creation
export async function GetContentObjects(req, res) {
  let film;
  let radio;
  let tv;
  const path = req.params.path;
  console.log(`${path} get content`);

  try {
    switch (path) {
      case 'tv':
        tv = await TvModel.find({}).exec();
        return res.status(200).json(tv);

      case 'radio':
        radio = await RadioModel.find({}).exec();
        return res.status(200).json(radio);

      /** case 'film':
        film = await FilmModel.find({}).exec();
        return res.status(200).json(film); * */
      default:
    }
  } catch (error) {
    console.log(error);
    return res.status(400).json([]);
  }
}

/**
 * For newer versions
 * Can be run from anywhere
 */
export async function SendPushAuto({ title, message, sound, typo, typoId, target }) {
  console.log('Send auto push now');
  let v1Devices;
  let v2Devices;
  let v3Devices;
  let dataType;

  let devices;

  const push = {
    title,
    message,
    sound,
    click_action: 'APP',
  };

  const d = {
    typo,
    typoId,
  };

  try {
    const extraQuery = await getTargetQuery(target);

    console.log('Extra query ', extraQuery);

    v1Devices = await getDevices({ version: '1', extraQuery });
    v1Devices = v1Devices.map(deva => {
      return {
        typo: 'android',
        info: {
          pushtoken: deva.fcmToken,
        },
      };
    }); // Parse Old devices to new version

    v2Devices = await getDevices({ version: '2', extraQuery });
    v3Devices = await getDevices({ version: '3', extraQuery });

    devices = await sortNChunk({ devices: concat(v3Devices, [...v1Devices, ...v2Devices]) });
    if (!devices) {
      throw { message: 'Failed to get sorted devices' };
    }

    // Push data
    dataType = await getDataTypo(d);
    if (dataType) {
      push.data = dataType;
    }

    // Run the loop now
    await AsyncPush({ clientType: 'ios', ids: devices.ios, push });
    console.log('Sent IOS = ', devices.ios.length);
    await AsyncPush({ clientType: 'android', ids: devices.android, push });
    console.log('Sent Android', devices.android.length);

    return { done: true };
    // await AsyncPush(chk, push);
  } catch (error) {
    console.log(error);
    return null;
  }
}

/**
 * Pass push object only, then send auto push
 */
export async function SendPushAutoNow(push) {
  try {
    const notification = await NotificationModel.findById(push.notificationId);
    if (!notification) {
      console.log('ABORT: SENDING NOTIFICATION, Notification not found');
      return { done: true };
    }
    const { title, message, typo, typoId } = notification;
    // console.log(cj);
    console.log('Sending Notification = ', { typo, typoId, message, title, target: push.target });
    if (typo && typoId && title && message) {
      return SendPushAuto({
        title,
        message,
        sound: 'default',
        typo,
        typoId,
        target: push.target,
      });
    }
  } catch (error) {
    console.log('ABORT: SENDING NOTIFICATION, Notification not found', error);
    return { done: true };
  }
}
