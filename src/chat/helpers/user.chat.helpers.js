import { isEmpty } from 'lodash';
import { UserProfileModel } from '../../user/models/user-profile';
import { createP2Pconvo } from '../models/convos';
import { ChatMsgModel } from '../models/chat-msgs';
import { addNremoveInbox } from '../../user/effects/user.profile.effects';

const WelcomeMessage = `Welcome to Vuga! We have improved our services please feel free ask us anything we are always here to help, \n Thank you so much ♥️`;

export async function addFirstChatWithAdmin(userId) {
  let user;
  let userAdmin;
  let convo;
  let chatMsg;
  let existingMessagesWithAdmin;
  try {
    userAdmin = await UserProfileModel.findOne({ 'info.username': 'vs' }).exec();
    user = await UserProfileModel.findById(userId).exec();

    if (!user) {
      throw new Error('User not found to create first convo');
    }
    if (!userAdmin) {
      throw new Error('Admin not found to create first convo');
    }

    // create convo if not exists
    convo = await createP2Pconvo({ userId, userTo: userAdmin._id });

    // Conversation not found
    if (!convo) {
      throw new Error('Conversation not found');
    }

    // Checking if conversation already has messages
    existingMessagesWithAdmin = await ChatMsgModel.find({ convoId: convo._id }).exec();
    if (existingMessagesWithAdmin.length > 0 || !isEmpty(existingMessagesWithAdmin)) {
      throw new Error('Conversation already has initital messages, ignoring this now');
    }

    chatMsg = new ChatMsgModel({
      convoId: convo._id,
      userId: userAdmin._id,
      message: {
        text: WelcomeMessage,
      },
    });

    // Save convo
    await chatMsg.save();

    // TODO save chat counts
    // Save the last message
    convo.lastChatId = chatMsg._id;

    // Activate the conversation.
    convo.isActivated = true;
    await convo.save();

    console.log('Saved the first message');

    /** Message Effects here no need to await */

    // Send new push to receiver
    // NO PUSH TO USER NOW
    // pushNewChatMessage({ userId, userTo, convoId: convo._id, message });

    // Add counter to receiver
    addNremoveInbox({ convoId: convo._id, userId, type: 'add' });

    return { done: true, recordId: chatMsg._id, record: chatMsg };
  } catch (error) {
    console.log(error.message);
    return { done: false };
  }
}
