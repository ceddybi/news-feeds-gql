/**
 * Created by vuga on 9/22/17.
 */
import mongoose from 'mongoose';

export const UploadsSchema = new mongoose.Schema(
  {
    name: { type: String, default: '' },
    hash: { type: String, default: '' },
    ext: { type: String, default: '' },
    mime: { type: String, default: '' },
    size: { type: String, default: '' },
    url: { type: String, default: '' },
    provider: { type: String, default: '' },
    related: [
      {
        ref: { type: mongoose.Schema.Types.ObjectId },
        kind: { type: String, default: '' },
        field: { type: String, default: '' },
      },
    ],
  },
  {
    timestamps: true,
    collection: 'upload_file',
  }
);

export const UploadFileModel = mongoose.model('Upload_file', UploadsSchema);
// const NewsPubFilterArticlesModel = mongoose.model('pubtop5', NewsPubFilterArticlsSchema);
export async function getRelatedUploads(id) {
  let uploads = [];
  try {
    uploads = await UploadFileModel.find({
      related: {
        $elemMatch: { ref: id },
      },
    }).exec();

    return uploads;
  } catch (error) {
    console.log(error);
    return [];
  }
}
