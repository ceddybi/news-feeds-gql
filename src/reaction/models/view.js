/**
 * Created by vuga on 9/22/17.
 */
import mongoose from 'mongoose';
import composeWithMongoose from 'graphql-compose-mongoose';
import composeWithRelay from 'graphql-compose-relay';
import { Resolver } from 'graphql-compose';
import { GraphQLList, GraphQLString, GraphQLInt, GraphQLEnumType } from 'graphql';
import _ from 'lodash';
import { dataAvailability, intentIdentifier } from '../../utils/data-types';
import { ThreadResolver } from '../../utils/gql-threads';

const ViewSchema = new mongoose.Schema(
  {
    // Intent ID
    intentId: { type: String, default: '' }, // Can be anything post ID, comment ID

    // User who created this nice
    userId: { type: String, default: '' },

    // Data availability here
    ...dataAvailability,

    // Data identifier here
    ...intentIdentifier,
  },
  {
    collection: 'views',
    timestamps: true,
  }
);

const ViewModel = mongoose.model('View', ViewSchema);
export const ViewTC = composeWithRelay(composeWithMongoose(ViewModel));

// Comments threads, by convoId = intentId
const ViewThreadsResolver = ThreadResolver({
  ModelTC: ViewTC,
  extraArgs: {
    intentId: { type: GraphQLString, isRequired: true },
  },
  extraQuery: ({ source, args, context, info }) => {
    const { intentId } = args;
    return {
      intentId,
    };
  },
  Model: ViewModel,
});

ViewTC.addResolver(ViewThreadsResolver);
