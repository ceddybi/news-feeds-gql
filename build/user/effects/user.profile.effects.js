'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.chatboxCounter = exports.addNremoveInbox = undefined;

/**
 *
 * @param {*} param0
 * type = add, see/read
 */
let addNremoveInbox = exports.addNremoveInbox = (() => {
  var _ref = _asyncToGenerator(function* ({ userId, type, convoId }) {
    let user;
    let convo;
    try {
      user = yield _userProfile.UserProfileModel.findById(userId);
      if (!user) {
        throw { message: 'User not found for adding counts to inbox' };
      }

      convo = yield _convos.ConvoModel.findById(convoId);
      if (!convo) {
        throw { message: 'Conversation not found for adding counts to inbox' };
      }

      if (type === 'add') {
        user.inbox.counts += 1;
      } else {
        // If it's below 0 or equal to zero
        if (user.inbox.counts <= 0) {
          user.inbox.counts = 0;
        } else {
          user.inbox.counts -= 1;
        }
      }

      yield user.save();
      console.log(`Added users counter ${user.inbox}`);

      // If we have convoId add it to the convoUser

      convo.users = convo.users.map(function (convoUser) {
        if (convoUser.userId === userId) {
          if (type === 'add') {
            convoUser.counts += 1;
          } else {
            // If it's below 0 or equal to zero
            if (convoUser.counts <= 0) {
              convoUser.counts = 0;
            } else {
              convoUser.counts -= 1;
            }
          }
          console.log(`Added users counter in convo Object ${convoUser}`);
          // return convoUser;
        }
        return convoUser;
      });

      yield convo.save();

      return { done: true };
    } catch (error) {
      console.log(error);
      return { done: false };
    }
  });

  return function addNremoveInbox(_x) {
    return _ref.apply(this, arguments);
  };
})();

/** Utility to add chat counts to the chat box  */


let addCountsTochatBox = (() => {
  var _ref2 = _asyncToGenerator(function* ({ user, convoId, type }) {

    console.log('User chat box before ', user.chatbox);
    try {
      let chatbox = user.chatbox;
      let currentConvo = (0, _lodash.find)(chatbox, { convoId });
      console.log('CurrentConvo', currentConvo);
      console.log('UserChatBox', user.chatbox);

      // If we have the convo in the user object
      if (type === 'add') {
        // this chatbox doesn't exist
        if (!currentConvo) {
          currentConvo = {
            convoId,
            counts: 1,
            utime: new Date()
          };
          chatbox.push(currentConvo);
        } else {
          // update the exsting one with an added count
          currentConvo.utime = new Date(); // Updated time
          currentConvo.counts += 1;

          // Update the chatbox
          chatbox = chatbox.map(function (ch) {
            if (ch.convoId === convoId) {
              return currentConvo;
            }
            return ch;
          });
        }
      } else {
        // Remove
        if (currentConvo) {
          if (currentConvo.counts <= 0) {
            // Already null remove it
            chatbox.filter(function (cb) {
              return cb.convoId !== convoId;
            });
          } else {
            // Only if it great than 0
            currentConvo.counts -= 1;
            // Update the user
            chatbox = chatbox.map(function (ch) {
              if (ch.convoId === convoId) {
                return currentConvo;
              }
              return ch;
            });
          }
        }
      }

      user.chatbox = chatbox;

      console.log('User chat box after ', user.chatbox);
      yield new Promise(function (res, rej) {
        return _userProfile.UserProfileModel.update({ _id: user._id }, user, { upsert: true }, function (err, raw) {
          if (!err) {
            return res(raw);
          }
          return rej(err);
        });
      });
      return user;
    } catch (er) {
      console.log(er);
      return null;
    }
  });

  return function addCountsTochatBox(_x2) {
    return _ref2.apply(this, arguments);
  };
})();
/**
 *
 * @param {*} param0
 * type = add, see/read
 */


let chatboxCounter = exports.chatboxCounter = (() => {
  var _ref3 = _asyncToGenerator(function* ({ userId, type, convoId }) {
    let user;
    let convo;
    let updated;
    try {
      user = yield _userProfile.UserProfileModel.findById(userId);
      if (!user) {
        throw { message: 'User not found for adding counts to inbox' };
      }

      convo = yield _convos.ConvoModel.findById(convoId);
      if (!convo) {
        throw { message: 'Conversation not found for adding counts to inbox' };
      }

      user = yield addCountsTochatBox({ userId, convoId, user, type });
      if (!user) {
        throw { message: 'User failed to update chatbox' };
      }
      console.log(`-------------------CHATBOX ${user.chatbox}`);

      // If we have convoId add it to the convoUser
      convo.users = convo.users.map(function (convoUser) {
        if (convoUser.userId === userId) {
          if (type === 'add') {
            convoUser.counts += 1;
          } else {
            // Remove
            // Just nullfy this
            convoUser.counts = 0;
          }
          // console.log(`Added users counter in convo Object ${convoUser}`);
          // return convoUser;
        }
        return convoUser;
      });

      yield convo.save();

      return { done: true };
    } catch (error) {
      console.log(error);
      return { done: false };
    }
  });

  return function chatboxCounter(_x3) {
    return _ref3.apply(this, arguments);
  };
})();

var _userProfile = require('../models/user-profile');

var _convos = require('../../chat/models/convos');

var _lodash = require('lodash');

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }