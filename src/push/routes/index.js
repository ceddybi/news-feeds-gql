import express from 'express';
import validate from 'express-validation';
import expressJwt from 'express-jwt';
import paramValidation from '../validator';
import config from '../../config';
import { GetContentObjects } from '../ctrl/push.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/get/content/:path').get(validate(paramValidation.getContent), GetContentObjects);

// router.route('/send/msg').post(validate(paramValidation.sendPush), SendPush);

export default router;
