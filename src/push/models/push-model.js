import mongoose, { Schema } from 'mongoose';
import composeWithMongoose from 'graphql-compose-mongoose';
import composeWithRelay from 'graphql-compose-relay';
import { isEmpty } from 'lodash';
import { GraphQLString, GraphQLBoolean } from 'graphql';
import { Resolver } from 'graphql-compose';
import { UniData } from '../../utils/data-types';
import { SendPushAuto, SendPushAutoNow } from '../ctrl/push.controller';

const PushSchema = new Schema(
  {
    ...UniData,
    timezone: { type: String, default: '' },

    notificationId: { type: Schema.Types.ObjectId, ref: 'Notification' }, // Notification
    cronJobId: { type: Schema.Types.ObjectId, ref: 'CronJob' }, // Notification

    // status, pend, done
    status: { type: String, default: '' },
    target: [String],
  },

  {
    collection: 'push-model',
    timestamps: true,
  }
);

export const PushModel = mongoose.model('PushModel', PushSchema);
export const PushTC = composeWithMongoose(PushModel);

// Send Push now if it's create,
// Update cronjob or create new one
PushTC.addResolver(
  new Resolver({
    name: 'updatePush',
    kind: 'mutation',
    type: PushTC,
    args: {
      _id: { type: GraphQLString },
      name: { type: GraphQLString },
      description: { type: GraphQLString },
      available: { type: GraphQLBoolean },
      target: { type: [GraphQLString] },
      notificationId: { type: GraphQLString },
      timezone: { type: GraphQLString },
      status: { type: GraphQLString },
    },
    resolve: async ({ source, args, context, info }) => {
      const { _id, name, description, available, target, notificationId, timezone, status } = args;
      let push;
      let pushOld;

      console.log('Availble = ', available);
      console.log('id = ', _id);

      try {

        // Update now
        if (_id && !isEmpty(_id)) {
          // We are updating cronjob now

          // Lets find the mf
          pushOld = await PushModel.findById(_id).exec();

          if (!pushOld) {
            throw new Error('Cronjob not found');
          }

          push = await PushModel.findByIdAndUpdate(_id, {
            name,
            description,
            available,
            target,
            notificationId,
            timezone,
            status,
          }).exec();

          console.log('updated push', push);

          return push;

          // Todo bind cron daemon
        }
        push = new PushModel({
          name,
          description,
          available,
          target,
          notificationId,
          timezone,
          status: 'done',
        });

        await push.save();

        // Send the push notification now, but do not wait for it
        SendPushAutoNow(push);

        return push;
      } catch (error) {
        console.log(error);
        return null;
      }
    },
  })
);