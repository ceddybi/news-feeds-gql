"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

let asyncForEach = exports.asyncForEach = (() => {
  var _ref = _asyncToGenerator(function* (array, callback) {
    for (let index = 0; index < array.length; index++) {
      yield callback(array[index], index, array);
    }
  });

  return function asyncForEach(_x, _x2) {
    return _ref.apply(this, arguments);
  };
})();

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }