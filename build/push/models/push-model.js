'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PushTC = exports.PushModel = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _graphqlComposeMongoose = require('graphql-compose-mongoose');

var _graphqlComposeMongoose2 = _interopRequireDefault(_graphqlComposeMongoose);

var _graphqlComposeRelay = require('graphql-compose-relay');

var _graphqlComposeRelay2 = _interopRequireDefault(_graphqlComposeRelay);

var _lodash = require('lodash');

var _graphql = require('graphql');

var _graphqlCompose = require('graphql-compose');

var _dataTypes = require('../../utils/data-types');

var _push = require('../ctrl/push.controller');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

const PushSchema = new _mongoose.Schema(_extends({}, _dataTypes.UniData, {
  timezone: { type: String, default: '' },

  notificationId: { type: _mongoose.Schema.Types.ObjectId, ref: 'Notification' }, // Notification
  cronJobId: { type: _mongoose.Schema.Types.ObjectId, ref: 'CronJob' }, // Notification

  // status, pend, done
  status: { type: String, default: '' },
  target: [String]
}), {
  collection: 'push-model',
  timestamps: true
});

const PushModel = exports.PushModel = _mongoose2.default.model('PushModel', PushSchema);
const PushTC = exports.PushTC = (0, _graphqlComposeMongoose2.default)(PushModel);

// Send Push now if it's create,
// Update cronjob or create new one
PushTC.addResolver(new _graphqlCompose.Resolver({
  name: 'updatePush',
  kind: 'mutation',
  type: PushTC,
  args: {
    _id: { type: _graphql.GraphQLString },
    name: { type: _graphql.GraphQLString },
    description: { type: _graphql.GraphQLString },
    available: { type: _graphql.GraphQLBoolean },
    target: { type: [_graphql.GraphQLString] },
    notificationId: { type: _graphql.GraphQLString },
    timezone: { type: _graphql.GraphQLString },
    status: { type: _graphql.GraphQLString }
  },
  resolve: (() => {
    var _ref = _asyncToGenerator(function* ({ source, args, context, info }) {
      const _id = args._id,
            name = args.name,
            description = args.description,
            available = args.available,
            target = args.target,
            notificationId = args.notificationId,
            timezone = args.timezone,
            status = args.status;

      let push;
      let pushOld;

      console.log('Availble = ', available);
      console.log('id = ', _id);

      try {

        // Update now
        if (_id && !(0, _lodash.isEmpty)(_id)) {
          // We are updating cronjob now

          // Lets find the mf
          pushOld = yield PushModel.findById(_id).exec();

          if (!pushOld) {
            throw new Error('Cronjob not found');
          }

          push = yield PushModel.findByIdAndUpdate(_id, {
            name,
            description,
            available,
            target,
            notificationId,
            timezone,
            status
          }).exec();

          console.log('updated push', push);

          return push;

          // Todo bind cron daemon
        }
        push = new PushModel({
          name,
          description,
          available,
          target,
          notificationId,
          timezone,
          status: 'done'
        });

        yield push.save();

        // Send the push notification now, but do not wait for it
        (0, _push.SendPushAutoNow)(push);

        return push;
      } catch (error) {
        console.log(error);
        return null;
      }
    });

    return function resolve(_x) {
      return _ref.apply(this, arguments);
    };
  })()
}));