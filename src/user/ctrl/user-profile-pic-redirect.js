import express from 'express';
import { isEmpty, includes } from 'lodash';
import querystring from 'querystring';
import { UserProfileModel } from '../models/user-profile';

const AWS_STORAGE_URL = 'http://vuga.s3-website-us-west-2.amazonaws.com';
const IMAGE_UPLOADER_URL = 'https://us-central1-vugainc.cloudfunctions.net/api3/upload/dp';

const router = express.Router();

const defaultImage = (sex, dim) => `${AWS_STORAGE_URL}/${dim}/img/u/default/${sex || 'male'}.jpg`;

function trimHost(url) {
  url = url.split('/').filter(f => f.length > 0);
  url.shift();
  url.shift();
  return url.join('/');
}

// Profile redirector
// Checks if user has an image_path else redirect to dummy image
// pic
router.get('/dp', async (req, res) => {
  console.log(req.query);
  const id = req.query.id;
  const height = req.query.height || '100';

  const dim = `${height}x${height}`;
  let user;
  let sex;

  try {
    user = await UserProfileModel.findOne({ _id: id }).exec();
    if (user) {
      console.log(user);
      // found user
      sex = !isEmpty(user.info.sex) ? user.info.sex.toLowerCase() : 'male';

      const imagePath = user.info.image_path;
      if (!isEmpty(imagePath)) {
        return res.redirect(`${AWS_STORAGE_URL}/${dim}/${trimHost(imagePath)}`);
      }

      throw new Error('User image not found');
    }

    throw new Error('User not found');

    // Throw error now,
  } catch (error) {
    console.log(error);
    return res.redirect(defaultImage(sex, dim));
  }
});

/** 
// Upload pic from here,
// get query and redirects to Lambda function to upload the pic
router.post('/dp/upload', async (req, res) => {
  console.log(req.query);
  const query = querystring.stringify(req.query);
  console.log(query);
  // let user;

  res.redirect(`${IMAGE_UPLOADER_URL}/?${query}`);
  // TODO check if profile pic exists else redirect to dummy image
  // res.redirect()
});
* */
export default router;
