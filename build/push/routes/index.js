'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _expressValidation = require('express-validation');

var _expressValidation2 = _interopRequireDefault(_expressValidation);

var _expressJwt = require('express-jwt');

var _expressJwt2 = _interopRequireDefault(_expressJwt);

var _validator = require('../validator');

var _validator2 = _interopRequireDefault(_validator);

var _config = require('../../config');

var _config2 = _interopRequireDefault(_config);

var _push = require('../ctrl/push.controller');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const router = _express2.default.Router(); // eslint-disable-line new-cap

router.route('/get/content/:path').get((0, _expressValidation2.default)(_validator2.default.getContent), _push.GetContentObjects);

// router.route('/send/msg').post(validate(paramValidation.sendPush), SendPush);

exports.default = router;