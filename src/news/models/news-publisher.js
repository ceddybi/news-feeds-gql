/**
 * Created by vuga on 9/22/17.
 */
import mongoose from 'mongoose';
import _, { isEmpty } from 'lodash';
import composeWithMongoose from 'graphql-compose-mongoose';
import composeWithRelay from 'graphql-compose-relay';
// const _DA = require('./static/data');
import { GraphQLObjectType } from 'graphql';
// import { NewsArtTC, NewsArtModel, newsArtSchema } from './news-article';
import { Resolver } from 'graphql-compose';
import { getRelatedUploads } from '../../utils/uploads/uploads-model';

const NewsPubSchema = new mongoose.Schema(
  {
    // Publisher ID
    pubid: { type: String, default: '' },

    // Country Code
    countryCode: { type: String, default: '' },

    // Index
    pos: { type: Number, default: 1 },

    available: { type: Boolean, default: true },

    image: {
      logo_white: { type: String, default: '' },
      logo_black: { type: String, default: '' },
      logo_color: { type: String, default: '' },
    },

    // Basic info
    meta: {
      // twitter, facebook, ig, website,
      name: { type: String, default: '' },
      slogan: { type: String, default: '' },
      des: { type: String, default: '' },
      website: { type: String, default: '' },
      username: { type: String, default: '' },
      xpath_latest: { type: String, default: '' },
      xpath_featured: { type: String, default: '' },
      xpath_des: { type: String, default: '' },
      xpath_author: { type: String, default: '' },
      xpath_fimg: { type: String, default: '' },
    },

    // New API plain
    // Basic info
    name: { type: String, default: '' },
    domain: { type: String, default: '' },
    slogan: { type: String, default: '' },
    description: { type: String, default: '' },
    website: { type: String, default: '' },
    username: { type: String, default: '' },

    // logoWhite: { type: mongoose.Schema.Types.ObjectId, ref: 'Upload_file' },
    // logoBlack: { type: mongoose.Schema.Types.ObjectId, ref: 'Upload_file' },
    // logoColor: { type: mongoose.Schema.Types.ObjectId, ref: 'Upload_file' },

    xpath_latest: { type: String, default: '' },
    xpath_featured: { type: String, default: '' },
    xpath_des: { type: String, default: '' },
    xpath_author: { type: String, default: '' },
    xpath_fimg: { type: String, default: '' },
  },
  {
    timestamps: true,
    collection: 'newspubs',
  }
);

/** 
const NewsPubFilterArticlsSchema = new mongoose.Schema({
   publisher: {type: NewsPubSchema},
   arts: [newsArtSchema]
}, 
{
  collection: 'pubtop5'
});
* */

export const NewsPublisherModel = mongoose.model('newsPub', NewsPubSchema);
// const NewsPubFilterArticlesModel = mongoose.model('pubtop5', NewsPubFilterArticlsSchema);

export const NewsPubTC = composeWithRelay(composeWithMongoose(NewsPublisherModel));
// export const PubTop5TC = composeWithRelay(composeWithMongoose(NewsPubFilterArticlesModel));
NewsPubTC.addResolver(
  new Resolver({
    name: 'getOneById',
    type: NewsPubTC,
    kind: 'query',
    resolve: async ({ source, args, context, info }) => {
      let pub = {};
      let uploads;
      // console.log(args);

      try {
        pub = await NewsPublisherModel.findOne(args).exec();

        if (!pub) {
          throw new Error('Publisher not found');
        }
        // transform old and new

        // Checking if it's new api
        if (pub.xpath_featured) {
          uploads = await getRelatedUploads(pub._id);

          if (uploads && uploads[0]) {
            pub.image = {
              logo_white: uploads[0].url,
            };
          }
          /**
          //console.log('RELATIONS', uploads);
          // We have uploads here, apply them now
          if (!isEmpty(uploads)) {
            uploads.map(uploaded => {
              uploaded.related.map(file => {
                if (file.url) {
                  pub[file.field] = uploaded.url;
                }
              });
            });
          }

          console.log('RELATED', pub);

         
          // APPLY new API HERE
          pub.meta = {
            xpath_author: pub.xpath_author,
            xpath_featured: pub.xpath_featured,
            xpath_fimg: pub.xpath_fimg,
            website: pub.website,
          }; * */
        }
        return pub;
      } catch (error) {
        console.log(error.message);

        // Return empty array
        return null;
      }
    },
  })
);
