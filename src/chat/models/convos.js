/**
 * Created by vuga on 9/22/17.
 */
import mongoose from 'mongoose';
import composeWithMongoose from 'graphql-compose-mongoose';
import composeWithRelay from 'graphql-compose-relay';
import { Resolver, TypeComposer } from 'graphql-compose';
import { GraphQLList, GraphQLString, GraphQLInt, GraphQLEnumType, GraphQLBoolean } from 'graphql';
import _, { isBoolean, isEmpty } from 'lodash';
import { UserProfileModel, PublicUser, UserProfileTC } from '../../user/models/user-profile';
import { ChatMsgTC } from './chat-msgs';
import { ThreadResolver } from '../../utils/gql-threads';
import { chatboxCounter } from '../../user/effects/user.profile.effects';

const convoUser = {
  userId: { type: String, default: '' },

  // unread messages
  counts: { type: Number, default: 0 },

  // Can no longer see anything about this convo
  banned: { type: Boolean, default: false },

  /**
   * If group chat, user cannot do actions, becomes an eyeball.
   * If p2p chat, user cannot do actions
   */
  suspended: { type: Boolean, default: false },

  isAdmin: { type: Boolean, default: false }, // If group chat, creator of this group
  isModerator: { type: Boolean, default: false }, // If group chat, Can add/remove users
  isEditor: { type: Boolean, default: true }, // IF group chat, Can edit meta { name, profile pic}
};

const ConvoSchema = new mongoose.Schema(
  {
    /** V3 and above */
    lastChatId: { type: String, default: '' },

    // Users in this chat
    users: [{ ...convoUser }],

    // p2p or grp
    convoType: { type: String, default: '' },

    // If a conversation is active or now
    isActivated: { type: Boolean, default: false },

    // Utime, the time it has been ironicaly updated
    utime: { type: Date, default: Date.now },

    /** Discontinued in V3 and above
    
    // OLD API HOES v2
    convoid: { type: String, default: '' }, // convoId

    user_id: [], // ID's of users in this chat room ['ID', 'ID']
    allowed: [],
    banned: [],
  
     // Time updates
    ut: _DA.ut,

    // Last message
    last: {
      userid: { type: String, default: '' },
      msg: { type: String, default: '' }
    },
   
    Discontinued in V3 and above
    creator: {
      userid: { type: String, default: '' },
      firstname: { type: String, default: '' },
      lastname: { type: String, default: '' },
      username: { type: String, default: '' },
      lastseen: { type: String, default: '' },
      image_path: { type: String, default: '' },
      a: { type: Boolean, default: false },
      obj: {},
      count: _DA.count,
    },

    operator: {
      userid: { type: String, default: '' },
      firstname: { type: String, default: '' },
      lastname: { type: String, default: '' },
      username: { type: String, default: '' },
      lastseen: { type: String, default: '' },
      image_path: { type: String, default: '' },
      a: { type: Boolean, default: false },
      obj: {},
      count: _DA.count,
    },

    * */
  },
  {
    collection: 'chatconvos',
    timestamps: true,
  }
);

export const ConvoModel = mongoose.model('Convo', ConvoSchema);
export const ConvoTC = composeWithRelay(composeWithMongoose(ConvoModel));

// Get convos by my userId
ConvoTC.setResolver(
  'connection',
  ConvoTC.getResolver('connection').addFilterArg({
    name: 'byMe',
    type: `input byMe {
        userId: String!
      }`,
    query: (rawQuery, value, resolveParams) => { // eslint-disable-line
      console.log(value);
      const { userId } = value;
      rawQuery.users = {
        $elemMatch: { userId },
      };
      rawQuery.isActivated = true;
    },
  })
);

// Chat threads, by convoId = intentId
const ConvoThreadsResolver = ThreadResolver({
  ModelTC: ConvoTC,
  extraArgs: {
    userId: GraphQLString,
  },
  extraQuery: ({ source, args, context, info }) => {
    const { userId, start, direction } = args;
    const query = {
      users: {
        $elemMatch: { userId },
      },
      isActivated: true,
      createdAt: { $ne: '1995-03-04T11:54:15.061Z' }, // ALWAYS TRUE FOR ALL DOCUMENTS, Pass fake data to Overriding the TIME,ELAPSED
    };

    // Direction here, TIME ELAPSED
    if (direction === 'old') {
      query.utime = { $lt: start };
    } else {
      // Greater than the time provided
      query.utime = { $gt: start };
    }

    return query;
  },
  extraSort: () => ({ utime: -1 }),
  Model: ConvoModel,
});
ConvoTC.addResolver(ConvoThreadsResolver);

/**
 * Checks if convo exits else create new convo
 * @param {*} userid one user
 * @param {*} userto other user
 * @returns Convo object
 */
export async function createP2Pconvo({ userId, userTo, activateIt }) {
  let userFirst;
  let userSecond;
  let newConvo;
  const isActivated = isBoolean(activateIt) ? activateIt : false;
  try {
    // checking if it exits
    newConvo = await ConvoModel.find({
      $and: [{ users: { $elemMatch: { userId } } }, { users: { $elemMatch: { userId: userTo } } }],
    }).exec();

    console.log(
      `************************************ Convos are for ${userId} ${userTo}`,
      newConvo.length
    );
    if (newConvo && newConvo.length > 0) {
      console.log('Convo already Exists');
      console.log(`ConvoID = ${newConvo[0]._id}`);
      // Return the first one
      return newConvo[0];
    }
    console.log('************************Creating a new conversation right now');

    userFirst = await UserProfileModel.findById(userId).exec();
    userSecond = await UserProfileModel.findById(userTo).exec();
    // New convo
    newConvo = {
      isActivated,
      convoType: 'p2p',
      users: [{ userId: userFirst._id }, { userId: userSecond._id }],
    };

    newConvo = new ConvoModel(newConvo);
    const save = await newConvo.save();
    if (save) {
      return newConvo;
    }
    throw new Error('Failed to save');

    return {};
    // console.log(newConvo);
    // return newConvo;
  } catch (error) {
    console.log(error);

    return null;
  }
}

/**
 * Checking if convo exists between two users
 * @param {*} me
 * @param {*} userTo
 * @returns {*} ConvoObject
 */
export async function checkIfConvoExits(me, userTo) {
  let Convos;
  try {
    // checking if it exits
    Convos = await ConvoModel.find({
      convoType: 'p2p',
      users: {
        $all: [{ $elemMatch: { userId: me } }, { $elemMatch: { userId: userTo } }],
      },
    }).exec();

    if (Convos.length > 0) {
      console.log('Convo found');
      // Return the first one
      return Convos[0];
    }
    // console.log(newConvo);
    return null;
  } catch (error) {
    console.log(error);

    return null;
  }
}

export const ConvoRecord = TypeComposer.create({
  name: 'newConvo',
  fields: {
    recordId: {
      type: GraphQLString,
      description: 'Created document ID',
    },
    record: {
      type: ConvoTC,
      description: 'Convo created',
    },
  },
});

// Check if we have a convo together, if not available create it silently
ConvoTC.addResolver(
  new Resolver({
    name: 'checkConvo',
    kind: 'query',
    type: ConvoRecord,
    args: {
      userId: {
        type: GraphQLString,
      },
      userTo: {
        type: GraphQLString,
      },
    },
    resolve: async ({ source, args, context, info }) => {
      const { userId, userTo } = args;
      let convoObject;

      try {
        if (!mongoose.Types.ObjectId.isValid(userId)) {
          // return
          throw new Error(`${userId} is not a valid ObjectID`);
        }
        convoObject = await createP2Pconvo({ userId, userTo });
        if (convoObject) {
          return {
            recordId: convoObject._id,
            record: convoObject,
          };
        }
        throw new Error('Cannot find conversation');
      } catch (error) {
        return {
          recordId: '',
          record: null,
        };
      }
    },
  })
);

// newConvo
ConvoTC.addResolver(
  new Resolver({
    name: 'newConvo',
    type: ConvoRecord,
    kind: 'mutation',
    args: {
      userId: {
        type: GraphQLString,
      },
      userTo: {
        type: GraphQLString,
      },
    },
    resolve: async ({ source, args, context, info }) => {
      const { userId, userTo } = args;
      let convo;
      console.log(args);
      try {
        convo = await createP2Pconvo({ userId, userTo });
        console.log('Got the convo');
        return { recordId: convo._id, record: convo };
      } catch (error) {
        console.log(error);

        // Return empty array
        return convo;
      }
    },
  })
);

// lastmessage
ConvoTC.addRelation('last', {
  resolver: ChatMsgTC.get('$findById'),
  prepareArgs: {
    _id: source => source.lastChatId,
  },
  projection: { _id: true },
});

// lastmessage
ConvoTC.addRelation('chatUsers', {
  resolver: UserProfileTC.getResolver('byConvoUser'),
  prepareArgs: {
    users: source => source.users,
  },
  projection: { users: true },
});

// Read conversation
ConvoTC.addResolver(
  new Resolver({
    name: 'readConvo',
    type: ConvoTC,
    kind: 'mutation',
    args: {
      userId: {
        type: GraphQLString,
      },
      convoId: {
        type: GraphQLString,
      },
    },
    resolve: async ({ source, args, context, info }) => {
      const { userId, convoId } = args;
      let userReader;
      let convo;
      console.log('Reading messages from a convo');
      try {
        // Must Have convoId,
        // Else throw
        convo = await ConvoModel.findById(convoId).exec();

        if (!convo) {
          throw new Error('Conversation not found');
        }
        // Check if users exist
        userReader = await UserProfileModel.findById(userId).exec();

        if (!userReader) {
          throw new Error('User reader not found');
        }
        await chatboxCounter({ userId, type: 'remove', convoId });
        return convo;
        // Add counter to receiver
        // addNremoveInbox({ convoId: convo._id, userId: userTo, type: 'remove' });

        // return { recordId: chatMsg._id, record: chatMsg };
      } catch (error) {
        console.log(error);

        // Return empty array
        return convo;
      }
    },
  })
);
