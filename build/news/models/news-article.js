'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.removeVCN = exports.putNewsArtVCN = exports.NewsArticleFeedTC = exports.NewsArtTC = exports.NewsArtModel = exports.newsArtSchema = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; /**
                                                                                                                                                                                                                                                                   * Created by vuga on 9/22/17.
                                                                                                                                                                                                                                                                   */


exports.newsArtsTransformer = newsArtsTransformer;

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _graphqlComposeMongoose = require('graphql-compose-mongoose');

var _graphqlComposeMongoose2 = _interopRequireDefault(_graphqlComposeMongoose);

var _graphqlCompose = require('graphql-compose');

var _graphqlComposeRelay = require('graphql-compose-relay');

var _graphqlComposeRelay2 = _interopRequireDefault(_graphqlComposeRelay);

var _graphql = require('graphql');

var _lodash = require('lodash');

var _dataTypes = require('../../utils/data-types');

var _newsPublisher = require('./news-publisher');

var _gqlThreads = require('../../utils/gql-threads');

var _CB = require('../../utils/CB');

var _util = require('util');

var _mathUtils = require('../../utils/math-utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

// const _DA = require('./static/data');
const reactionCounts = {
  counts: { type: Number, default: 0 },
  userIds: []
};

const newsArtSchema = exports.newsArtSchema = new _mongoose2.default.Schema({
  // Publisher ID
  pubid: { type: String, default: '' },

  // Article ID
  artid: { type: String, default: '' },

  // Index
  pos: { type: Number, default: 1 },

  // Featured
  featured: { type: Boolean, default: false },

  // Article banner images
  image: {
    banner: { type: String, default: '' },
    banner_rec: { type: String, default: '' },
    banner_sq: { type: String, default: '' }
  },

  // Basic info
  meta: {
    // twitter, facebook, ig, website,
    title_sm: { type: String, default: '' },
    title_og: { type: String, default: '' },
    des: { type: String, default: '' },

    url: { type: String, default: '' },
    author: { type: String, default: '' }
  },

  // contents of the post
  data: [{
    // Media ID, for all types
    medid: { type: String, default: '' },

    // Index
    pos: { type: Number, default: 1 },

    // Typo, txt,img,vid
    typo: { type: String, default: '' },

    // Text
    text: { type: String, default: '' },

    /** Begin OBJECTS ** */
    // Text object
    txt: {
      // TODO, bold,h2,h3,p,it
      style: { type: String, default: '' },

      text: { type: String, default: '' },
      quote: { type: String, default: '' }
    },

    // Image object
    img: [{
      // TODO 300x400, XxY
      size: { type: String, default: '' },

      src: { type: String, default: '' },
      cap: { type: String, default: '' }
    }],

    // Video object
    vid: [{
      // Typo, raw, yt
      typo: { type: String, default: '' },

      // TODO 300x400, WxH
      size: { type: String, default: '' },

      // Typo, txt,img,vid
      src: { type: String, default: '' },
      cap: { type: String, default: '' }
    }],

    iframe: [{
      // Typo, raw, yt
      typo: { type: String, default: '' },

      // TODO 300x400, WxH
      size: { type: String, default: '' },

      // Typo, txt,img,vid
      src: { type: String, default: '' },
      cap: { type: String, default: '' }
    }]
  }],

  // rawhtml, TODO Deprecate
  raw: [{
    typo: { type: String, default: '' },
    data: { type: String, default: '' }
  }],

  html: { type: String, default: '' },

  utime: { type: Number, default: new Date().getTime() },

  ut: {
    d: { type: Date, default: Date.now },
    dstr: { type: String, default: `${new Date().getTime()}` },
    dnum: { type: Number, default: new Date().getTime() }
  },

  views: _extends({
    cstr: { type: String, default: '0' },
    cnum: { type: Number, default: 0 } }, reactionCounts),

  comments: _extends({}, reactionCounts),

  nices: _extends({}, reactionCounts),

  shares: _extends({}, reactionCounts),
  countryCode: { type: String, default: '' }
  // createdAt: { type: Date },
  // updatedAt: { type: Date },
}, {
  collection: 'newsarts',
  timestamps: true
});

function newsArtsTransformer(items) {
  // console.log('Got these items,', items);
  if (!(0, _util.isArray)(items)) {
    items = [items];
  }
  return items.map(item => {
    return {
      _id: item._id,
      imageBanner: item.image.banner,
      html: item.raw[0].data /** Old version here * */ || item.html /** new version here * */
      , author: item.meta.author,
      url_source: item.meta.url,
      title: item.meta.title_og,
      description: item.meta.des,
      createdAt: item.createdAt || new Date(item.ut.dnum) || new Date(),
      updatedAt: item.updatedAt || new Date(item.ut.dnum) || null,
      pubId: item.pubid,

      //  Reactions
      views_counts: item.views.cnum + item.views.counts,
      comments_counts: item.comments.counts,

      // Country code
      countryCode: item.countryCode || 'RW'
    };
  });
}

const NewsArtModel = exports.NewsArtModel = _mongoose2.default.model('newsArt', newsArtSchema);

const NewsArtTC = exports.NewsArtTC = (0, _graphqlComposeRelay2.default)((0, _graphqlComposeMongoose2.default)(NewsArtModel));

const resolverByDate = new _graphqlCompose.Resolver({
  name: 'byArtDate',
  type: [NewsArtTC],
  kind: 'query',
  resolve: ({ source, args, context, info }) => {
    return NewsArtModel.find({}).sort({ 'ut.cnum': -1 }).limit(20).lean();
  }
});

const resolverTop5 = new _graphqlCompose.Resolver({
  name: 'top5',
  type: [NewsArtTC],
  kind: 'query',
  args: {
    website: {
      type: _graphql.GraphQLString
    }
  },
  resolve: ({ source, args, context, info }) => {
    // console.log(args);
    // console.log(source);
    return NewsArtModel.find({ 'meta.url': { $regex: `.*${args.website.url}.*` } }).sort({ 'ut.cnum': -1 }).limit(5).lean();
  }
});
NewsArtTC.addResolver(resolverTop5);

const resolverBy5Latest = new _graphqlCompose.Resolver({
  name: 'by5Latest',
  type: [NewsArtTC],
  kind: 'query',
  resolve: ({ source, args, context, info }) => {
    return NewsArtModel.find({}).sort({ 'ut.cnum': -1 }).limit(20).lean();
  }
});

const NewsArticleFeedTC = exports.NewsArticleFeedTC = _graphqlCompose.TypeComposer.create(`
  type NewsArticleFeed {
    _id: String
    pubId: String
    imageBanner: String
    title: String
    description: String
    html: String
    url_source: String
    author: String
    createdAt: Date
    updatedAt: Date
    views_counts: Int
    comments_counts: Int
    countryCode: String
  }
`);

/** GET ARTICLE BY ID */
const resolverById = new _graphqlCompose.Resolver({
  name: 'byId',
  type: NewsArticleFeedTC,
  kind: 'query',
  args: {
    id: { type: _graphql.GraphQLString }
  },
  resolve: (() => {
    var _ref = _asyncToGenerator(function* ({ source, args, context, info }) {
      const id = args.id;

      let article;
      try {
        article = yield NewsArtModel.findById(id).exec();
        if (!article) {
          throw new Error('Article not found');
        }

        return _extends({}, newsArtsTransformer(article)[0]);
      } catch (error) {
        console.log(error.message);
        return null;
      }
    });

    return function resolve(_x) {
      return _ref.apply(this, arguments);
    };
  })()
});
NewsArtTC.addResolver(resolverById);

const NewsArticleFeedThreadTC = _graphqlCompose.TypeComposer.create({
  name: 'NewsArticleFeedThread',
  fields: {
    pageInfo: _dataTypes.typePageInfo,
    items: [NewsArticleFeedTC]
  }
});

// publisher relation
NewsArticleFeedTC.addRelation('publisher', {
  resolver: _newsPublisher.NewsPubTC.getResolver('getOneById'),
  prepareArgs: {
    _id: source => source.pubId
  },
  projection: { _id: true }
});

NewsArtTC.addResolver(resolverByDate);

// News article threads
// let limit = 48, per feeds fetch
// let limitFeeds = 48;
const NewsArtThreadsResolver = (0, _gqlThreads.ThreadResolver)({
  Model: NewsArtModel,
  ModelTC: NewsArticleFeedTC,
  itemsFormater: items => {
    return newsArtsTransformer(items);
  },
  extraArgs: {
    countryCode: { type: _graphql.GraphQLString }
  },
  extraQuery: ({ source, args, context, info }) => {
    console.log('-------------------------------------------Arguments ', args);
    const direction = args.direction,
          start = args.start;

    let countryCode = args.countryCode || '';
    // start = new Date('2018-04-20T11:01:28.154Z');

    if (countryCode === '*' || (0, _lodash.isEmpty)(countryCode)) {
      // All articles
      countryCode = { $ne: '*' };
    }

    const query = {
      countryCode
    };

    if (direction === 'old') {
      query.$or = [{ 'ut.dnum': { $lte: new Date(start).getTime() } }];
    } else {
      // Greater than the time provided
      query.$or = [{ 'ut.dnum': { $gte: new Date(start).getTime() } }];
    }
    // query.countryCode = countryCode;
    console.log('Query parses', query);
    return query;
  },
  extraLimit: ({ source, args, context, info }) => {
    const limit = args.limit;

    if (limit < 48) {
      return 48;
    }
    return limit;
  },
  extraSort: ({ source, args, context, info }) => {
    const start = args.start;


    return {
      // createdAt: -1,
      'ut.dnum': -1
    };

    /**
    if (new Date(start) > new Date('2018-04-19T15:08:58.349Z')) {
      return { createdAt: -1 };
    }
    return { 'ut.dnum': -1 };
    * */
  }
});

NewsArtTC.addResolver(NewsArtThreadsResolver);

/**
 * VIEW-COMMENT-NICES begin from here
 * @param {*} article
 * @param {*} userId
 * @param {*} type
 */
const putUser = (() => {
  var _ref2 = _asyncToGenerator(function* (article, userId, type) {
    article[type].userIds = article[type].userIds.push(userId);
    article[type].userIds = (0, _lodash.uniq)(article[type].userIds);
    return article;
    // article[type].userIds = includes(article[type].userIds, type)
  });

  return function putUser(_x2, _x3, _x4) {
    return _ref2.apply(this, arguments);
  };
})();

const removeUser = (() => {
  var _ref3 = _asyncToGenerator(function* (article, userId, type) {
    article[type].userIds = article[type].userIds.filter(function (id) {
      return id !== userId;
    });
    // article[type].userIds = uniq(article[type].userIds);
    return article;
    // article[type].userIds = includes(article[type].userIds, type)
  });

  return function removeUser(_x5, _x6, _x7) {
    return _ref3.apply(this, arguments);
  };
})();

/**
 *
 * @param {*} user
 * @param {*} cmt
 * @returns updated articleObject and returns it parsed.
 */
const putNewsArtVCN = exports.putNewsArtVCN = (() => {
  var _ref4 = _asyncToGenerator(function* ({ userId, articleId, type }) {
    const vcmType = type;
    let article;
    let updateArticle;
    console.log(`Updating this article ${articleId} ${type}`);

    try {
      article = yield NewsArtModel.findById(articleId).exec();

      if (!article) {
        throw new Error('Article not found');
      }

      // Overide the VCN counts article counts
      switch (vcmType) {
        case _CB.TYPE_COMMENTS:
          article.comments.counts += 1;
          break;
        case _CB.TYPE_VIEWS:
          article[vcmType].counts += (0, _mathUtils.getRandomInt)(400, 1300);
          // if (article.views.counts < 0) {
          // New API CHECKER
          // article.views.counts = article.views.cnum;
          // }
          // article.views.count += 1;
          break;
        case _CB.TYPE_NICES:
          article.nices.counts += 1;
          break;
        default:
          break;
      }

      // Add user to VCN userIds to article if present
      if (userId) {
        // Add user action to the object
        article = yield putUser(article, userId, vcmType);
        // return article;
      }

      // Save the article now
      updateArticle = yield NewsArtModel.findOneAndUpdate({ _id: articleId }, article, {
        upsert: true
      }).exec();

      // Parse the article before being sent back to client
      article = yield newsArtsTransformer(article);

      console.log('Done transforming article');
      // Return the article now
      return article;
    } catch (error) {
      console.log(error);
      return article;
    }

    // return article;
  });

  return function putNewsArtVCN(_x8) {
    return _ref4.apply(this, arguments);
  };
})();

const removeVCN = exports.removeVCN = (() => {
  var _ref5 = _asyncToGenerator(function* (userId, article, vcmType) {
    switch (vcmType) {
      case _CB.TYPE_COMMENTS:
        article.comments.counts -= 1;
        break;
      case _CB.TYPE_VIEWS:
        if (article.views.counts < 0) {
          // New API CHECKER
          article.views.counts = article.views.cnum;
        }
        article.views.count -= 1;
        break;
      case _CB.TYPE_NICES:
        article.nices.counts -= 1;
        break;
      default:
        break;
    }

    article = yield removeUser(article, userId, vcmType);
    return article;
  });

  return function removeVCN(_x9, _x10, _x11) {
    return _ref5.apply(this, arguments);
  };
})();

// Add views to articles from here
NewsArtTC.addResolver(new _graphqlCompose.Resolver({
  name: 'viewArticle',
  args: _extends({}, _dataTypes.typeUserIntent, {
    articleId: _graphql.GraphQLString // Article id
  }),
  type: [NewsArticleFeedTC],
  kind: 'query',
  resolve: (() => {
    var _ref6 = _asyncToGenerator(function* ({ source, args, context, info }) {
      const intentId = args.intentId,
            userId = args.userId,
            articleId = args.articleId;

      let article;
      try {
        article = yield NewsArtModel.findById(articleId).exec();
        if (article) {
          // Put Views on this article and return it
          article = yield putNewsArtVCN({ articleId, userId, type: _CB.TYPE_VIEWS });

          return article;
        }
        return {};
      } catch (error) {
        console.log('View, articles', error.message);
        return {};
      }

      // console.log(start.getTime());
      return {};
    });

    return function resolve(_x12) {
      return _ref6.apply(this, arguments);
    };
  })()
}));

// New article public article view
NewsArtTC.addResolver(new _graphqlCompose.Resolver({
  name: 'ArticleViewMutate',
  args: _extends({}, _dataTypes.typeUserIntent, {
    articleId: _graphql.GraphQLString // Article id
  }),
  type: NewsArticleFeedTC,
  kind: 'mutation',
  resolve: (() => {
    var _ref7 = _asyncToGenerator(function* ({ source, args, context, info }) {
      const userId = args.userId,
            articleId = args.articleId;

      let article;
      try {
        article = yield NewsArtModel.findById(articleId).exec();
        if (article) {
          // Put Views on this article and return it,
          // TODO it returns an array
          article = yield putNewsArtVCN({ articleId, userId, type: _CB.TYPE_VIEWS });
          // console.log('ARTICLE', article);
          return article[0];
        }
        return {};
      } catch (error) {
        console.log('View, articles', error.message);
        return {};
      }

      // console.log(start.getTime());
      return {};
    });

    return function resolve(_x13) {
      return _ref7.apply(this, arguments);
    };
  })()
}));

// New article public article view
NewsArtTC.addResolver(new _graphqlCompose.Resolver({
  name: 'ArticleViewQuery',
  args: _extends({}, _dataTypes.typeUserIntent, {
    articleId: _graphql.GraphQLString // Article id
  }),
  type: NewsArticleFeedTC,
  kind: 'query',
  resolve: (() => {
    var _ref8 = _asyncToGenerator(function* ({ source, args, context, info }) {
      const userId = args.userId,
            articleId = args.articleId;

      let article;
      try {
        article = yield NewsArtModel.findById(articleId).exec();
        if (article) {
          // Put Views on this article and return it,
          // TODO it returns an array
          article = yield putNewsArtVCN({ articleId, userId, type: _CB.TYPE_VIEWS });
          // console.log('ARTICLE', article);
          return article[0];
        }
        return {};
      } catch (error) {
        console.log('View, articles', error.message);
        return {};
      }

      // console.log(start.getTime());
      return {};
    });

    return function resolve(_x14) {
      return _ref8.apply(this, arguments);
    };
  })()
}));