'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ViewTC = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; /**
                                                                                                                                                                                                                                                                   * Created by vuga on 9/22/17.
                                                                                                                                                                                                                                                                   */


var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _graphqlComposeMongoose = require('graphql-compose-mongoose');

var _graphqlComposeMongoose2 = _interopRequireDefault(_graphqlComposeMongoose);

var _graphqlComposeRelay = require('graphql-compose-relay');

var _graphqlComposeRelay2 = _interopRequireDefault(_graphqlComposeRelay);

var _graphqlCompose = require('graphql-compose');

var _graphql = require('graphql');

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _dataTypes = require('../../utils/data-types');

var _gqlThreads = require('../../utils/gql-threads');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const ViewSchema = new _mongoose2.default.Schema(_extends({
  // Intent ID
  intentId: { type: String, default: '' }, // Can be anything post ID, comment ID

  // User who created this nice
  userId: { type: String, default: '' }

}, _dataTypes.dataAvailability, _dataTypes.intentIdentifier), {
  collection: 'views',
  timestamps: true
});

const ViewModel = _mongoose2.default.model('View', ViewSchema);
const ViewTC = exports.ViewTC = (0, _graphqlComposeRelay2.default)((0, _graphqlComposeMongoose2.default)(ViewModel));

// Comments threads, by convoId = intentId
const ViewThreadsResolver = (0, _gqlThreads.ThreadResolver)({
  ModelTC: ViewTC,
  extraArgs: {
    intentId: { type: _graphql.GraphQLString, isRequired: true }
  },
  extraQuery: ({ source, args, context, info }) => {
    const intentId = args.intentId;

    return {
      intentId
    };
  },
  Model: ViewModel
});

ViewTC.addResolver(ViewThreadsResolver);