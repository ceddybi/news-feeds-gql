'use strict';

/**
 * Create future jobs if they don't exits
 *
 * @param {*} occurrences [] or dates
 * @param {*} cronJobId the cronjob doing this shit
 */
let createJobs = (() => {
  var _ref = _asyncToGenerator(function* (occurrences = [], cronJob) {
    try {
      if (!cronJob) {
        throw new Error('CronJob not found');
      }

      yield (0, _asyncUtils.asyncForEach)(occurrences, (() => {
        var _ref2 = _asyncToGenerator(function* (element) {
          let job = yield _pushModel.PushModel.findOne({ cronJobId: cronJob._id, utime: element }).exec();

          // console.log('Job', job);
          if (!job) {
            // creating new job
            job = new _pushModel.PushModel({
              name: `${cronJob.name} - ${new Date(element)}`,
              cronJobId: cronJob._id,
              utime: element,
              notificationId: cronJob.notificationId,
              timezone: cronJob.timezone
            });
            yield job.save();

            /** 
            // Save the job now
            await new Promise((res, rej) => {
             
              job.save((er, saved) => {
                if (saved) {
                  return res(saved);
                }
                return rej(er);
              });
            }); * */

            return console.log(`Created = ${new Date(element)}`);
          }
          return console.log(`Already created = ${new Date(element)}`);
        });

        return function (_x) {
          return _ref2.apply(this, arguments);
        };
      })());
    } catch (error) {
      return console.log(error);
    }
  });

  return function createJobs() {
    return _ref.apply(this, arguments);
  };
})();

let getOccurences = (() => {
  var _ref3 = _asyncToGenerator(function* (cronJobs) {
    const jobs = [];
    try {
      yield (0, _asyncUtils.asyncForEach)(cronJobs, (() => {
        var _ref4 = _asyncToGenerator(function* (cj) {
          const occurrences = createOccurences(cj.cronexp);
          jobs.push({ cj, occurrences });
          return { done: true };
        });

        return function (_x3) {
          return _ref4.apply(this, arguments);
        };
      })());
      // console.log('Occurences', jobs);
      return jobs;
    } catch (error) {
      console.log(error);
      return jobs;
    }
  });

  return function getOccurences(_x2) {
    return _ref3.apply(this, arguments);
  };
})();

let cronJoblist = (() => {
  var _ref5 = _asyncToGenerator(function* () {
    let cronjobs;
    try {
      cronjobs = yield _cronJobs.CronJobModel.find({ available: true }).exec();

      console.log('Cronjobs BEGIN', cronjobs.length);
      if (!cronjobs) {
        throw new Error('CronJobs null');
      }

      cronjobs = yield getOccurences(cronjobs);

      yield (0, _asyncUtils.asyncForEach)(cronjobs, (() => {
        var _ref6 = _asyncToGenerator(function* (job) {
          yield createJobs(job.occurrences, job.cj);
          return { done: true };
        });

        return function (_x4) {
          return _ref6.apply(this, arguments);
        };
      })());

      console.log('Cronjobs complete');
      return { done: true };
    } catch (error) {
      return console.log(error);
    }
  });

  return function cronJoblist() {
    return _ref5.apply(this, arguments);
  };
})();

// cronJoblist();


let startCronWorker = (() => {
  var _ref7 = _asyncToGenerator(function* (params) {
    let cronjobs;
    try {
      cronjobs = yield _cronJobs.CronJobModel.find({ available: true }).exec();

      console.log('Cronjobs BEGIN', cronjobs.length);
      if (!cronjobs) {
        throw new Error('CronJobs null');
      }
    } catch (error) {
      console.log(error);
      return startCronWorker;
    }
  });

  return function startCronWorker(_x5) {
    return _ref7.apply(this, arguments);
  };
})();

var _cronJobs = require('../models/cron-jobs');

var _later = require('later');

var _later2 = _interopRequireDefault(_later);

var _pushModel = require('../models/push-model');

var _asyncUtils = require('../../utils/async-utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

const cronJobsQueue = [];

const futureNumber = 2;
const timeoutCreator = 5000;

function createOccurences(cronexp = '0 20 * * 1-5') {
  const sched = _later2.default.parse.cron(cronexp);
  const occurrences = _later2.default.schedule(sched).next(futureNumber);
  return occurrences;
}setInterval(() => {
  cronJoblist();
}, timeoutCreator);