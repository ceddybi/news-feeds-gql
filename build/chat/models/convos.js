'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ConvoRecord = exports.checkIfConvoExits = exports.createP2Pconvo = exports.ConvoTC = exports.ConvoModel = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; /**
                                                                                                                                                                                                                                                                   * Created by vuga on 9/22/17.
                                                                                                                                                                                                                                                                   */


/**
 * Checks if convo exits else create new convo
 * @param {*} userid one user
 * @param {*} userto other user
 * @returns Convo object
 */
let createP2Pconvo = exports.createP2Pconvo = (() => {
  var _ref = _asyncToGenerator(function* ({ userId, userTo, activateIt }) {
    let userFirst;
    let userSecond;
    let newConvo;
    const isActivated = (0, _lodash.isBoolean)(activateIt) ? activateIt : false;
    try {
      // checking if it exits
      newConvo = yield ConvoModel.find({
        $and: [{ users: { $elemMatch: { userId } } }, { users: { $elemMatch: { userId: userTo } } }]
      }).exec();

      console.log(`************************************ Convos are for ${userId} ${userTo}`, newConvo.length);
      if (newConvo && newConvo.length > 0) {
        console.log('Convo already Exists');
        console.log(`ConvoID = ${newConvo[0]._id}`);
        // Return the first one
        return newConvo[0];
      }
      console.log('************************Creating a new conversation right now');

      userFirst = yield _userProfile.UserProfileModel.findById(userId).exec();
      userSecond = yield _userProfile.UserProfileModel.findById(userTo).exec();
      // New convo
      newConvo = {
        isActivated,
        convoType: 'p2p',
        users: [{ userId: userFirst._id }, { userId: userSecond._id }]
      };

      newConvo = new ConvoModel(newConvo);
      const save = yield newConvo.save();
      if (save) {
        return newConvo;
      }
      throw new Error('Failed to save');

      return {};
      // console.log(newConvo);
      // return newConvo;
    } catch (error) {
      console.log(error);

      return null;
    }
  });

  return function createP2Pconvo(_x) {
    return _ref.apply(this, arguments);
  };
})();

/**
 * Checking if convo exists between two users
 * @param {*} me
 * @param {*} userTo
 * @returns {*} ConvoObject
 */


let checkIfConvoExits = exports.checkIfConvoExits = (() => {
  var _ref2 = _asyncToGenerator(function* (me, userTo) {
    let Convos;
    try {
      // checking if it exits
      Convos = yield ConvoModel.find({
        convoType: 'p2p',
        users: {
          $all: [{ $elemMatch: { userId: me } }, { $elemMatch: { userId: userTo } }]
        }
      }).exec();

      if (Convos.length > 0) {
        console.log('Convo found');
        // Return the first one
        return Convos[0];
      }
      // console.log(newConvo);
      return null;
    } catch (error) {
      console.log(error);

      return null;
    }
  });

  return function checkIfConvoExits(_x2, _x3) {
    return _ref2.apply(this, arguments);
  };
})();

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _graphqlComposeMongoose = require('graphql-compose-mongoose');

var _graphqlComposeMongoose2 = _interopRequireDefault(_graphqlComposeMongoose);

var _graphqlComposeRelay = require('graphql-compose-relay');

var _graphqlComposeRelay2 = _interopRequireDefault(_graphqlComposeRelay);

var _graphqlCompose = require('graphql-compose');

var _graphql = require('graphql');

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _userProfile = require('../../user/models/user-profile');

var _chatMsgs = require('./chat-msgs');

var _gqlThreads = require('../../utils/gql-threads');

var _userProfile2 = require('../../user/effects/user.profile.effects');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

const convoUser = {
  userId: { type: String, default: '' },

  // unread messages
  counts: { type: Number, default: 0 },

  // Can no longer see anything about this convo
  banned: { type: Boolean, default: false },

  /**
   * If group chat, user cannot do actions, becomes an eyeball.
   * If p2p chat, user cannot do actions
   */
  suspended: { type: Boolean, default: false },

  isAdmin: { type: Boolean, default: false }, // If group chat, creator of this group
  isModerator: { type: Boolean, default: false }, // If group chat, Can add/remove users
  isEditor: { type: Boolean, default: true } // IF group chat, Can edit meta { name, profile pic}
};

const ConvoSchema = new _mongoose2.default.Schema({
  /** V3 and above */
  lastChatId: { type: String, default: '' },

  // Users in this chat
  users: [_extends({}, convoUser)],

  // p2p or grp
  convoType: { type: String, default: '' },

  // If a conversation is active or now
  isActivated: { type: Boolean, default: false },

  // Utime, the time it has been ironicaly updated
  utime: { type: Date, default: Date.now }

  /** Discontinued in V3 and above
  
  // OLD API HOES v2
  convoid: { type: String, default: '' }, // convoId
   user_id: [], // ID's of users in this chat room ['ID', 'ID']
  allowed: [],
  banned: [],
      // Time updates
  ut: _DA.ut,
   // Last message
  last: {
    userid: { type: String, default: '' },
    msg: { type: String, default: '' }
  },
      Discontinued in V3 and above
  creator: {
    userid: { type: String, default: '' },
    firstname: { type: String, default: '' },
    lastname: { type: String, default: '' },
    username: { type: String, default: '' },
    lastseen: { type: String, default: '' },
    image_path: { type: String, default: '' },
    a: { type: Boolean, default: false },
    obj: {},
    count: _DA.count,
  },
   operator: {
    userid: { type: String, default: '' },
    firstname: { type: String, default: '' },
    lastname: { type: String, default: '' },
    username: { type: String, default: '' },
    lastseen: { type: String, default: '' },
    image_path: { type: String, default: '' },
    a: { type: Boolean, default: false },
    obj: {},
    count: _DA.count,
  },
   * */
}, {
  collection: 'chatconvos',
  timestamps: true
});

const ConvoModel = exports.ConvoModel = _mongoose2.default.model('Convo', ConvoSchema);
const ConvoTC = exports.ConvoTC = (0, _graphqlComposeRelay2.default)((0, _graphqlComposeMongoose2.default)(ConvoModel));

// Get convos by my userId
ConvoTC.setResolver('connection', ConvoTC.getResolver('connection').addFilterArg({
  name: 'byMe',
  type: `input byMe {
        userId: String!
      }`,
  query: (rawQuery, value, resolveParams) => {
    // eslint-disable-line
    console.log(value);
    const userId = value.userId;

    rawQuery.users = {
      $elemMatch: { userId }
    };
    rawQuery.isActivated = true;
  }
}));

// Chat threads, by convoId = intentId
const ConvoThreadsResolver = (0, _gqlThreads.ThreadResolver)({
  ModelTC: ConvoTC,
  extraArgs: {
    userId: _graphql.GraphQLString
  },
  extraQuery: ({ source, args, context, info }) => {
    const userId = args.userId,
          start = args.start,
          direction = args.direction;

    const query = {
      users: {
        $elemMatch: { userId }
      },
      isActivated: true,
      createdAt: { $ne: '1995-03-04T11:54:15.061Z' } // ALWAYS TRUE FOR ALL DOCUMENTS, Pass fake data to Overriding the TIME,ELAPSED
    };

    // Direction here, TIME ELAPSED
    if (direction === 'old') {
      query.utime = { $lt: start };
    } else {
      // Greater than the time provided
      query.utime = { $gt: start };
    }

    return query;
  },
  extraSort: () => ({ utime: -1 }),
  Model: ConvoModel
});
ConvoTC.addResolver(ConvoThreadsResolver);const ConvoRecord = exports.ConvoRecord = _graphqlCompose.TypeComposer.create({
  name: 'newConvo',
  fields: {
    recordId: {
      type: _graphql.GraphQLString,
      description: 'Created document ID'
    },
    record: {
      type: ConvoTC,
      description: 'Convo created'
    }
  }
});

// Check if we have a convo together, if not available create it silently
ConvoTC.addResolver(new _graphqlCompose.Resolver({
  name: 'checkConvo',
  kind: 'query',
  type: ConvoRecord,
  args: {
    userId: {
      type: _graphql.GraphQLString
    },
    userTo: {
      type: _graphql.GraphQLString
    }
  },
  resolve: (() => {
    var _ref3 = _asyncToGenerator(function* ({ source, args, context, info }) {
      const userId = args.userId,
            userTo = args.userTo;

      let convoObject;

      try {
        if (!_mongoose2.default.Types.ObjectId.isValid(userId)) {
          // return
          throw new Error(`${userId} is not a valid ObjectID`);
        }
        convoObject = yield createP2Pconvo({ userId, userTo });
        if (convoObject) {
          return {
            recordId: convoObject._id,
            record: convoObject
          };
        }
        throw new Error('Cannot find conversation');
      } catch (error) {
        return {
          recordId: '',
          record: null
        };
      }
    });

    return function resolve(_x4) {
      return _ref3.apply(this, arguments);
    };
  })()
}));

// newConvo
ConvoTC.addResolver(new _graphqlCompose.Resolver({
  name: 'newConvo',
  type: ConvoRecord,
  kind: 'mutation',
  args: {
    userId: {
      type: _graphql.GraphQLString
    },
    userTo: {
      type: _graphql.GraphQLString
    }
  },
  resolve: (() => {
    var _ref4 = _asyncToGenerator(function* ({ source, args, context, info }) {
      const userId = args.userId,
            userTo = args.userTo;

      let convo;
      console.log(args);
      try {
        convo = yield createP2Pconvo({ userId, userTo });
        console.log('Got the convo');
        return { recordId: convo._id, record: convo };
      } catch (error) {
        console.log(error);

        // Return empty array
        return convo;
      }
    });

    return function resolve(_x5) {
      return _ref4.apply(this, arguments);
    };
  })()
}));

// lastmessage
ConvoTC.addRelation('last', {
  resolver: _chatMsgs.ChatMsgTC.get('$findById'),
  prepareArgs: {
    _id: source => source.lastChatId
  },
  projection: { _id: true }
});

// lastmessage
ConvoTC.addRelation('chatUsers', {
  resolver: _userProfile.UserProfileTC.getResolver('byConvoUser'),
  prepareArgs: {
    users: source => source.users
  },
  projection: { users: true }
});

// Read conversation
ConvoTC.addResolver(new _graphqlCompose.Resolver({
  name: 'readConvo',
  type: ConvoTC,
  kind: 'mutation',
  args: {
    userId: {
      type: _graphql.GraphQLString
    },
    convoId: {
      type: _graphql.GraphQLString
    }
  },
  resolve: (() => {
    var _ref5 = _asyncToGenerator(function* ({ source, args, context, info }) {
      const userId = args.userId,
            convoId = args.convoId;

      let userReader;
      let convo;
      console.log('Reading messages from a convo');
      try {
        // Must Have convoId,
        // Else throw
        convo = yield ConvoModel.findById(convoId).exec();

        if (!convo) {
          throw new Error('Conversation not found');
        }
        // Check if users exist
        userReader = yield _userProfile.UserProfileModel.findById(userId).exec();

        if (!userReader) {
          throw new Error('User reader not found');
        }
        yield (0, _userProfile2.chatboxCounter)({ userId, type: 'remove', convoId });
        return convo;
        // Add counter to receiver
        // addNremoveInbox({ convoId: convo._id, userId: userTo, type: 'remove' });

        // return { recordId: chatMsg._id, record: chatMsg };
      } catch (error) {
        console.log(error);

        // Return empty array
        return convo;
      }
    });

    return function resolve(_x6) {
      return _ref5.apply(this, arguments);
    };
  })()
}));