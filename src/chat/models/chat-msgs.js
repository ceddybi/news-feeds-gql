/**
 * Created by vuga on 9/22/17.
 */
import mongoose, { Model } from 'mongoose';
import composeWithMongoose from 'graphql-compose-mongoose';
import composeWithRelay from 'graphql-compose-relay';
import { Resolver, TypeComposer } from 'graphql-compose';
import { GraphQLList, GraphQLString, GraphQLInt, GraphQLEnumType } from 'graphql';
import _ from 'lodash';
import { dataAvailability } from '../../utils/data-types';
import { UserProfileModel } from '../../user/models/user-profile';
import { createP2Pconvo, ConvoModel, ConvoTC, ConvoRecord } from './convos';
import { ThreadResolver } from '../../utils/gql-threads';
import { pushNewChatMessage } from '../../push/helpers/push-utils';
import { addNremoveInbox, chatboxCounter } from '../../user/effects/user.profile.effects';

const ChatMsgSchema = new mongoose.Schema(
  {
    ...dataAvailability,

    // User sending the message
    userId: { type: String, default: '' },

    // Conversation ID
    convoId: { type: String, default: '' },
    sent: { type: Boolean, default: true },
    seen: { type: Boolean, default: true },

    // Text content
    message: {
      text: { type: String, default: '' },

      // Number of times it has been edited
      edits: [
        {
          text: { type: String, default: '' },
          utime: { type: String, default: '' },
        },
      ],
    },

    // Media attachments
    attachments: [
      {
        // type/subType
        mimeType: { type: String, default: '' },

        // Optional Name of this file
        name: { type: String, default: '' },

        // Optional caption of this file
        caption: { type: String, default: '' },

        // Url link to this data
        src: { type: String, default: '' },
      },
    ],
  },
  {
    collection: 'chatmsgs',
    timestamps: true,
  }
);

export const ChatMsgModel = mongoose.model('ChatMsg', ChatMsgSchema);
export const ChatMsgTC = composeWithRelay(composeWithMongoose(ChatMsgModel));

export const ChatMsgRecord = TypeComposer.create({
  name: 'newChatMsg',
  fields: {
    recordId: {
      type: GraphQLString,
      description: 'Created document ID',
    },
    record: {
      type: ChatMsgTC,
      description: 'Chat Item created',
    },
  },
});

// newChatMsg
ChatMsgTC.addResolver(
  new Resolver({
    name: 'newChatMsg',
    type: ChatMsgRecord,
    kind: 'mutation',
    args: {
      userId: {
        type: GraphQLString,
      },
      convoId: {
        type: GraphQLString,
      },
      userTo: {
        type: GraphQLString,
      },
      message: {
        type: GraphQLString,
      },
    },
    resolve: async ({ source, args, context, info }) => {
      const { userId, userTo, message, convoId } = args;
      let userSender;
      let userReceiver;
      let convo;
      let chatMsg;
      console.log(args);
      try {
        // Must Have convoId,
        // Else throw
        convo = await ConvoModel.findById(convoId).exec();

        if (!convo) {
          throw new Error('Conversation not found');
        }
        // Check if users exist
        userSender = await UserProfileModel.findById(userId).exec();
        userReceiver = await UserProfileModel.findById(userTo).exec();

        console.log(`Message from ${userSender.info.firstname}`);
        console.log(`Message to ${userReceiver.info.firstname}`);
        // create convo if not exists

        // Conversation not found
        /** if (!convo) {
          throw new Error('Conversation not found');
        }* */

        chatMsg = new ChatMsgModel({
          convoId: convo._id,
          userId,
          message: {
            text: message,
          },
        });

        // Save convo
        await chatMsg.save();

        // TODO save chat counts
        // Save the last message
        convo.lastChatId = chatMsg._id;

        // Activate the conversation.
        convo.isActivated = true;

        // Update utime now
        convo.utime = new Date();
        await convo.save();

        console.log('Saved the message');

        /** Message Effects here no need to await */
        const userObjects = [];

        // Send new push to receiver
        pushNewChatMessage({
          userId,
          userTo,
          convoId,
          message,
        });
        // Add counter to receiver
        addNremoveInbox({ convoId: convo._id, userId: userTo, type: 'add' });

        chatboxCounter({ userId: userTo, type: 'add', convoId: convo._id });

        return { recordId: chatMsg._id, record: chatMsg };
      } catch (error) {
        console.log(error);

        // Return empty array
        return convo;
      }
    },
  })
);

// Connection by convoId
ChatMsgTC.setResolver(
  'connection',
  ChatMsgTC.getResolver('connection').addFilterArg({
    name: 'byConvoId',
    type: `input byConvoId {
        convoId: String!
      }`,
    query: (rawQuery, value, resolveParams) => { // eslint-disable-line
      console.log(value);
      // console.log(resolveParams);
      // TODO GOT the context
      const { convoId } = value;
      rawQuery.convoId = convoId;
    },
  })
);

// Chat threads, by convoId = intentId
const ChatThreadsResolver = ThreadResolver({
  ModelTC: ChatMsgTC,
  extraArgs: {
    convoId: GraphQLString,
    userId: GraphQLString, // UserId id to be used to mark read!
  },
  extraQuery: ({ source, args, context, info }) => {
    const { convoId, userId } = args;
    // Remove seen messages
    // addNremoveInbox({ userId, convoId, type: 'remove' });
    return {
      convoId,
    };
  },
  Model: ChatMsgModel,
});
ChatMsgTC.addResolver(ChatThreadsResolver);
