'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LiveTvCategoryTC = exports.LiveTvTC = exports.TvModel = undefined;

var _graphqlComposeMongoose = require('graphql-compose-mongoose');

var _graphqlComposeMongoose2 = _interopRequireDefault(_graphqlComposeMongoose);

var _graphqlCompose = require('graphql-compose');

var _graphqlComposeRelay = require('graphql-compose-relay');

var _graphqlComposeRelay2 = _interopRequireDefault(_graphqlComposeRelay);

var _graphql = require('graphql');

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

/**
 * Created by vuga on 9/22/17.
 */
const mongoose = require('mongoose');

// Default preference countries here,
const defPrefCountrys = ['RW', 'KE', 'UG', 'TZ', 'CD', 'BI'];

const TvSchema = new mongoose.Schema({
  tvid: { type: String, default: '' },
  title: { type: String, default: '' },
  description: { type: String, default: '' },
  typo: { type: String, default: '' },

  // target category
  countryCode: { type: String, default: '' },

  // isWebView
  isWebView: { type: Boolean, default: false },

  meta: { type: String, default: '' },
  link: { type: String, default: '' },
  featured: { type: Number, default: 1 },
  veri: { type: Number, default: 1 },
  int_date: { type: Number, default: 0 },
  updated_at: { type: Number, default: 0 },
  lock: { type: String, default: '' },
  lockCode: { type: String, default: '' },
  youtubeChannel: { type: String, default: '' },
  image_path: { type: String, default: '' },
  created_at: { type: Date, default: Date.now },
  available: { type: Boolean, default: true }
}, {
  collection: 'tvs',
  timestamps: true
});
const TvModel = exports.TvModel = mongoose.model('Tv', TvSchema);
TvSchema.index({ tvid: 1 });
const LiveTvTC = exports.LiveTvTC = (0, _graphqlComposeRelay2.default)((0, _graphqlComposeMongoose2.default)(TvModel));

const TvCategorySchema = new mongoose.Schema({
  tvs: { type: [TvSchema] },
  countryCode: { type: String, default: '' },
  title: { type: String, default: '' },
  index: { type: Number, default: 0 }
}, {
  collection: 'tvcategories'
});
const tvCategoryModel = mongoose.model('Tvcat', TvCategorySchema);
const LiveTvCategoryTC = exports.LiveTvCategoryTC = (0, _graphqlComposeRelay2.default)((0, _graphqlComposeMongoose2.default)(tvCategoryModel));

const resolverByDate = new _graphqlCompose.Resolver({
  name: 'categoryCountry',
  type: [LiveTvCategoryTC],
  kind: 'query',
  args: {
    countrys: {
      type: [_graphql.GraphQLString]
    }
  },
  resolve: (() => {
    var _ref = _asyncToGenerator(function* ({ source, args, context, info }) {
      let tvs = [];
      const tvscat = [];
      try {
        tvs = yield TvModel.find({ available: true }).sort({ featured: -1 }).lean();

        // Check if we have Preference countries
        if (args.countrys && !_lodash2.default.isEmpty(args.countrys)) {
          args.countrys.map(function (code, index) {
            tvscat.push({
              tvs: _lodash2.default.filter(tvs, { countryCode: code }),
              countryCode: code,
              index,
              title: code // TODO Add countries name
            });
            return code;
          });
          return tvscat;
        }

        // Return default countries order
        defPrefCountrys.map(function (code, index) {
          tvscat.push({
            tvs: _lodash2.default.filter(tvs, { countryCode: code }),
            countryCode: code,
            index: `${index}`,
            title: code // TODO Add countries name
          });
          return code;
        });
        return tvscat;
      } catch (error) {
        console.log(error);

        // Return empty array
        return tvscat;
      }
    });

    return function resolve(_x) {
      return _ref.apply(this, arguments);
    };
  })()
});

LiveTvTC.addResolver(resolverByDate);