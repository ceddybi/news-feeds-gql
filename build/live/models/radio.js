'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LiveRadioTC = exports.RadioModel = undefined;

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _graphqlComposeMongoose = require('graphql-compose-mongoose');

var _graphqlComposeMongoose2 = _interopRequireDefault(_graphqlComposeMongoose);

var _graphqlComposeRelay = require('graphql-compose-relay');

var _graphqlComposeRelay2 = _interopRequireDefault(_graphqlComposeRelay);

var _graphqlCompose = require('graphql-compose');

var _graphql = require('graphql');

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * Created by vuga on 9/22/17.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            */


const defPrefCountrys = ['RW', 'KE', 'UG', 'TZ'];

const RadioSchema = new _mongoose2.default.Schema({
  rid: { type: String, default: '' },
  title: { type: String, default: '' },
  description: { type: String, default: '' },
  meta: { type: String, default: '' },
  typo: { type: String, default: '' },

  // target category
  countryCode: { type: String, default: '' },

  // isWebView
  isWebView: { type: Boolean, default: false },

  veri: { type: Number, default: 1 },
  featured: { type: Number, default: 1 },
  int_date: { type: Number, default: 0 },
  updated_at: { type: Number, default: 0 },
  link: { type: String, default: '' },
  lock: { type: String, default: '' },
  lockCode: { type: String, default: '' },
  youtubeChannel: { type: String, default: '' },
  image_path: { type: String, default: '' },
  created_at: { type: Date, default: Date.now },
  available: { type: Boolean, default: true }
}, {
  collection: 'radios',
  timestamps: true
});

const RadioModel = exports.RadioModel = _mongoose2.default.model('Radio', RadioSchema);
const LiveRadioTC = exports.LiveRadioTC = (0, _graphqlComposeRelay2.default)((0, _graphqlComposeMongoose2.default)(RadioModel));

const RadioCategorySchema = new _mongoose2.default.Schema({
  radios: { type: [RadioSchema] },
  title: { type: String },
  index: { type: Number },
  countryCode: { type: String }
}, {
  collection: 'Radiocategory'
});

const RadioCategoryModel = _mongoose2.default.model('RadioCategory', RadioCategorySchema);
const LiveRadioCategoryTC = (0, _graphqlComposeRelay2.default)((0, _graphqlComposeMongoose2.default)(RadioCategoryModel));

const resolverByDate = new _graphqlCompose.Resolver({
  name: 'categoryCountry',
  type: [LiveRadioCategoryTC],
  kind: 'query',
  args: {
    countrys: {
      type: [_graphql.GraphQLString]
    }
  },
  resolve: (() => {
    var _ref = _asyncToGenerator(function* ({ source, args, context, info }) {
      let radios = [];
      const radioCats = [];
      try {
        radios = yield RadioModel.find({ available: true }).sort({ featured: -1 }).lean();

        // Check if we have Preference countries
        if (args.countrys && !_lodash2.default.isEmpty(args.countrys)) {
          args.countrys.map(function (code, index) {
            radioCats.push({
              radios: _lodash2.default.filter(radios, { countryCode: code }),
              countryCode: code,
              index,
              title: code // TODO Add countries name
            });
            return code;
          });
          return radioCats;
        }

        // Return default countries order
        defPrefCountrys.map(function (code, index) {
          radioCats.push({
            radios: _lodash2.default.filter(radios, { countryCode: code }),
            countryCode: code,
            index: `${index}`,
            title: code // TODO Add countries name
          });
          return code;
        });
        return radioCats;
      } catch (error) {
        console.log(error);

        // Return empty array
        return radioCats;
      }
    });

    return function resolve(_x) {
      return _ref.apply(this, arguments);
    };
  })()
});

LiveRadioTC.addResolver(resolverByDate);