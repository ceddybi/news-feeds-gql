/**
 * Created by vuga on 9/22/17.
 */
import mongoose from 'mongoose';
import composeWithMongoose from 'graphql-compose-mongoose';
import composeWithRelay from 'graphql-compose-relay';
import { Resolver } from 'graphql-compose';
import { GraphQLList, GraphQLString, GraphQLInt, GraphQLEnumType } from 'graphql';
import _ from 'lodash';

const user = {
  userid: { type: String, default: '' },
};
const NiceSchema = new mongoose.Schema(
  {
    // Intent ID
    intentId: { type: String, default: '' }, // Can be anything post ID, comment ID
    // User who created this nice
    user,

    utime: { type: Number, default: new Date().getTime() },
  },
  {
    collection: 'nices',
  }
);

const NiceModel = mongoose.model('Nices', NiceSchema);
export const NiceTC = composeWithRelay(composeWithMongoose(NiceModel));

const resolverByIntentId = new Resolver({
  name: 'byIntentId',
  type: [NiceTC],
  kind: 'query',
  args: {
    /** custom: [{
      field: { type: GraphQLString },
      value: { type: GraphQLString },
    }], // Can be used to get specif comments using special filters * */
    intentId: { type: GraphQLString }, // Required context
    start: { type: GraphQLInt }, // start from this timestamp, will default to current time
    end: { type: GraphQLInt }, // optional field to end results from here
    limit: { type: GraphQLInt }, // limit number of comments i need, default to 20 if query less than 10
    direction: { type: GraphQLString }, // either new or old
  },

  resolve: async ({ source, args, context, info }) => {
    let nix = [];
    const query = {};

    const start = args.start || new Date().getTime();
    const end = args.start || new Date().getTime();
    const direction = args.direction || 'old';

    const limit = args.limit && args.limit > 10 ? args.limit : 20;

    /** if(!isEmpty(args.custom)){
      // Put all customs to query object
      args.custom.map(cus => {
        query[cus.field] = cus.value;
      })
    } * */
    // Check direction
    if (direction === 'old') {
      // Less than the time provided
      query.utime = { $lt: start };
    } else {
      // Greater than the time provided
      query.utime = { $gt: start };
    }

    try {
      nix = await NiceModel.find({ intentId: args.intentId, ...query })
        .lean()
        .limit(limit)
        .exec();
      return nix;
    } catch (error) {
      console.log(error);
      return null;
    }
  },
});

NiceTC.addResolver(resolverByIntentId);
