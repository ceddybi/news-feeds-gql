/**
 * Created by vuga on 9/22/17.
 */
const mongoose = require('mongoose');

import composeWithMongoose from 'graphql-compose-mongoose';
import { Resolver } from 'graphql-compose';
import composeWithRelay from 'graphql-compose-relay';
import { GraphQLList, GraphQLString } from 'graphql';
import _ from 'lodash';

// Default preference countries here,
const defPrefCountrys = ['RW', 'KE', 'UG', 'TZ', 'CD', 'BI'];

const TvSchema = new mongoose.Schema(
  {
    tvid: { type: String, default: '' },
    title: { type: String, default: '' },
    description: { type: String, default: '' },
    typo: { type: String, default: '' },

    // target category
    countryCode: { type: String, default: '' },

    // isWebView
    isWebView: { type: Boolean, default: false },

    meta: { type: String, default: '' },
    link: { type: String, default: '' },
    featured: { type: Number, default: 1 },
    veri: { type: Number, default: 1 },
    int_date: { type: Number, default: 0 },
    updated_at: { type: Number, default: 0 },
    lock: { type: String, default: '' },
    lockCode: { type: String, default: '' },
    youtubeChannel: { type: String, default: '' },
    image_path: { type: String, default: '' },
    created_at: { type: Date, default: Date.now },
    available: { type: Boolean, default: true },
  },
  {
    collection: 'tvs',
    timestamps: true,
  }
);
export const TvModel = mongoose.model('Tv', TvSchema);
TvSchema.index({ tvid: 1 });
export const LiveTvTC = composeWithRelay(composeWithMongoose(TvModel));

const TvCategorySchema = new mongoose.Schema(
  {
    tvs: { type: [TvSchema] },
    countryCode: { type: String, default: '' },
    title: { type: String, default: '' },
    index: { type: Number, default: 0 },
  },
  {
    collection: 'tvcategories',
  }
);
const tvCategoryModel = mongoose.model('Tvcat', TvCategorySchema);
export const LiveTvCategoryTC = composeWithRelay(composeWithMongoose(tvCategoryModel));

const resolverByDate = new Resolver({
  name: 'categoryCountry',
  type: [LiveTvCategoryTC],
  kind: 'query',
  args: {
    countrys: {
      type: [GraphQLString],
    },
  },
  resolve: async ({ source, args, context, info }) => {
    let tvs = [];
    const tvscat = [];
    try {
      tvs = await TvModel
        .find({ available: true })
        .sort({ featured: -1 })
        .lean();

      // Check if we have Preference countries
      if (args.countrys && !_.isEmpty(args.countrys)) {
        args.countrys.map((code, index) => {
          tvscat.push({
            tvs: _.filter(tvs, { countryCode: code }),
            countryCode: code,
            index,
            title: code, // TODO Add countries name
          });
          return code;
        });
        return tvscat;
      }

      // Return default countries order
      defPrefCountrys.map((code, index) => {
        tvscat.push({
          tvs: _.filter(tvs, { countryCode: code }),
          countryCode: code,
          index: `${index}`,
          title: code, // TODO Add countries name
        });
        return code;
      });
      return tvscat;
    } catch (error) {
      console.log(error);

      // Return empty array
      return tvscat;
    }
  },
});

LiveTvTC.addResolver(resolverByDate);
