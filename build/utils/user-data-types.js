'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.randomToken = undefined;
exports.returnUser = returnUser;
exports.returnMyUserObject = returnMyUserObject;

var _randToken = require('rand-token');

var _randToken2 = _interopRequireDefault(_randToken);

var _lodash = require('lodash');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const data = {
  device: {
    wlan: { type: String, default: '' },
    imei: { type: String, default: '' },
    osid: { type: String, default: '' },
    osName: { type: String, default: '' },
    pushtoken: { type: String, default: '' },
    unique: { type: String, default: '' },
    appversion: { type: String, default: '13' },
    active: {
      last: { type: String, default: '' },
      logged: { type: String, default: '' }
    },
    extra: {}
  },

  info: {
    // top level
    a: { type: String, default: '' },

    d: { type: Boolean, default: false },
    dd: { type: String, default: '' },

    // Init data
    isVerify: { type: String, default: '' },
    reg: { type: Date, default: new Date() },
    reg_unix_int: { type: Number, default: new Date().getTime() },

    // basic info
    userid: { type: String, default: '' },
    username: { type: String, default: '' },
    firstname: { type: String, default: '' },
    lastname: { type: String, default: '' },
    nickname: { type: String, default: '' },
    sex: { type: String, default: '' },
    email: { type: String, default: '' },
    phone: { type: String, default: '' },
    image_path: { type: String, default: '' },
    address: { type: String, default: '' },

    // Short bio
    bio: { type: String, default: '' },

    gmt: { type: String, default: 'no' },
    ethic: { type: String, default: '' },
    interest: [{ type: String, default: '' }],

    // Birthday
    bday: {
      day: { type: String, default: '' },
      month: { type: String, default: '' },
      year: { type: String, default: '' }
    }
  },

  // location
  location: {
    // GEO
    longitude: { type: Number, default: 0 },
    latitude: { type: Number, default: 0 },

    // ADDRESS
    formattedAddress: { type: String, default: '' },
    streetName: { type: String, default: '' },
    city: { type: String, default: '' },
    country: { type: String, default: '' },
    countryCode: { type: String, default: '' },

    extra: {},
    administrativeLevels: {}
  },

  // Universal counter
  count: {
    cstr: { type: String, default: '0' },
    cnum: { type: Number, default: 0 }
  },

  inbox: {
    convos: [''],
    counts: { type: Number, default: 0 }
  }
};

exports.default = data;
const randomToken = exports.randomToken = num => {
  const n = num || 20;
  return _randToken2.default.generate(n);
};

function returnUser(ud) {
  if (!(0, _lodash.isEmpty)(ud)) {
    return {
      userId: ud._id,
      // userid: ud.info.userid,
      firstname: ud.info.firstname,
      lastname: ud.info.lastname,
      username: ud.info.username,
      lastactive: ud.utime || ud.updatedAt,
      nickname: ud.info.username,
      image_path: ud.info.image_path,
      sex: ud.info.sex,
      bio: ud.info.bio,
      address: ud.info.address
    };
  }
  return {};
}

function returnMyUserObject(ud) {
  if (!(0, _lodash.isEmpty)(ud)) {
    return {
      userId: ud._id,
      firstname: ud.info.firstname,
      lastname: ud.info.lastname,
      username: ud.info.username,
      nickname: ud.info.username,
      bio: ud.info.bio,
      address: ud.info.address,
      image_path: ud.info.image_path,
      lastactive: ud.utime || ud.updatedAt,
      sex: ud.info.sex,
      inbox: ud.inbox,
      chatbox: ud.chatbox
    };
  }
  return {};
}