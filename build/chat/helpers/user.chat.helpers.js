'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.addFirstChatWithAdmin = undefined;

let addFirstChatWithAdmin = exports.addFirstChatWithAdmin = (() => {
  var _ref = _asyncToGenerator(function* (userId) {
    let user;
    let userAdmin;
    let convo;
    let chatMsg;
    let existingMessagesWithAdmin;
    try {
      userAdmin = yield _userProfile.UserProfileModel.findOne({ 'info.username': 'vs' }).exec();
      user = yield _userProfile.UserProfileModel.findById(userId).exec();

      if (!user) {
        throw new Error('User not found to create first convo');
      }
      if (!userAdmin) {
        throw new Error('Admin not found to create first convo');
      }

      // create convo if not exists
      convo = yield (0, _convos.createP2Pconvo)({ userId, userTo: userAdmin._id });

      // Conversation not found
      if (!convo) {
        throw new Error('Conversation not found');
      }

      // Checking if conversation already has messages
      existingMessagesWithAdmin = yield _chatMsgs.ChatMsgModel.find({ convoId: convo._id }).exec();
      if (existingMessagesWithAdmin.length > 0 || !(0, _lodash.isEmpty)(existingMessagesWithAdmin)) {
        throw new Error('Conversation already has initital messages, ignoring this now');
      }

      chatMsg = new _chatMsgs.ChatMsgModel({
        convoId: convo._id,
        userId: userAdmin._id,
        message: {
          text: WelcomeMessage
        }
      });

      // Save convo
      yield chatMsg.save();

      // TODO save chat counts
      // Save the last message
      convo.lastChatId = chatMsg._id;

      // Activate the conversation.
      convo.isActivated = true;
      yield convo.save();

      console.log('Saved the first message');

      /** Message Effects here no need to await */

      // Send new push to receiver
      // NO PUSH TO USER NOW
      // pushNewChatMessage({ userId, userTo, convoId: convo._id, message });

      // Add counter to receiver
      (0, _userProfile2.addNremoveInbox)({ convoId: convo._id, userId, type: 'add' });

      return { done: true, recordId: chatMsg._id, record: chatMsg };
    } catch (error) {
      console.log(error.message);
      return { done: false };
    }
  });

  return function addFirstChatWithAdmin(_x) {
    return _ref.apply(this, arguments);
  };
})();

var _lodash = require('lodash');

var _userProfile = require('../../user/models/user-profile');

var _convos = require('../models/convos');

var _chatMsgs = require('../models/chat-msgs');

var _userProfile2 = require('../../user/effects/user.profile.effects');

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

const WelcomeMessage = `Welcome to Vuga! We have improved our services please feel free ask us anything we are always here to help, \n Thank you so much ♥️`;