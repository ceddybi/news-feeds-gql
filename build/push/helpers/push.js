'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getDataTypo = exports.sendCustomPush = exports.AsyncPush = undefined;

let AsyncPush = exports.AsyncPush = (() => {
  var _ref = _asyncToGenerator(function* ({ ids, push, clientType }) {
    // const pus = push;
    // var r = res;
    const arrSize = (0, _lodash.size)(ids); // number of groups
    console.log(`Ids chunks = ${arrSize}`);

    let pushNow;
    // If zero return now
    if (arrSize <= 0) {
      return { done: true };
    }

    const toBeUsed = ids[0];
    const d = {
      ids: toBeUsed,
      push,
      clientType
    };
    console.log(`Group # ${count}`);

    try {
      pushNow = yield pushIt(d);

      if (pushNow) {
        ids.splice(0, 1);
        count += 1;

        if ((0, _lodash.size)(ids) > 0) {
          // We have more pushes so let's continue looping
          console.log(`Next is ${count}`);
          return yield AsyncPush({ ids, push, clientType }); // run it again
        }
        console.log(`Finished sending push for ${clientType}`);
        return { done: true };
      }
      return { done: true };
    } catch (error) {
      console.log(error.message);
      return { done: false };
    }
  });

  return function AsyncPush(_x) {
    return _ref.apply(this, arguments);
  };
})();

/**
 * For custom push
 * */
let sendCustomPush = exports.sendCustomPush = (() => {
  var _ref2 = _asyncToGenerator(function* (query, pushObject) {
    let resu;

    let d;
    let ac;
    let push;
    let chk;
    let ids;

    const title = pushObject.title,
          msg = pushObject.msg,
          sound = pushObject.sound,
          click_action = pushObject.click_action,
          data = pushObject.data;


    try {
      resu = yield UserDevice.find(query).lean().exec();
      console.log(`User devices = ${_lodash2.default.size(resu)}`);
      // console.log("Req typo = " + typo);
      ids = _lodash2.default.map(resu, 'info.pushtoken');
      console.log(`IDs = ${_lodash2.default.size(ids)}`);
      chk = _lodash2.default.chunk(ids, 1000);
      console.log(`Chunks = ${_lodash2.default.size(chk)}`);
      // push = {};
      // ac = req.body.typo.toUpperCase();
      push = {
        title,
        message: msg,
        sound: sound || 'vuga_zing',
        click_action: click_action || 'APP',
        collapse_key: title,
        data
      };

      console.log('Push ================ ');
      console.log(push);
      LoopArrayGroups(chk, push);
    } catch (error) {
      console.log(error);
    }
  });

  return function sendCustomPush(_x2, _x3) {
    return _ref2.apply(this, arguments);
  };
})();

let getDataTypo = exports.getDataTypo = (() => {
  var _ref3 = _asyncToGenerator(function* (d) {
    const typo = d.typo;
    const id = d.typoId;
    let tv;
    let radio;
    console.log(`getDataTypo = ${typo} ${id}`);

    switch (typo) {
      case 'tv':
        tv = yield TvModel.findById(id).exec();
        if (tv) {
          return {
            typo: 'tv',
            id: tv._id,
            link: tv.link,
            image_path: tv.image_path,
            title: tv.title,
            countryCode: tv.countryCode,
            isWebView: tv.isWebView
            // obj: tv,
          };
        }
        return null;

      case 'radio':
        radio = yield RadioModel.findById(id).exec();
        if (radio) {
          return {
            typo: 'radio',
            id: radio._id,
            link: radio.link,
            image_path: radio.image_path,
            title: radio.title,
            countryCode: radio.countryCode
            // obj: radio,
          };
        }
        return null;

      default:
        // const f = ;
        return {
          id,
          typo
        };
    }
  });

  return function getDataTypo(_x4) {
    return _ref3.apply(this, arguments);
  };
})();

exports.pushIt = pushIt;

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

const UserDevice = _mongoose2.default.model('UserDevice');
const TvModel = _mongoose2.default.model('Tv');
const RadioModel = _mongoose2.default.model('Radio');

const http = require('https');

const keyAndroid = 'AIzaSyAnrVE3xfJceR2CbEGzsKNrBBaOF1_h5cU';
const keyIos = 'AIzaSyDIJjVNYHoMgk1TFM8OmQtmyHMruFsMmys';

let count = 0;

function pushIt({ ids, push, clientType }) {
  let respo;
  const key = clientType === 'ios' ? keyIos : keyAndroid;

  const payload = JSON.stringify({
    registration_ids: ids, // required
    collapse_key: push.collapse_key || 'your_collapse_key',
    notification: {
      title: push.title,
      body: push.message,
      sound: push.sound,
      click_action: push.click_action,
      icon: 'ic_l',
      color: '#ffffff'
    },
    data: push.data
  });

  const headers = {
    Host: 'fcm.googleapis.com',
    Authorization: `key=${key}`,
    'Content-Type': 'application/json',
    'Content-Length': Buffer.byteLength(payload)
  };

  const post_options = {
    host: 'fcm.googleapis.com',
    port: 443,
    path: '/fcm/send',
    method: 'POST',
    headers
  };

  return new Promise((resolve, reject) => {
    const post_req = http.request(post_options, response => {
      response.setEncoding('utf8');

      response.on('data', namedata => {
        respo = String(namedata);
      });
      response.on('end', () => {
        console.log(respo);
        resolve(respo);
      });

      response.on('error', err => {
        // respo.error = err;
        console.log(`On error ${err}`);
        reject(err);
      });
    });

    post_req.on('error', e => {
      // respo.error = e;
      // reject(e);
      console.log(`On error ${e}`);
      reject(e);
    });

    post_req.write(payload);
    post_req.end();
  });
}