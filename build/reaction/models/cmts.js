'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CommentTC = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; /**
                                                                                                                                                                                                                                                                   * Created by vuga on 9/22/17.
                                                                                                                                                                                                                                                                   */


var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _graphqlComposeMongoose = require('graphql-compose-mongoose');

var _graphqlComposeMongoose2 = _interopRequireDefault(_graphqlComposeMongoose);

var _graphqlComposeRelay = require('graphql-compose-relay');

var _graphqlComposeRelay2 = _interopRequireDefault(_graphqlComposeRelay);

var _graphqlCompose = require('graphql-compose');

var _graphql = require('graphql');

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _gqlThreads = require('../../utils/gql-threads');

var _userProfile = require('../../user/models/user-profile');

var _newsArticle = require('../../news/models/news-article');

var _CB = require('../../utils/CB');

var _radio = require('../../live/models/radio');

var _tv = require('../../live/models/tv');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function getUt() {
  return new Date().getTime();
}
const commentObject = {
  text: { type: String, default: '' },
  attachments: [{
    type: { type: String, default: '' },
    url: { type: String, default: '' }
  }]
};

const userObj = {
  userId: { type: String, default: '' },
  firstname: { type: String, default: '' },
  lastname: { type: String, default: '' },
  photo: { type: String, default: '' }
};

const CommentSchema = new _mongoose2.default.Schema(_extends({
  intentId: { type: String, default: '' }, // Can be anything post, comment, live comment
  parent: { type: String, default: '' }, // no, or id of parent
  depth: { type: Number, default: 0 }, // 0 = parent, 1 sub reply, 2 sub-reply
  nixCount: { type: Number, default: 0 }, // 0 = parent, 1 sub reply, 2 sub-reply
  // User who created this comment
  user: userObj,
  userId: { type: String, default: '' },

  sent: { type: Boolean, default: true },
  read: { type: Boolean, default: false }

}, commentObject, {

  // Inject edits history
  edits: [commentObject],
  utime: { type: Number, default: getUt() }
}), {
  collection: 'cmts',
  timestamps: true
});

const CommentsModel = _mongoose2.default.model('Comments', CommentSchema);
const CommentTC = exports.CommentTC = (0, _graphqlComposeRelay2.default)((0, _graphqlComposeMongoose2.default)(CommentsModel));

// TODO ADD ARTICLE, LIVETV & LIVERADIO to comment Object
CommentTC.addFields({
  article: [_newsArticle.NewsArticleFeedTC],
  tv: [_tv.LiveTvTC],
  radio: [_radio.LiveRadioTC]
});

const resolverByIntentId = new _graphqlCompose.Resolver({
  name: 'byIntentId',
  type: [CommentTC],
  kind: 'query',
  args: {
    /** custom: [{
      field: { type: GraphQLString },
      value: { type: GraphQLString },
    }], // Can be used to get specif comments using special filters * */
    intentId: { type: _graphql.GraphQLString }, // Required context
    start: { type: _graphql.GraphQLInt }, // start from this timestamp, will default to current time
    end: { type: _graphql.GraphQLInt }, // optional field to end results from here
    limit: { type: _graphql.GraphQLInt }, // limit number of comments i need, default to 20 if query less than 10
    direction: { type: _graphql.GraphQLString } // either new or old
  },

  resolve: (() => {
    var _ref = _asyncToGenerator(function* ({ source, args, context, info }) {
      console.log(info);
      let comments = [];
      const query = {};

      const start = args.start || new Date().getTime();
      const end = args.start || new Date().getTime();
      const direction = args.direction || 'old';

      const limit = args.limit && args.limit > 10 ? args.limit : 20;

      /** if(!isEmpty(args.custom)){
        // Put all customs to query object
        args.custom.map(cus => {
          query[cus.field] = cus.value;
        })
      }* */
      // Check direction
      if (direction === 'old') {
        // Less than the time provided
        query.createdAt = { $lt: start };
      } else {
        // Greater than the time provided
        query.createdAt = { $gt: start };
      }

      try {
        comments = yield CommentsModel.find(_extends({ intentId: args.intentId }, query)).lean().limit(limit).exec();
        return comments;
      } catch (error) {
        console.log(error);
        return null;
      }
    });

    return function resolve(_x) {
      return _ref.apply(this, arguments);
    };
  })()
});
CommentTC.addResolver(resolverByIntentId);

const resolverGetRepliesCount = new _graphqlCompose.Resolver({
  name: 'getRepliesCount',
  type: _graphql.GraphQLInt,
  kind: 'query',
  args: {
    commentId: { type: _graphql.GraphQLString } // Required context
  },

  resolve: (() => {
    var _ref2 = _asyncToGenerator(function* ({ source, args, context, info }) {
      // let replys = {count: 0, replies: []};

      try {
        // Getting all comments with parent id as argument
        comments = yield CommentsModel.find({ parent: args.commentId }).lean().exec();
        // TODO circulate
        return comments.length;
      } catch (error) {
        console.log(error);
        // return default one
        return 0;
      }
    });

    return function resolve(_x2) {
      return _ref2.apply(this, arguments);
    };
  })()
});
CommentTC.addResolver(resolverGetRepliesCount);

const resolverCreateReply = new _graphqlCompose.Resolver({
  name: 'createComment',
  type: _graphql.GraphQLInt,
  kind: 'mutation',
  args: {
    commentId: { type: _graphql.GraphQLString } // Required context
  },

  resolve: (() => {
    var _ref3 = _asyncToGenerator(function* ({ source, args, context, info }) {
      // let replys = {count: 0, replies: []};

      try {
        // Getting all comments with parent id as argument
        comments = yield CommentsModel.find({ parent: args.commentId }).lean().exec();
        // TODO circulate
        return comments.length;
      } catch (error) {
        console.log(error);
        // return default one
        return 0;
      }
    });

    return function resolve(_x3) {
      return _ref3.apply(this, arguments);
    };
  })()
});

CommentTC.addRelation('replies', {
  resolver: CommentTC.getResolver('getRepliesCount'),
  prepareArgs: {
    _id: source => ({ _id: source._id })
  },
  projection: { _id: true }
});

// lastmessage
CommentTC.addRelation('fulluser', {
  resolver: _userProfile.UserProfileTC.getResolver('viewUser'),
  prepareArgs: {
    userId: source => source.userId
  },
  projection: { users: true }
});

CommentTC.setResolver('connection', CommentTC.getResolver('connection').addFilterArg({
  name: 'byPage',
  type: `input byIntentId {
      intentId: String!
      start: Date!
      limit: Int
      direction: String
    }`,
  query: (rawQuery, value, resolveParams) => {
    // eslint-disable-line
    console.log(value);
    console.log(rawQuery);
    if (!value.intentId) return;

    const start = value.start || new Date();
    const direction = value.direction || 'old';
    // const limit = args.limit && args.limit > 10 ? args.limit : 20;
    // Check direction
    if (direction === 'old') {
      // Less than the time provided
      rawQuery.createdAt = { $lt: start };
    } else {
      // Greater than the time provided
      rawQuery.createdAt = { $gt: start };
    }
    rawQuery.intentId = value.intentId;
  }
})
// /* FOR DEBUG */
//   .debug()
// /* OR MORE PRECISELY */
//   .debugParams()
//   .debugPayload()
//   .debugExecTime()
);

// Comments threads, by convoId = intentId
const CommentThreadsResolver = (0, _gqlThreads.ThreadResolver)({
  ModelTC: CommentTC,
  extraArgs: {
    intentId: { type: _graphql.GraphQLString, isRequired: true }
  },
  extraQuery: ({ source, args, context, info }) => {
    const intentId = args.intentId;

    return {
      intentId
    };
  },
  Model: CommentsModel
});

CommentTC.addResolver(CommentThreadsResolver);

// create Comment
const createComment = new _graphqlCompose.Resolver({
  name: 'createComment',
  type: CommentTC,
  // output: CommentTC,
  kind: 'mutation',
  args: {
    intentType: { type: _graphql.GraphQLString }, // when post-article or live-tv, live-radio
    intentId: { type: _graphql.GraphQLString }, // Required context
    userId: { type: _graphql.GraphQLString }, // Required context
    text: { type: _graphql.GraphQLString } // Required context
  },

  resolve: (() => {
    var _ref4 = _asyncToGenerator(function* ({ source, args, context, info }) {
      // let replys = {count: 0, replies: []};
      const intentId = args.intentId,
            userId = args.userId,
            text = args.text;


      console.log('create new comment ', args);
      let user;
      const intentType = args.intentType || _CB.TYPE_ARTICLES;
      let newComment;
      let article;
      let tv;
      let radio;
      try {
        // Getting user
        user = yield _userProfile.UserProfileModel.findById(userId).exec();

        // Create the new comment
        newComment = new CommentsModel({
          userId,
          text,
          intentId,
          user: {
            userId,
            firstname: user.info.firstname,
            lastname: user.info.lastname
          }
        });

        yield newComment.save();

        /** UPDATE INTENT COUNTS */
        // TODO other
        if (intentType === _CB.TYPE_ARTICLES) {
          console.log('Putting comments count now');
          article = yield (0, _newsArticle.putNewsArtVCN)({ articleId: intentId, userId, type: _CB.TYPE_COMMENTS });
          // console.log('Article was ', article);
          newComment.article = article;
        }

        return newComment;
      } catch (error) {
        console.log(error);
        return null;
      }
    });

    return function resolve(_x4) {
      return _ref4.apply(this, arguments);
    };
  })()
});

CommentTC.addResolver(createComment);