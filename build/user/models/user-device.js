'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.generateNewTokenForDevice = exports.uniDeviceSync = exports.deviceSync = exports.UuserDeviceTC = exports.UserDeviceModel = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

/**
 *
 * @param {*} updateDevice { DeviceObject }
 * @param {*} query { Query }
 */
let deviceSync = exports.deviceSync = (() => {
  var _ref = _asyncToGenerator(function* ({ updateDevice, query }) {
    let device;
    console.log(updateDevice);
    console.log(query);
    try {
      device = yield UserDeviceModel.findOneAndUpdate(query, updateDevice, {
        upsert: true,
        new: true
      }).exec();
      if (device) {
        console.log('Saved device');
        console.log(device);
      } else {
        console.log('Failed to save device');
      }
    } catch (error) {
      console.log(error);
    }

    return device;
  });

  return function deviceSync(_x) {
    return _ref.apply(this, arguments);
  };
})();

/**
 *
 * @param {*} req { unique, pushtoken, osName, clientType, appversion, userId  }
 * @returns {*} deviceObject
 */


let uniDeviceSync = exports.uniDeviceSync = (() => {
  var _ref2 = _asyncToGenerator(function* ({ userDevice }) {
    const unique = userDevice.unique,
          pushtoken = userDevice.pushtoken,
          osName = userDevice.osName,
          appversion = userDevice.appversion,
          clientType = userDevice.clientType,
          userId = userDevice.userId;


    let configDevice;
    let query;
    let device;
    if (userId) {
      // We have a userId
      configDevice = {
        userId,
        typo: clientType,

        info: _extends({}, userDevice, {
          unique,
          pushtoken,
          osName,
          appversion
        })
      };
      query = { userId, 'info.unique': unique };
    } else {
      // We don't have any user session
      // Going to anonymous device
      query = { 'info.unique': unique };
      configDevice = {
        // userId: req.user._id,
        typo: clientType,

        info: _extends({}, userDevice, {
          unique,
          pushtoken,
          osName,
          appversion
        })
      };
    }

    try {
      device = yield deviceSync({ updateDevice: configDevice, query });

      if (!device) {
        throw new Error('Error syncing the device');
      }

      return device;
    } catch (error) {
      console.log(error.message);
      return device;
    }
  });

  return function uniDeviceSync(_x2) {
    return _ref2.apply(this, arguments);
  };
})();

/**
 *
 * @param {*} deviceId
 * @param {*} userId
 */


let generateNewTokenForDevice = exports.generateNewTokenForDevice = (() => {
  var _ref3 = _asyncToGenerator(function* ({ deviceId, userId }) {
    // let userObject;
    let userDeviceObject;

    try {
      /** 
      userObject = await UserProfileModel.findById(userId).exec();
      if (!userObject) {
        throw new Error('User not found');
      }
      * */

      userDeviceObject = yield UserDeviceModel.findById(deviceId).exec();
      if (!userDeviceObject) {
        throw new Error('User device not found');
      }

      // Assign new token for this user's device
      const token = _jsonwebtoken2.default.sign({
        _id: userId,
        exp: (new Date().getTime() + mobileDeviceLifetime) / 1000
      }, _config2.default.jwtSecret);

      const refreshToken = _jsonwebtoken2.default.sign({
        _id: userId,
        exp: new Date().getTime() / 1000
      }, _config2.default.jwtSecret);

      // console.log('User device = ', userDeviceObject);
      userDeviceObject.auth.token = token;
      userDeviceObject.auth.refreshToken = refreshToken;

      yield userDeviceObject.save();
      console.log('Added auth to device');
      console.log(userDeviceObject.auth);
      return userDeviceObject;
    } catch (error) {
      console.log(error.message);
      return null;
    }
  });

  return function generateNewTokenForDevice(_x3) {
    return _ref3.apply(this, arguments);
  };
})();

exports.refreshTokenFromOld = refreshTokenFromOld;

var _jsonwebtoken = require('jsonwebtoken');

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

var _jwtDecode = require('jwt-decode');

var _jwtDecode2 = _interopRequireDefault(_jwtDecode);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _graphqlComposeMongoose = require('graphql-compose-mongoose');

var _graphqlComposeMongoose2 = _interopRequireDefault(_graphqlComposeMongoose);

var _graphqlComposeRelay = require('graphql-compose-relay');

var _graphqlComposeRelay2 = _interopRequireDefault(_graphqlComposeRelay);

var _userProfile = require('../models/user-profile');

var _config = require('../../config');

var _config2 = _interopRequireDefault(_config);

var _dataTypes = require('../../utils/data-types');

var _dataTypes2 = _interopRequireDefault(_dataTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * Created by vuga on 9/22/17.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            */

// import { Resolver } from 'graphql-compose';
// import { GraphQLList, GraphQLString, GraphQLInt, GraphQLEnumType } from 'graphql';
// import _ from 'lodash';
// import { returnUser } from '../../utils/user-data-types';


const mobileDeviceLifetime = 100000; // time a device can live

const UserDeviceSchema = new _mongoose2.default.Schema({
  // User who created this device
  userId: { type: String, default: '' },

  // android, ios, web
  typo: { type: String, default: '' },

  // User device
  info: {
    wlan: { type: String, default: '' },
    deviceCountry: { type: String, default: '' },
    timezone: { type: String, default: '' },
    imei: { type: String, default: '' },
    osid: { type: String, default: '' },
    osName: { type: String, default: '' },
    pushtoken: { type: String, default: '' },
    unique: { type: String, default: '' },
    appversion: { type: String, default: '13' }
  },

  auth: {
    token: { type: String, default: '' },
    refreshToken: { type: String, default: '' },
    removed: { type: Boolean, default: false },
    suspended: { type: Boolean, default: false }
  },

  prefs: {
    pushNotifications: { type: Boolean, default: false }
  },

  utime: { type: Number, default: new Date().getTime() },

  // Poor old UT
  ut: _dataTypes2.default.ut
}, {
  collection: 'userdevices',
  timestamps: true
});

UserDeviceSchema.index({ createdAt: 1, 'ut.dnum': 1 });

const UserDeviceModel = exports.UserDeviceModel = _mongoose2.default.model('UserDevice', UserDeviceSchema);
const UuserDeviceTC = exports.UuserDeviceTC = (0, _graphqlComposeRelay2.default)((0, _graphqlComposeMongoose2.default)(UserDeviceModel));function refreshTokenFromOld(oldToken) {
  // decode the old token
  const user = (0, _jwtDecode2.default)(oldToken);

  const token = _jsonwebtoken2.default.sign({
    _id: user._id,
    exp: (new Date().getTime() + mobileDeviceLifetime) / 1000
  }, _config2.default.jwtSecret);

  /** 
  const refreshToken = jwt.sign(
    {
      _id: user._id,
      exp: new Date().getTime() / 1000,
    },
    config.jwtSecret
  );
  * */
  return { token };
}