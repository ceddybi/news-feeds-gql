'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _lodash = require('lodash');

var _querystring = require('querystring');

var _querystring2 = _interopRequireDefault(_querystring);

var _userProfile = require('../models/user-profile');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

const AWS_STORAGE_URL = 'http://vuga.s3-website-us-west-2.amazonaws.com';
const IMAGE_UPLOADER_URL = 'https://us-central1-vugainc.cloudfunctions.net/api3/upload/dp';

const router = _express2.default.Router();

const defaultImage = (sex, dim) => `${AWS_STORAGE_URL}/${dim}/img/u/default/${sex || 'male'}.jpg`;

function trimHost(url) {
  url = url.split('/').filter(f => f.length > 0);
  url.shift();
  url.shift();
  return url.join('/');
}

// Profile redirector
// Checks if user has an image_path else redirect to dummy image
// pic
router.get('/dp', (() => {
  var _ref = _asyncToGenerator(function* (req, res) {
    console.log(req.query);
    const id = req.query.id;
    const height = req.query.height || '100';

    const dim = `${height}x${height}`;
    let user;
    let sex;

    try {
      user = yield _userProfile.UserProfileModel.findOne({ _id: id }).exec();
      if (user) {
        console.log(user);
        // found user
        sex = !(0, _lodash.isEmpty)(user.info.sex) ? user.info.sex.toLowerCase() : 'male';

        const imagePath = user.info.image_path;
        if (!(0, _lodash.isEmpty)(imagePath)) {
          return res.redirect(`${AWS_STORAGE_URL}/${dim}/${trimHost(imagePath)}`);
        }

        throw new Error('User image not found');
      }

      throw new Error('User not found');

      // Throw error now,
    } catch (error) {
      console.log(error);
      return res.redirect(defaultImage(sex, dim));
    }
  });

  return function (_x, _x2) {
    return _ref.apply(this, arguments);
  };
})());

/** 
// Upload pic from here,
// get query and redirects to Lambda function to upload the pic
router.post('/dp/upload', async (req, res) => {
  console.log(req.query);
  const query = querystring.stringify(req.query);
  console.log(query);
  // let user;

  res.redirect(`${IMAGE_UPLOADER_URL}/?${query}`);
  // TODO check if profile pic exists else redirect to dummy image
  // res.redirect()
});
* */
exports.default = router;