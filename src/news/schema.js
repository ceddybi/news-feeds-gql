/* @flow */

// SINGLE SCHEMA ON SERVER
// import { GQC } from 'graphql-compose';

// MULTI SCHEMA MODE IN ONE SERVER
// create new GQC from ComposeStorage
import { ComposeStorage } from 'graphql-compose';
import composeWithRelay from 'graphql-compose-relay';

import { NewsArtTC, NewsArtModel } from './models/news-article';
import { NewsPubTC } from './models/news-publisher';

const GQC = new ComposeStorage();

composeWithRelay(GQC.rootQuery());

const ViewerTC = GQC.get('Viewer');
GQC.rootQuery().addFields({
  viewer: {
    type: ViewerTC.getType(),
    description: 'Data under client context',
    resolve: () => ({}),
  },
});

const fields = {
  articlesLatest: NewsArtTC.getResolver('byArtDate'),
  //publisherLatestArts: NewsPubTC.getResolver('byLatestArts'),
  PublisherConnection: NewsPubTC.getResolver('connection'),
  PublisherTop5: NewsPubTC.getResolver('top5'),
  ArticleConnetion: NewsArtTC.getResolver('connection'),
};

ViewerTC.addFields(fields);

GQC.rootMutation().addFields({
  createProduct: NewsArtTC.get('$createOne'),
  removeProductById: NewsArtTC.get('$removeById'),
});

export default GQC.buildSchema();
