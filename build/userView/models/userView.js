'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UserViewTC = exports.UserViewModel = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; /**
                                                                                                                                                                                                                                                                   * Created by vuga on 9/22/17.
                                                                                                                                                                                                                                                                   */


var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _graphqlComposeMongoose = require('graphql-compose-mongoose');

var _graphqlComposeMongoose2 = _interopRequireDefault(_graphqlComposeMongoose);

var _graphqlComposeRelay = require('graphql-compose-relay');

var _graphqlComposeRelay2 = _interopRequireDefault(_graphqlComposeRelay);

var _graphqlCompose = require('graphql-compose');

var _graphql = require('graphql');

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _dataTypes = require('../../utils/data-types');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const UserViewSchema = new _mongoose2.default.Schema(_extends({}, _dataTypes.dataAvailability, {

  // User
  userId: { type: String, default: '' },

  // Can be anything, user profile, post, comment, message e.t.c
  intentId: { type: String, default: '' },

  // sub of Intent Object, e.g chatSeen, chatDelievered
  intentTarget: { type: String, default: '' }
}), {
  collection: 'userviews',
  timestamps: true
});

const UserViewModel = exports.UserViewModel = _mongoose2.default.model('UserView', UserViewSchema);
const UserViewTC = exports.UserViewTC = (0, _graphqlComposeRelay2.default)((0, _graphqlComposeMongoose2.default)(UserViewModel));