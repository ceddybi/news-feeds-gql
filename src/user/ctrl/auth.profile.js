import jwt from 'jsonwebtoken';
import httpStatus from 'http-status';
import jwtDecode from 'jwt-decode';
import { isEmpty } from 'lodash';
import APIError from '../helpers/APIError';
import config from '../../config';
import { returnUser, returnMyUserObject, randomToken } from '../../utils/user-data-types';
import { UserProfileModel } from '../models/user-profile';
import { UserDeviceModel, deviceSync, uniDeviceSync } from '../models/user-device';
import { argonVerify, generatePwHash } from '../../utils/passord-utils';
import { AwsConfig } from '../../utils/config';
import { saveUniversalFile } from '../helpers/awsImageUploader';
import { addFirstChatWithAdmin } from '../../chat/helpers/user.chat.helpers';
import { AWS_STORAGE_URL } from '../../config';

const mobileDeviceLifetime = 100000; // time a device can live
const AWS = require('aws-sdk');

AWS.config = AwsConfig;
// AWS.config('./s3_config.json');
const s3Bucket = new AWS.S3({ params: { Bucket: 'vuga' } });
const defaultImage = (sex, dim) => `${AWS_STORAGE_URL}${dim}/img/u/default/${sex || 'male'}.jpg`;

function trimHost(url) {
  url = url.split('/').filter(f => f.length > 0);
  url.shift();
  url.shift();
  return url.join('/');
}

/**
 * Profile image redirect
 * @param {*} req query { id, height }
 * @param {*} res
 * @param {*} next
 */
export async function profileImage(req, res, next) {
  console.log(req.query);
  const id = req.query.id;
  const height = req.query.height || '100';

  const dim = `${height}x${height}`;
  let user;
  let sex;
  let url;

  try {
    user = await UserProfileModel.findOne({ _id: id }).exec();
    if (user) {
      // console.log(user);
      // found user
      console.log('User found for dp');
      sex = !isEmpty(user.info.sex) ? user.info.sex.toLowerCase() : 'male';
      // console.log(sex);
      const imagePath = user.info.image_path;
      if (!isEmpty(imagePath)) {
        url = `${AWS_STORAGE_URL}${dim}/${trimHost(imagePath)}`;
        console.log(url);
        return res.redirect(url);
      }

      throw new Error('User image not found');
    }

    throw new Error('User not found');

    // Throw error now,
  } catch (error) {
    console.log(error.message);
    // console.log('User not found for dp');
    // sex = sex && !isEmpty(sex) ? sex : 'male';
    return res.redirect(defaultImage(sex, dim));
  }
}

/**
 * MUST BE AUTHANICATED
 */
export async function changePassword(req, res, next) {
  const { password, userId, device } = req.body;
  console.log('change password');
  console.log(req.body);
  let user;
  try {
    if (!userId) {
      throw new Error('Unauthorized');
    }

    user = await UserProfileModel.findById(userId).exec();
    const oldHash = user.info.a;

    if (user) {
      // Argon password
      const passwordHashed = await generatePwHash(password);
      if (!passwordHashed) {
        throw new Error('Unexpected error occured, please try again');
      }
      // Update the user
      user.info.a = passwordHashed;

      // Save the user now
      await UserProfileModel.update({ _id: user._id }, user).exec();
      await addFirstChatWithAdmin(user._id);

      // Sign user's device now
      const token = jwt.sign(
        {
          _id: user._id,
          exp: new Date().getTime() + mobileDeviceLifetime / 1000,
        },
        config.jwtSecret
      );

      // const userToreturn = returnMyUserObject(user);

      const loggedDevice = {
        userId: user._id,

        info: {
          ...device,
        },
        auth: {
          token,
          refreshToken: token,
        },
      };

      // Save or update device
      await uniDeviceSync({
        userDevice: { ...loggedDevice, userId: user._id },
      });

      console.log('Old hash', oldHash);
      console.log('New hash', user.info.a);

      return res.json({
        deviceAuth: loggedDevice.auth,
        user: returnMyUserObject(user),
        status: 200,
        message: 'Successfully updated',
      });
      // return returnMyUserObject(user);
    }

    // Error
    throw new Error('Please try again later');
  } catch (error) {
    console.log(error);
    return res.json({ status: 400, message: error.message });
  }
}

/**
 * MUST BE AUTHANICATED
 */
export async function updateProfileInfo(req, res, next) {
  // const {
  //    firstname, lastname, image_path, dob, username, sex, location, status,
  //    webiste, facebook, twitter, instagram,  } = req.body.info;
  let user;
  let userNameObject;
  const { userId } = req.body;
  console.log(req.body);
  try {
    if (!userId) {
      throw new Error('Unauthorized');
    }

    user = await UserProfileModel.findById(userId)
      .lean()
      .exec();

    if (!user) {
      // Error
      throw new Error('Please try again later');
    }

    if (user) {
      // If the request contains username, first check and see
      // if it's not taken;       && username is not my own
      if (user.info.username !== req.body.info.username) {
        userNameObject = await UserProfileModel.findOne({
          'info.username': req.body.info.username,
        }).exec();

        // If username is not taken by me
        if (userNameObject) {
          throw new Error('Username is already taken');
        }
      }
      // Update the user
      user.info = { ...user.info, ...req.body.info };
      // Save profile
      await UserProfileModel.findByIdAndUpdate(user._id, user).exec();
      // console.log('Update user profile', user);
      return res.json({
        user: returnMyUserObject(user),
        status: 200,
        message: 'Successfully updated',
      });
      // return returnMyUserObject(user);
    }
  } catch (error) {
    console.log(error);
    return res.json({ status: 401, message: error.message });
  }
}

export async function base64Image(req, res, next) {
  const { imageBinary, imageType, userId, fileName } = req.body;
  const awsPath = `u/${userId}/${fileName}`;
  const buf = new Buffer.from(imageBinary.replace(/^data:image\/\w+;base64,/, ''), 'base64');

  let awsFinalUrl;
  const data = {
    Key: awsPath,
    Body: buf,
    ContentEncoding: 'base64',
    ContentType: imageType,
  };

  try {
    awsFinalUrl = await new Promise((res, rej) => {
      s3Bucket.putObject(data, (err, data) => {
        if (err) {
          console.log(err);
          console.log('Error uploading data: ', data);
          rej(err);
        } else {
          console.log('succesfully uploaded the image!');
          res(AWS_STORAGE_URL + awsPath);
        }
      });
    });

    return res.json({ status: 200, url: awsFinalUrl });
  } catch (error) {
    console.log(error);
    return res.json({ status: 400 });
  }
}

export async function uploadImage(req, res, next) {
  console.log(req.body);
  console.log(req.files);
  // console.log(req.headers);
  // console.log(req);

  const d = new Date();
  const currentTime = d.getTime();
  let imageAwsUrl;
  let value;

  try {
    if (req.files.length > 0) {
      value = req.files[0];
      const splittedName = value.originalname.split('.');
      const imageName = `_image_${currentTime}_${randomToken()}.${splittedName[splittedName.length - 1]}`;
      imageName.replace(/ /g, '%20');

      // Upload that shit!
      imageAwsUrl = await saveUniversalFile({
        filepath: value.path,
        mime: value.mimetype,
        fileName: imageName,
      });

      if (!imageAwsUrl) {
        throw new Error('Image not uploaded');
      }

      return res.json({ url: imageAwsUrl, status: 200 });
    }
    return res.json({ status: 400 });
  } catch (error) {
    console.log(error);
    return res.json({ status: 400 });
  }
}
