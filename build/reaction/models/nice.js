'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.NiceTC = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _graphqlComposeMongoose = require('graphql-compose-mongoose');

var _graphqlComposeMongoose2 = _interopRequireDefault(_graphqlComposeMongoose);

var _graphqlComposeRelay = require('graphql-compose-relay');

var _graphqlComposeRelay2 = _interopRequireDefault(_graphqlComposeRelay);

var _graphqlCompose = require('graphql-compose');

var _graphql = require('graphql');

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * Created by vuga on 9/22/17.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            */


const user = {
  userid: { type: String, default: '' }
};
const NiceSchema = new _mongoose2.default.Schema({
  // Intent ID
  intentId: { type: String, default: '' }, // Can be anything post ID, comment ID
  // User who created this nice
  user,

  utime: { type: Number, default: new Date().getTime() }
}, {
  collection: 'nices'
});

const NiceModel = _mongoose2.default.model('Nices', NiceSchema);
const NiceTC = exports.NiceTC = (0, _graphqlComposeRelay2.default)((0, _graphqlComposeMongoose2.default)(NiceModel));

const resolverByIntentId = new _graphqlCompose.Resolver({
  name: 'byIntentId',
  type: [NiceTC],
  kind: 'query',
  args: {
    /** custom: [{
      field: { type: GraphQLString },
      value: { type: GraphQLString },
    }], // Can be used to get specif comments using special filters * */
    intentId: { type: _graphql.GraphQLString }, // Required context
    start: { type: _graphql.GraphQLInt }, // start from this timestamp, will default to current time
    end: { type: _graphql.GraphQLInt }, // optional field to end results from here
    limit: { type: _graphql.GraphQLInt }, // limit number of comments i need, default to 20 if query less than 10
    direction: { type: _graphql.GraphQLString } // either new or old
  },

  resolve: (() => {
    var _ref = _asyncToGenerator(function* ({ source, args, context, info }) {
      let nix = [];
      const query = {};

      const start = args.start || new Date().getTime();
      const end = args.start || new Date().getTime();
      const direction = args.direction || 'old';

      const limit = args.limit && args.limit > 10 ? args.limit : 20;

      /** if(!isEmpty(args.custom)){
        // Put all customs to query object
        args.custom.map(cus => {
          query[cus.field] = cus.value;
        })
      } * */
      // Check direction
      if (direction === 'old') {
        // Less than the time provided
        query.utime = { $lt: start };
      } else {
        // Greater than the time provided
        query.utime = { $gt: start };
      }

      try {
        nix = yield NiceModel.find(_extends({ intentId: args.intentId }, query)).lean().limit(limit).exec();
        return nix;
      } catch (error) {
        console.log(error);
        return null;
      }
    });

    return function resolve(_x) {
      return _ref.apply(this, arguments);
    };
  })()
});

NiceTC.addResolver(resolverByIntentId);