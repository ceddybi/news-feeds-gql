import { UserDeviceModel } from '../models/user-device';
import { concat, get, compact, uniqBy, size, chunk, map, isEqual, uniqWith } from 'lodash';
import { countries } from './countryWithTZ';
import { find, includes, isEmpty } from 'lodash';
import { UserOldModel } from '../models/user-old';
import mongoose from 'mongoose';

const { ObjectId } =  mongoose.Types;

const deviceFetchLimit = 10000;


export async function getDevices({
  version,
  model,
  devices = [],
  lastId = null,
  count = 0,
  extraQuery,
}) {
  let thouDevices;
  console.log(`Count = ${count}`);
  console.log(`Devices = ${devices.length}`);
  // const sort = { createdAt: -1 };
  let query = { _id: { $lte: lastId } };

  // TODO Switch between versions to set models

  if (!model) { // IF model is null assign it now
    switch (version) {
      case '1':
        model = UserOldModel;
        break;
      case '2':
        model = UserDeviceModel;
        break;
      case '3':
        model = UserDeviceModel;
        break;
      default:
        model = UserDeviceModel;
    }
  }

  // If it's null delete the ID query
  if (lastId === null) {
    delete query._id;
  }

  // If we have more queries
  if (extraQuery) {
    query = { ...query, ...extraQuery };
  }

  try {
    thouDevices = await circleDevicesByID({ lastId, model, version, query });

    count += 1;
    if (thouDevices.data.length > 0) {
      devices = concat(devices, thouDevices.data);
      devices = compact(devices);
      // devices = uniqBy(devices, '_id');

      // Recursive
      if (thouDevices.hasNext) {
        return await getDevices({
          version,
          model,
          devices,
          lastId: thouDevices.lastId,
          count,
          extraQuery,
        });
      }
    }

    console.log(`Version ${version} complete now ${devices.length}`);
    // else stop and return devices
    return devices;
  } catch (error) {
    console.log(error.message);
    return [];
  }
}

export async function sortNChunk({ devices }) {
  try {
    // filter Android & iOS devices
    const iosDevices = devices.filter(device => device.typo === 'ios');
    const androidDevices = devices.filter(device => device.typo === 'android');

    console.log(`Got iOS ${size(iosDevices)}`);
    console.log(`Got Android ${size(androidDevices)}`);
    // Ids
    let iosIds = map(iosDevices, 'info.pushtoken');
    iosIds = uniqWith(iosIds, isEqual); // remove duplicates
    let androidIds = map(androidDevices, 'info.pushtoken');
    androidIds = uniqWith(androidIds, isEqual); // remove duplicates

    // 1000 Chunks
    const iosChunks = chunk(iosIds, 1000);
    const androidChunks = chunk(androidIds, 1000);

    return { ios: iosChunks, android: androidChunks };
  } catch (error) {
    console.log(error.message);
    return null;
  }
}

export async function getTargetQuery(target = '') {
  console.log('TARGET DEVICES', target);

  let to;
  const query = {
    $and: [],
  };

  if (isEmpty(target)) {
    console.log('Returning ALL devices');
    // returning null query
    return null;
  }

  // If we have all return now
  if (includes(target, 'all')) {
    console.log('Returning ALL devices');
    return {};
  }

  try {
    to = target;

    to.forEach(t => {
      const cccode = t.toLocaleUpperCase();
      const countryData = find(countries, { country_code: cccode });

      let tzs = countryData.timezones;
      tzs = tzs.map(tx => {
        return {
          'info.timezone': tx,
        };
      });

      query.$and.push({
        $or: [{ 'location.countryCode': cccode }, ...tzs],
      });
    });
    return query;
  } catch (err) {
    console.log(err);
    return query;
  }
}

/**
 * Circular by ID
 */
export async function circleDevicesByID({ lastId , query, model }) {
  let last;
  let lastObjectId;

  // Will be default to newer models
  if (!model) {
    model = UserDeviceModel;
  }

  let thouDevices;
  let hasNext = false;
  console.log(`Using last ${lastId}`);
  if (lastId === null) {
    delete query._id; // Remove searches with ID
  }

  try {
    thouDevices = await model
      .find(query)
      .sort({ _id: -1 })
      .limit(deviceFetchLimit + 1)
      .lean()
      .exec();

    if (thouDevices) {
      // Check if we have next
      if (thouDevices.length > deviceFetchLimit) {
        hasNext = true;
      }

      // lastItem = Object.assign({}, thouDevices[thouDevices.length - 1]);
      last = thouDevices.slice(-1).pop();
      lastObjectId = new ObjectId(last._id);
      // console.log('Last object ID', lastObjectId);

      return { lastId: lastObjectId, hasNext, data: thouDevices };
    }

    return { lastId: lastObjectId, hasNext, data: thouDevices };
  } catch (error) {
    console.log(error.message);
    return { lastId: null, hasNext, data: [] };
  }
}
