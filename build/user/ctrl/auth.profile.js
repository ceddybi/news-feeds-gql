'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.uploadImage = exports.base64Image = exports.updateProfileInfo = exports.changePassword = exports.profileImage = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

/**
 * Profile image redirect
 * @param {*} req query { id, height }
 * @param {*} res
 * @param {*} next
 */
let profileImage = exports.profileImage = (() => {
  var _ref = _asyncToGenerator(function* (req, res, next) {
    console.log(req.query);
    const id = req.query.id;
    const height = req.query.height || '100';

    const dim = `${height}x${height}`;
    let user;
    let sex;
    let url;

    try {
      user = yield _userProfile.UserProfileModel.findOne({ _id: id }).exec();
      if (user) {
        // console.log(user);
        // found user
        console.log('User found for dp');
        sex = !(0, _lodash.isEmpty)(user.info.sex) ? user.info.sex.toLowerCase() : 'male';
        // console.log(sex);
        const imagePath = user.info.image_path;
        if (!(0, _lodash.isEmpty)(imagePath)) {
          url = `${_config.AWS_STORAGE_URL}${dim}/${trimHost(imagePath)}`;
          console.log(url);
          return res.redirect(url);
        }

        throw new Error('User image not found');
      }

      throw new Error('User not found');

      // Throw error now,
    } catch (error) {
      console.log(error.message);
      // console.log('User not found for dp');
      // sex = sex && !isEmpty(sex) ? sex : 'male';
      return res.redirect(defaultImage(sex, dim));
    }
  });

  return function profileImage(_x, _x2, _x3) {
    return _ref.apply(this, arguments);
  };
})();

/**
 * MUST BE AUTHANICATED
 */


let changePassword = exports.changePassword = (() => {
  var _ref2 = _asyncToGenerator(function* (req, res, next) {
    var _req$body = req.body;
    const password = _req$body.password,
          userId = _req$body.userId,
          device = _req$body.device;

    console.log('change password');
    console.log(req.body);
    let user;
    try {
      if (!userId) {
        throw new Error('Unauthorized');
      }

      user = yield _userProfile.UserProfileModel.findById(userId).exec();
      const oldHash = user.info.a;

      if (user) {
        // Argon password
        const passwordHashed = yield (0, _passordUtils.generatePwHash)(password);
        if (!passwordHashed) {
          throw new Error('Unexpected error occured, please try again');
        }
        // Update the user
        user.info.a = passwordHashed;

        // Save the user now
        yield _userProfile.UserProfileModel.update({ _id: user._id }, user).exec();
        yield (0, _userChat.addFirstChatWithAdmin)(user._id);

        // Sign user's device now
        const token = _jsonwebtoken2.default.sign({
          _id: user._id,
          exp: new Date().getTime() + mobileDeviceLifetime / 1000
        }, _config2.default.jwtSecret);

        // const userToreturn = returnMyUserObject(user);

        const loggedDevice = {
          userId: user._id,

          info: _extends({}, device),
          auth: {
            token,
            refreshToken: token
          }
        };

        // Save or update device
        yield (0, _userDevice.uniDeviceSync)({
          userDevice: _extends({}, loggedDevice, { userId: user._id })
        });

        console.log('Old hash', oldHash);
        console.log('New hash', user.info.a);

        return res.json({
          deviceAuth: loggedDevice.auth,
          user: (0, _userDataTypes.returnMyUserObject)(user),
          status: 200,
          message: 'Successfully updated'
        });
        // return returnMyUserObject(user);
      }

      // Error
      throw new Error('Please try again later');
    } catch (error) {
      console.log(error);
      return res.json({ status: 400, message: error.message });
    }
  });

  return function changePassword(_x4, _x5, _x6) {
    return _ref2.apply(this, arguments);
  };
})();

/**
 * MUST BE AUTHANICATED
 */


let updateProfileInfo = exports.updateProfileInfo = (() => {
  var _ref3 = _asyncToGenerator(function* (req, res, next) {
    // const {
    //    firstname, lastname, image_path, dob, username, sex, location, status,
    //    webiste, facebook, twitter, instagram,  } = req.body.info;
    let user;
    let userNameObject;
    const userId = req.body.userId;

    console.log(req.body);
    try {
      if (!userId) {
        throw new Error('Unauthorized');
      }

      user = yield _userProfile.UserProfileModel.findById(userId).lean().exec();

      if (!user) {
        // Error
        throw new Error('Please try again later');
      }

      if (user) {
        // If the request contains username, first check and see
        // if it's not taken;       && username is not my own
        if (user.info.username !== req.body.info.username) {
          userNameObject = yield _userProfile.UserProfileModel.findOne({
            'info.username': req.body.info.username
          }).exec();

          // If username is not taken by me
          if (userNameObject) {
            throw new Error('Username is already taken');
          }
        }
        // Update the user
        user.info = _extends({}, user.info, req.body.info);
        // Save profile
        yield _userProfile.UserProfileModel.findByIdAndUpdate(user._id, user).exec();
        // console.log('Update user profile', user);
        return res.json({
          user: (0, _userDataTypes.returnMyUserObject)(user),
          status: 200,
          message: 'Successfully updated'
        });
        // return returnMyUserObject(user);
      }
    } catch (error) {
      console.log(error);
      return res.json({ status: 401, message: error.message });
    }
  });

  return function updateProfileInfo(_x7, _x8, _x9) {
    return _ref3.apply(this, arguments);
  };
})();

let base64Image = exports.base64Image = (() => {
  var _ref4 = _asyncToGenerator(function* (req, res, next) {
    var _req$body2 = req.body;
    const imageBinary = _req$body2.imageBinary,
          imageType = _req$body2.imageType,
          userId = _req$body2.userId,
          fileName = _req$body2.fileName;

    const awsPath = `u/${userId}/${fileName}`;
    const buf = new Buffer.from(imageBinary.replace(/^data:image\/\w+;base64,/, ''), 'base64');

    let awsFinalUrl;
    const data = {
      Key: awsPath,
      Body: buf,
      ContentEncoding: 'base64',
      ContentType: imageType
    };

    try {
      awsFinalUrl = yield new Promise(function (res, rej) {
        s3Bucket.putObject(data, function (err, data) {
          if (err) {
            console.log(err);
            console.log('Error uploading data: ', data);
            rej(err);
          } else {
            console.log('succesfully uploaded the image!');
            res(_config.AWS_STORAGE_URL + awsPath);
          }
        });
      });

      return res.json({ status: 200, url: awsFinalUrl });
    } catch (error) {
      console.log(error);
      return res.json({ status: 400 });
    }
  });

  return function base64Image(_x10, _x11, _x12) {
    return _ref4.apply(this, arguments);
  };
})();

let uploadImage = exports.uploadImage = (() => {
  var _ref5 = _asyncToGenerator(function* (req, res, next) {
    console.log(req.body);
    console.log(req.files);
    // console.log(req.headers);
    // console.log(req);

    const d = new Date();
    const currentTime = d.getTime();
    let imageAwsUrl;
    let value;

    try {
      if (req.files.length > 0) {
        value = req.files[0];
        const splittedName = value.originalname.split('.');
        const imageName = `_image_${currentTime}_${(0, _userDataTypes.randomToken)()}.${splittedName[splittedName.length - 1]}`;
        imageName.replace(/ /g, '%20');

        // Upload that shit!
        imageAwsUrl = yield (0, _awsImageUploader.saveUniversalFile)({
          filepath: value.path,
          mime: value.mimetype,
          fileName: imageName
        });

        if (!imageAwsUrl) {
          throw new Error('Image not uploaded');
        }

        return res.json({ url: imageAwsUrl, status: 200 });
      }
      return res.json({ status: 400 });
    } catch (error) {
      console.log(error);
      return res.json({ status: 400 });
    }
  });

  return function uploadImage(_x13, _x14, _x15) {
    return _ref5.apply(this, arguments);
  };
})();

var _jsonwebtoken = require('jsonwebtoken');

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

var _httpStatus = require('http-status');

var _httpStatus2 = _interopRequireDefault(_httpStatus);

var _jwtDecode = require('jwt-decode');

var _jwtDecode2 = _interopRequireDefault(_jwtDecode);

var _lodash = require('lodash');

var _APIError = require('../helpers/APIError');

var _APIError2 = _interopRequireDefault(_APIError);

var _config = require('../../config');

var _config2 = _interopRequireDefault(_config);

var _userDataTypes = require('../../utils/user-data-types');

var _userProfile = require('../models/user-profile');

var _userDevice = require('../models/user-device');

var _passordUtils = require('../../utils/passord-utils');

var _config3 = require('../../utils/config');

var _awsImageUploader = require('../helpers/awsImageUploader');

var _userChat = require('../../chat/helpers/user.chat.helpers');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

const mobileDeviceLifetime = 100000; // time a device can live
const AWS = require('aws-sdk');

AWS.config = _config3.AwsConfig;
// AWS.config('./s3_config.json');
const s3Bucket = new AWS.S3({ params: { Bucket: 'vuga' } });
const defaultImage = (sex, dim) => `${_config.AWS_STORAGE_URL}${dim}/img/u/default/${sex || 'male'}.jpg`;

function trimHost(url) {
  url = url.split('/').filter(f => f.length > 0);
  url.shift();
  url.shift();
  return url.join('/');
}