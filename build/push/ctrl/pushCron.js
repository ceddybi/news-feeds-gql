'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.pushCronJob = exports.startPushCrons = undefined;

let startPushCrons = exports.startPushCrons = (() => {
  var _ref = _asyncToGenerator(function* () {
    let cronjobs;
    // let notification;
    try {
      cronjobs = yield _cronJobs.CronJobModel.find({ available: true })
      // .populate('cronJobId')
      // .populate('notifcationId')
      .exec();

      if (!cronjobs) {
        throw new Error('Cron Jobs not found');
      }

      (0, _asyncUtils.asyncForEach)(cronjobs, (() => {
        var _ref2 = _asyncToGenerator(function* (cj) {
          return (0, _pushCronManager.bind)({
            cronTime: cj.cronexp,
            id: cj._id,
            timeZone: cj.timezone,
            cb: (() => {
              var _ref3 = _asyncToGenerator(function* () {
                const notification = yield _notifications.NotificationModel.findById(cj.notificationId);
                if (!notification) {
                  return { done: true };
                }
                const title = notification.title,
                      message = notification.message,
                      typo = notification.typo,
                      typoId = notification.typoId;
                // console.log(cj);

                console.log('Notification', { typo, typoId, message, title });
                if (typo && typoId && title && message) {
                  return (0, _push.SendPushAuto)({
                    title,
                    message,
                    sound: 'default',
                    typo,
                    typoId,
                    target: cj.target
                  });
                }
                // console.log('Error notification is null');
              });

              return function cb() {
                return _ref3.apply(this, arguments);
              };
            })()
          });
        });

        return function (_x) {
          return _ref2.apply(this, arguments);
        };
      })());

      console.log('Cronjobs completed = ', cronjobs.length);
    } catch (error) {
      return console.log(error);
    }
  });

  return function startPushCrons() {
    return _ref.apply(this, arguments);
  };
})();

let pushCronJob = exports.pushCronJob = (() => {
  var _ref4 = _asyncToGenerator(function* (cj) {
    if (!process.env.CRONS || process.env.CRONS <= 0) {
      return console.log('CronJobs are not enabled on this server');
      // return;
    }

    console.log('Updating cronjob');

    return (0, _pushCronManager.bind)({
      cronTime: cj.cronexp,
      id: cj._id,
      timeZone: cj.timezone,
      cb: (() => {
        var _ref5 = _asyncToGenerator(function* () {
          const notification = yield _notifications.NotificationModel.findById(cj.notificationId);
          if (!notification) {
            return { done: true };
          }
          const title = notification.title,
                message = notification.message,
                typo = notification.typo,
                typoId = notification.typoId;
          // console.log(cj);

          console.log('Notification', { typo, typoId, message, title });
          if (typo && typoId && title && message) {
            return (0, _push.SendPushAuto)({
              title,
              message,
              sound: 'default',
              typo,
              typoId,
              target: cj.target
            });
          }
          // console.log('Error notification is null');
        });

        return function cb() {
          return _ref5.apply(this, arguments);
        };
      })()
    });
  });

  return function pushCronJob(_x2) {
    return _ref4.apply(this, arguments);
  };
})();

// Listen for


var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _cronJobs = require('../models/cron-jobs');

var _pushCronManager = require('./pushCron-manager');

var _push = require('./push.controller');

var _notifications = require('../models/notifications');

var _asyncUtils = require('../../utils/async-utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

require('dotenv').config();

_mongoose2.default.connection.once('open', () => {
  console.log(`MongoDB successfully connected from pushCron`);
  if (process.env.CRONS && process.env.CRONS > 0) {
    console.log('CRONS = ', process.env.CRONS);
    return startPushCrons();
  }
  console.log('Crons are not enabled');
});