/* @flow */

import mongoose from 'mongoose';
// import { mongoUri } from './src/config';
import { NewsArtModel } from './src/news/models/news-article';
import { get, isEmpty } from 'lodash';
import { NewsPublisherModel } from './src/news/models/news-publisher';
import xpath from 'xpath';
import xmldom, { DOMParser } from 'xmldom';
import request from 'request';

const AWS_VUGA_CACHE = 'http://vuga.s3-website-us-west-2.amazonaws.com/';

require('dotenv').config();

const mongoUri = process.env.MONGODB_URI;
console.log(mongoUri);
mongoose.Promise = Promise;

const opts = {
  autoReconnect: true,
  reconnectTries: Number.MAX_VALUE,
  reconnectInterval: 1000,
  // useMongoClient: true,
};

mongoose.connect(mongoUri, opts);

export const { connection } = mongoose;
connection.on('error', e => {
  if (e.message.code === 'ETIMEDOUT') {
    console.log(e);
    mongoose.connect(mongoUri, opts);
  }
  console.log(e);
});
connection.once('open', async () => {
  console.log(`MongoDB successfully connected to ${mongoUri}`);
  // const { NewsArtModel } = require('./src/news/models/news-article');
  await loop({ countryCode: 'KE' });
});

async function loop({ starting = new Date(), countryCode }) {
  const { NewsArtModel } = require('./src/news/models/news-article');
  let des;
  let items;
  let last;
  const limit = 1000;
  console.log('Starting now');
  const startDate = new Date(starting);
  try {
    des = await NewsArtModel.find({
      createdAt: { $lte: startDate.getTime() },
      countryCode,
      // 'ut.dnum': { $lte: startDate.getTime() },
    })
      .limit(limit + 1)
      .sort({ createdAt: -1 })
      .exec();

    console.log(`got ${des.length}`);
    if (des.length > 1) {
      console.log('Got 500 items');
      last = des[des.length - 1].createdAt;
      console.log(`Got last item ${last}`);
      await Promise.all(
        des.map(async de => {
          let xpathQuery;
          let fImg;
          const publisher = await NewsPublisherModel.findById(de.pubid);

          // If publisher is not found return now
          if (!publisher) {
            console.log(' Publisher not found ');
            return { done: false };
          }

          xpathQuery = publisher.xpath_fimg;
          if (!xpathQuery || isEmpty(xpathQuery)) {
            console.log(' Xpath is null, returning ');
            return { done: false };
          }

          // update the article image here
          fImg = await getFeaturedImage(de.meta.url, xpathQuery);
          if (!fImg || isEmpty(fImg)) {
            console.log(' Fimg is null, returning ');
            return { done: false };
          }

          de.image.banner = fImg;
          console.log('>>>>>>>>>>>>>>>>>>>>>>>>DOING>>>>>>>>>>>>>>>>>>>>>>>>>>');
          // console.log(`>>>>>>>>>>>>>>>>>>>>>>>>${fImg}>>>>>>>>>>>>>>>>>>>>>>>>>>`);
          await new Promise((res, rej) =>
            NewsArtModel.update({ _id: de._id }, de, (er, reso) => {
              if (er) {
                console.log(er);
                return rej(er);
              }
              return res(reso);
            })
          );

          // Add image to the cache
          await getVugaCachedImage(fImg);
          console.log('Edited');
          return {};
        })
      );
      console.log('Completed the batch');
      if (des.length > limit) {
        console.log('Going to a new batch');
        return await loop({ starting, countryCode });
      }
      console.log('Completed');
      return {};
    }
    console.log('Nothing completed');
    return {};
  } catch (error) {
    console.log(error);
    return { done: false };
  }
}

async function getHtml(url) {
  return new Promise((res, rej) => {
    request(url, (error, response, body) => {
      if (!error && response.statusCode == 200) {
        console.log('body>>>>>>>>>> Done');
        // console.log(`response body>>>>>>>>>${response.body}`);
        return res(body);
      }
      console.log(`error>>>>>>>>>${error}`);
      // console.log(`response statusCode>>>>>>>>>${response.statusCode}`);
      // console.log(`response body>>>>>>>>>${response.body}`);
      return rej(error);
    });
  });
}

async function getFeaturedImage(url, xpathQuery) {
  let html;
  try {
    html = await getHtml(url);
    if (!html) {
      throw new Error('Html is null');
    }
    const ogDoc = new DOMParser().parseFromString(html);
    const nodes = xpath.select(xpathQuery, ogDoc);
    console.log('Completed');
    // console.log(nodes);
    console.log(nodes[0].value);
    return nodes[0].value;
  } catch (error) {
    console.log(error);
    return {
      done: false,
    };
  }
}

async function getVugaCachedImage(url, dimension = '300x300') {
  const out = `${AWS_VUGA_CACHE + dimension}/vuga-cache/img/${encodeURIComponent(url)}`;
  // console.log(out);
  return new Promise((res, rej) => {
    request(url, (error, response, body) => {
      if (!error && response.statusCode == 200) {
        console.log('Done>>>>>>>>>> Fetching image from cache');
        // console.log(`response body>>>>>>>>>${response.body}`);
        return res(body);
      }
      console.log(`error>>>>>>>>>${error}`);
      return rej(error);
    });
  });
  // return out;
}
