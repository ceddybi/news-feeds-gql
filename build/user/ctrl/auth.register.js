'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.register = exports.loginByPhone = exports.checkUserByPhone = exports.checkUsername = exports.checkNumberAndEmail = exports.login = exports.mobileDeviceLifetime = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

// time a device can live

let addAdmin = (() => {
  var _ref = _asyncToGenerator(function* () {
    let adminUser;
    let adminPw;
    try {
      adminPw = yield (0, _passordUtils.generatePwHash)('thebiggerthebill');
      const vg = new _userProfile.UserProfileModel({
        info: {
          // userid: 'vs',
          firstname: 'Vuga',
          lastname: 'Team',
          username: 'vs',
          phone: '80',
          email: 'support@vuga.io',
          a: adminPw,
          image_path: 'http://vuga.s3-website-us-west-2.amazonaws.com/static/vuga_peace_black.jpg'
        }
      });
      vg.save(function (err, doc) {
        // d = doc;
        console.log('Created Admin Account');
      });

      return {};
    } catch (error) {
      console.log(error.message);
      return {};
    }
  });

  return function addAdmin() {
    return _ref.apply(this, arguments);
  };
})();

let adminChecker = (() => {
  var _ref2 = _asyncToGenerator(function* () {
    let admin;
    try {
      admin = yield _userProfile.UserProfileModel.findOne({ 'info.username': 'vs' });
      if (!admin) {
        addAdmin();
      } else {
        console.log('Admin check is perfect');
      }
    } catch (error) {
      console.log(error.message);
    }
  });

  return function adminChecker() {
    return _ref2.apply(this, arguments);
  };
})();

/**
 * Returns jwt token if valid username and password is provided
 * @param req
 * @param res
 * @param next
 * @returns { }
 */
let login = exports.login = (() => {
  var _ref3 = _asyncToGenerator(function* (req, res, next) {
    console.log('User Login request');
    console.log(req.body);
    const un = req.body.username;
    const pw = req.body.password;
    let user;

    try {
      user = yield _userProfile.UserProfileModel.findOne({
        $or: [{ 'info.username': un.toLowerCase() }, { 'info.email': un.toLowerCase() }, { 'info.phone': un }]
      }).exec();

      // User not found, throw error
      if (!user) {
        throw new Error('Invalid login details');
      }

      console.log(`Found some user ${user.info.username}`);
      // We are very sure field `a` is available
      const argoHash = user.info.a;
      const argonParams = {
        hash: argoHash,
        pw
      };

      const isPasswordCorrect = yield (0, _passordUtils.argonVerify)(argonParams);

      // if password correct
      if (!isPasswordCorrect) {
        throw new Error('Invalid password or username');
      }

      const userToreturn = (0, _userDataTypes.returnMyUserObject)(user);

      const token = _jsonwebtoken2.default.sign({
        _id: user._id,
        exp: new Date().getTime() + mobileDeviceLifetime / 1000
      }, _config2.default.jwtSecret);

      const loggedDevice = {
        userId: user._id,

        info: _extends({}, req.body.device),
        auth: {
          token,
          refreshToken: token
        }
      };

      // Save or update device
      yield (0, _userDevice.uniDeviceSync)({
        userDevice: _extends({}, loggedDevice, { userId: user._id })
      });

      return res.json({
        message: 'Success Login',
        deviceAuth: loggedDevice.auth,
        status: 200,
        user: userToreturn
      });

      // throw new Error('Invalid login details');
    } catch (error) {
      console.log(error);

      // return error now
      return res.json({
        status: 400,
        message: error.message,
        user: {}
      });
    }
  });

  return function login(_x, _x2, _x3) {
    return _ref3.apply(this, arguments);
  };
})();

/**
 * Checking if user's email or phone are taken
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */


let checkNumberAndEmail = exports.checkNumberAndEmail = (() => {
  var _ref4 = _asyncToGenerator(function* (req, res, next) {
    var _req$body = req.body;
    const phone = _req$body.phone,
          email = _req$body.email;

    let userEmail;
    let userPhone;
    try {
      userPhone = yield _userProfile.UserProfileModel.findOne({
        $or: [{ 'info.phone': phone }, { 'info.phone': `+${phone}` }]
      }).exec();
      if (userPhone) {
        console.log(userPhone.info);
        throw new Error('Phone number is already used');
      }

      userEmail = yield _userProfile.UserProfileModel.findOne({ 'info.email': email }).exec();
      if (userEmail) {
        console.log(userEmail.info);
        throw new Error('Email is already used');
      }

      console.log('User not found');
      return res.json({ message: 'Success', status: 200 });
      // Throw error now,
    } catch (error) {
      console.log(error);
      return res.json({ message: error.message, status: 400 });
    }
  });

  return function checkNumberAndEmail(_x4, _x5, _x6) {
    return _ref4.apply(this, arguments);
  };
})();

/**
 * Checking if username exists
 * @param {*} req
 * @param {*} res
 * @param {*} next
 *
 */


let checkUsername = exports.checkUsername = (() => {
  var _ref5 = _asyncToGenerator(function* (req, res, next) {
    const username = req.body.username;

    let userObject;
    try {
      userObject = yield _userProfile.UserProfileModel.findOne({ 'info.username': username }).exec();
      if (userObject) {
        console.log(userObject.info);
        throw new Error('Username is already taken');
      }
      // console.log('User not found');
      return res.json({ message: 'Success', status: 200 });
      // Throw error now,
    } catch (error) {
      console.log(error.message);
      return res.json({ message: error.message, status: 400 });
    }
  });

  return function checkUsername(_x7, _x8, _x9) {
    return _ref5.apply(this, arguments);
  };
})();

/**
 * Checking if user exists by phone number
 * @param {*} req phone
 * @param {*} res
 * @param {*} next
 *
 */


let checkUserByPhone = exports.checkUserByPhone = (() => {
  var _ref6 = _asyncToGenerator(function* (req, res, next) {
    let phone = req.body.phone;

    phone = phone.replace('+', '');
    let userObject;

    try {
      userObject = yield _userProfile.UserProfileModel.findOne({
        $or: [{ 'info.phone': phone }, { 'info.phone': `+${phone}` }]
      }).exec();
      if (userObject) {
        console.log(userObject.info);
        return res.json({ message: 'Success', status: 200 });
      }
      throw new Error('User does not exist, please signup');
      // console.log('User not found');
    } catch (error) {
      console.log(error.message);
      return res.json({ message: error.message, status: 400 });
    }
  });

  return function checkUserByPhone(_x10, _x11, _x12) {
    return _ref6.apply(this, arguments);
  };
})();

/**
 * Checking if user exists by phone number
 * @param {*} req phone, device
 * @param {*} res
 * @param {*} next
 *
 */


let loginByPhone = exports.loginByPhone = (() => {
  var _ref7 = _asyncToGenerator(function* (req, res, next) {
    const device = req.body.device;
    let phone = req.body.phone;

    phone = phone.replace('+', '');
    let userObject;
    try {
      userObject = yield _userProfile.UserProfileModel.findOne({
        $or: [{ 'info.phone': phone }, { 'info.phone': `+${phone}` }]
      }).exec();
      if (userObject) {
        console.log(userObject.info);

        const token = _jsonwebtoken2.default.sign({
          _id: userObject._id,
          exp: new Date().getTime() + mobileDeviceLifetime / 1000
        }, _config2.default.jwtSecret);

        const userToreturn = (0, _userDataTypes.returnMyUserObject)(userObject);

        const loggedDevice = {
          userId: userObject._id,

          info: _extends({}, device),
          auth: {
            token,
            refreshToken: token
          }
        };

        // Save or update device
        yield (0, _userDevice.uniDeviceSync)({
          userDevice: _extends({}, loggedDevice, { userId: userObject._id })
        });

        return res.json({
          message: 'Success Login',
          deviceAuth: loggedDevice.auth,
          status: 200,
          user: userToreturn
        });
      }
      throw new Error('User does not exist, please signup');
      // console.log('User not found');
    } catch (error) {
      console.log(error.message);
      return res.json({ message: error.message, status: 400 });
    }
  });

  return function loginByPhone(_x13, _x14, _x15) {
    return _ref7.apply(this, arguments);
  };
})();

/**
 * Signup/registration
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */


let register = exports.register = (() => {
  var _ref8 = _asyncToGenerator(function* (req, res, next) {
    var _req$body2 = req.body;
    const lastname = _req$body2.lastname,
          firstname = _req$body2.firstname,
          username = _req$body2.username,
          sex = _req$body2.sex,
          email = _req$body2.email,
          password = _req$body2.password;
    let phone = req.body.phone;

    phone = phone.replace('+', '');
    // TODO check email, phone, username
    let userEmail;
    let userPhone;
    let userUsername;
    let newUserDevice;
    let newUser;

    try {
      // Checking username, email, phone.
      userPhone = yield _userProfile.UserProfileModel.findOne({
        $or: [{ 'info.phone': phone }, { 'info.phone': `+${phone}` }]
      }).exec();
      if (userPhone) {
        console.log(userPhone.info);
        throw new Error('Phone number is already used');
      }

      userEmail = yield _userProfile.UserProfileModel.findOne({ 'info.email': email }).exec();
      if (userEmail) {
        console.log(userEmail.info);
        throw new Error('Email is already used');
      }

      userUsername = yield _userProfile.UserProfileModel.findOne({ 'info.username': username }).exec();
      if (userUsername) {
        console.log(userUsername.info);
        throw new Error('Username is already taken');
      }

      // Create account now,

      // Argon password
      const passwordHashed = yield (0, _passordUtils.generatePwHash)(password);
      if (!passwordHashed) {
        throw new Error('Unexpected error occured, please try again');
      }

      newUser = {
        info: {
          a: passwordHashed, // Award winning password argo.
          firstname,
          lastname,
          phone,
          email,
          username: username.toLowerCase(),
          sex
          // password: pass
        }
      };

      newUser = new _userProfile.UserProfileModel(newUser);

      // create a new device
      newUserDevice = yield (0, _userDevice.uniDeviceSync)({
        userDevice: _extends({}, req.body.device, { userId: newUser._id })
      });

      if (!newUserDevice) {
        throw new Error('Failed to create new device, please try again later');
      }

      // add token to device
      newUserDevice = yield (0, _userDevice.generateNewTokenForDevice)({
        deviceId: newUserDevice._id,
        userId: newUser._id
      });

      if (!newUserDevice) {
        throw new Error('Failed to create new device, please try again later');
      }

      // save new user
      yield newUser.save();

      // Add first convo with admin, but no need to listen for callback
      yield (0, _userChat.addFirstChatWithAdmin)(newUser._id);

      // Sucesss signup now
      return res.json({
        message: 'Success',
        status: 200,
        user: (0, _userDataTypes.returnMyUserObject)(newUser),
        deviceAuth: newUserDevice.auth
      });
    } catch (error) {
      console.log(error);
      return res.json({ message: error.message, status: 400 });
    }
  });

  return function register(_x16, _x17, _x18) {
    return _ref8.apply(this, arguments);
  };
})();

var _jsonwebtoken = require('jsonwebtoken');

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

var _httpStatus = require('http-status');

var _httpStatus2 = _interopRequireDefault(_httpStatus);

var _jwtDecode = require('jwt-decode');

var _jwtDecode2 = _interopRequireDefault(_jwtDecode);

var _lodash = require('lodash');

var _APIError = require('../helpers/APIError');

var _APIError2 = _interopRequireDefault(_APIError);

var _config = require('../../config');

var _config2 = _interopRequireDefault(_config);

var _passordUtils = require('../../utils/passord-utils');

var _userDataTypes = require('../../utils/user-data-types');

var _userProfile = require('../models/user-profile');

var _userDevice = require('../models/user-device');

var _userChat = require('../../chat/helpers/user.chat.helpers');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

// import { uniDeviceSync, generateNewTokenForDevice } from './auth.device';

const mobileDeviceLifetime = exports.mobileDeviceLifetime = 259200;
adminChecker();