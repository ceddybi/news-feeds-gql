/**
 * Created by vuga on 9/22/17.
 */
import mongoose from 'mongoose';
import composeWithMongoose from 'graphql-compose-mongoose';
import composeWithRelay from 'graphql-compose-relay';
import { Resolver } from 'graphql-compose';
import { GraphQLList, GraphQLString, GraphQLInt, GraphQLEnumType } from 'graphql';
import _ from 'lodash';
import { dataAvailability } from '../../utils/data-types';

const UserViewSchema = new mongoose.Schema(
  {
    ...dataAvailability,

    // User
    userId: { type: String, default: '' },

    // Can be anything, user profile, post, comment, message e.t.c
    intentId: { type: String, default: '' },

    // sub of Intent Object, e.g chatSeen, chatDelievered
    intentTarget: { type: String, default: '' },
  },
  {
    collection: 'userviews',
    timestamps: true,
  }
);

export const UserViewModel = mongoose.model('UserView', UserViewSchema);
export const UserViewTC = composeWithRelay(composeWithMongoose(UserViewModel));
