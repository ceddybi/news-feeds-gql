'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _joi = require('joi');

var _joi2 = _interopRequireDefault(_joi);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  getContent: {
    params: {
      path: _joi2.default.string().required()
    }
  },
  sendPush: {
    body: {
      typo: _joi2.default.string().required(),
      content_id: _joi2.default.string().required(),
      msg: _joi2.default.string().required(),
      title: _joi2.default.string().required(),
      sound: _joi2.default.string()
    }
  }
};