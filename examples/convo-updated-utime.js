/* @flow */

import mongoose from 'mongoose';
import { mongoUri } from './src/config';
// import { NewsArtModel } from './src/news/models/news-article';

mongoose.Promise = Promise;

const opts = {
  autoReconnect: true,
  reconnectTries: Number.MAX_VALUE,
  reconnectInterval: 1000,
  // useMongoClient: true,
};

mongoose.connect(mongoUri, opts);

export const { connection } = mongoose;
connection.on('error', e => {
  if (e.message.code === 'ETIMEDOUT') {
    console.log(e);
    mongoose.connect(mongoUri, opts);
  }
  console.log(e);
});
connection.once('open', async () => {
  console.log(`MongoDB successfully connected to ${mongoUri}`);
  // const { NewsArtModel } = require('./src/news/models/news-article');
  await loop();
  console.log('Finished');
  process.exit(1);
});

async function loop(starting = new Date()) {
  const { RadioModel } = require('./src/live/models/radio');
  const { tvModel } = require('./src/live/models/tv');
  const { ConvoModel } = require('./src/chat/models/convos');
  let des;
  let items;
  const limit = 600;
  try {
    console.log('Fetching items');
    items = await ConvoModel.find({})
      .limit(limit)
      .exec();

    console.log('Got items');
    await Promise.all(
      items.map(async item => {
        return new Promise((res, rej) => {
          item.utime = item.updatedAt;
          ConvoModel.update({ _id: item._id }, item, (er, reso) => {
            if (reso) {
              console.log(`Saved ${item.updatedAt} = ${item.utime}`);
              return res();
            }
            console.log('Not updated');
            return res();
          });
        });
      })
    );
    return { done: true };
  } catch (error) {
    console.log(error);
  }
}
