/**
 * Created by vuga on 9/22/17.
 */
import mongoose from 'mongoose';
import composeWithMongoose from 'graphql-compose-mongoose';
import composeWithRelay from 'graphql-compose-relay';

const UserOldSchema = new mongoose.Schema(
  {
    fcmToken: { type: String, default: '' },
  },
  {
    collection: 'users',
    timestamps: true,
  }
);

UserOldSchema.index({ created_at: 1 });

export const UserOldModel = mongoose.model('User', UserOldSchema);
export const UserOldTC = composeWithRelay(composeWithMongoose(UserOldModel));
