import { argon2i } from 'argon2-ffi';
import crypto from 'crypto';
import Promise from 'bluebird';

const randomBytes = Promise.promisify(crypto.randomBytes);

export async function argonVerify(ob) {
  console.log(`Argon Verify called hash = ${ob.pw}`);
  console.log(ob.hash);
  const encodedHash = ob.hash;
  // const password = Buffer.from(ob.pw);
  const password = ob.pw;
  let isValid;
  try {
    isValid = await argon2i.verify(encodedHash, password);
    console.log('Is valid', isValid);
    return isValid;
  } catch (error) {
    console.log(error);
    return false;
  }
}

export async function generatePwHash(pw) {
  const password = Buffer.from(pw);
  const options = { timeCost: 4, memoryCost: 1 << 14, parallelism: 2, hashLength: 64 };
  let salt;
  let hash;
  try {
    salt = await randomBytes(32);
    hash = await argon2i.hash(password, salt, options);
    return hash;
  } catch (error) {
    console.log(error);
    return null;
  }
}
