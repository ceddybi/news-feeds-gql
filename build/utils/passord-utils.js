'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.generatePwHash = exports.argonVerify = undefined;

let argonVerify = exports.argonVerify = (() => {
  var _ref = _asyncToGenerator(function* (ob) {
    console.log(`Argon Verify called hash = ${ob.pw}`);
    console.log(ob.hash);
    const encodedHash = ob.hash;
    // const password = Buffer.from(ob.pw);
    const password = ob.pw;
    let isValid;
    try {
      isValid = yield _argon2Ffi.argon2i.verify(encodedHash, password);
      console.log('Is valid', isValid);
      return isValid;
    } catch (error) {
      console.log(error);
      return false;
    }
  });

  return function argonVerify(_x) {
    return _ref.apply(this, arguments);
  };
})();

let generatePwHash = exports.generatePwHash = (() => {
  var _ref2 = _asyncToGenerator(function* (pw) {
    const password = Buffer.from(pw);
    const options = { timeCost: 4, memoryCost: 1 << 14, parallelism: 2, hashLength: 64 };
    let salt;
    let hash;
    try {
      salt = yield randomBytes(32);
      hash = yield _argon2Ffi.argon2i.hash(password, salt, options);
      return hash;
    } catch (error) {
      console.log(error);
      return null;
    }
  });

  return function generatePwHash(_x2) {
    return _ref2.apply(this, arguments);
  };
})();

var _argon2Ffi = require('argon2-ffi');

var _crypto = require('crypto');

var _crypto2 = _interopRequireDefault(_crypto);

var _bluebird = require('bluebird');

var _bluebird2 = _interopRequireDefault(_bluebird);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new _bluebird2.default(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return _bluebird2.default.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

const randomBytes = _bluebird2.default.promisify(_crypto2.default.randomBytes);