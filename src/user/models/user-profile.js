/**
 * Created by vuga on 9/22/17.
 */
import mongoose from 'mongoose';
import composeWithMongoose from 'graphql-compose-mongoose';
import composeWithRelay from 'graphql-compose-relay';
import {
  Resolver,
  TypeComposer,
  GraphQLJSON,
  InputTypeComposer,
  GraphQLNonNull,
} from 'graphql-compose';
import {
  GraphQLObjectType,
  GraphQLList,
  GraphQLString,
  GraphQLInt,
  GraphQLBoolean,
  GraphQLOutputType,
} from 'graphql';
// import _ from 'lodash';
// import { ConvoUserTC } from '../../chat/models/convos'

import userData, { returnUser } from '../../utils/user-data-types';
import _DA from '../../utils/data-types';
import { nKformater } from '../../utils/text-utils';

const UserProfileSchema = new mongoose.Schema(
  {
    // Version updates of profile
    veri: { type: Number, default: 1 },

    // Usermeta, basic info
    info: userData.info,
    inbox: userData.inbox,

    // Chat, last active, follow, views, post
    lastactive: _DA.ut,

    chatcount: userData.count,
    followcount: userData.count,
    viewcount: userData.count,
    postcount: userData.count,

    utime: { type: Date, default: Date.now },
    views: { type: Number, default: 0 },

    // New API
    chatbox: [
      /** {
          convoId, 
          count,
          utime
           } */
    ],

    // New friendship API, friends with more love appear in the news feeds
    friends: [
      /** {
          userId, 
          since: Date(),
          love: Number
          ,
      } */
    ],
  },
  {
    collection: 'userprofiles',
    timestamps: true,
  }
);

export const UserProfileModel = mongoose.model('UserProfile', UserProfileSchema);
export const UserProfileTC = composeWithRelay(composeWithMongoose(UserProfileModel));

UserProfileTC.setResolver(
  'connection',
  UserProfileTC.getResolver('connection').addFilterArg({
    name: 'byName',
    type: `input byName {
      name: String!
    }`,
    query: (rawQuery, value, resolveParams) => { // eslint-disable-line
      console.log(value);
      const { name } = value;
      rawQuery.$or = [
        { 'info.firstname': { $regex: `.*${name}.*` } },
        { 'info.lastname': { $regex: `.*${name}$.*` } },
      ];
    },
  })
  // /* FOR DEBUG */
  //   .debug()
  // /* OR MORE PRECISELY */
  //   .debugParams()
  //   .debugPayload()
  //   .debugExecTime()
);
// UserProfileTC.addResolver(resolverByIntentId);

// Login, checkusername, checkemail, signup, getProfilePic

const resolverByName = new Resolver({
  name: 'byName',
  type: [UserProfileTC],
  kind: 'query',
  args: {
    name: {
      type: [GraphQLString],
    },
  },
  resolve: async ({ source, args, context, info }) => {
    const { name } = args;
    let users = [];
    try {
      users = await UserProfileModel.find({
        $or: [
          { 'info.firstname': { $regex: `.*${name}.*` } },
          { 'info.lastname': { $regex: `.*${name}$.*` } },
        ],
      })
        .limit(20)
        .exec();
      return users;
    } catch (error) {
      console.log(error);

      // Return empty array
      return users;
    }
  },
});

UserProfileTC.addResolver(resolverByName);

export const PublicUser = TypeComposer.create({
  name: 'PublicUser',
  fields: {
    userId: { type: GraphQLString },
    firstname: { type: GraphQLString },
    lastname: { type: GraphQLString },
    username: { type: GraphQLString },
    image_path: { type: GraphQLString },
    lastactive: { type: GraphQLString },
    sex: { type: GraphQLString },
  },
});

// Get many users by userIds
UserProfileTC.addResolver(
  new Resolver({
    name: 'byId',
    type: [PublicUser],
    kind: 'query',
    args: {
      userIds: {
        type: [GraphQLString],
      },
    },
    resolve: async ({ source, args, context, info }) => {
      const { userIds } = args;
      let users = {};
      try {
        users = await userIds.map(async userId => {
          const user = await UserProfileModel.findById(userId).exec();
          return returnUser(user);
        });
        console.log('Got the user');
        return users;
      } catch (error) {
        console.log(error);
        // Return empty array
        return users;
      }
    },
  })
);

// Get one user by userId
UserProfileTC.addResolver(
  new Resolver({
    name: 'byUserId',
    type: PublicUser,
    kind: 'query',
    args: {
      userId: {
        type: GraphQLString,
      },
    },
    resolve: async ({ source, args, context, info }) => {
      const { userId } = args;
      let user = {};
      try {
        user = await UserProfileModel.findById(userId).exec();
        return returnUser(user);
      } catch (error) {
        console.log(error);
        // Return empty array
        return user;
      }
    },
  })
);

// user stats + public user
export const ConvoUserPublic = TypeComposer.create({
  name: 'ConvoUserPublic',
  fields: {
    userId: { type: GraphQLString },
    counts: { type: GraphQLInt },

    // Public user
    firstname: { type: GraphQLString },
    lastname: { type: GraphQLString },
    username: { type: GraphQLString },
    image_path: { type: GraphQLString },
    photo: { type: GraphQLString },
    lastactive: { type: GraphQLString },
    sex: { type: GraphQLString },
  },
});

// User public user, viewable by anyone
export const UserPublicTC = TypeComposer.create({
  name: 'UserPublicTC',
  fields: {
    userId: { type: GraphQLString },
    // Public user
    firstname: { type: GraphQLString },
    lastname: { type: GraphQLString },
    username: { type: GraphQLString },
    nickname: { type: GraphQLString },
    photo: { type: GraphQLString },
    lastactive: { type: GraphQLString },
    views: { type: GraphQLString },
    sex: { type: GraphQLString },
    bio: { type: GraphQLString },
    address: { type: GraphQLString },
    since: { type: GraphQLString },
  },
});

// Utility to expose UserPublicTC
export function parsePublicUser(user) {
  const { firstname, lastname, nickname, sex, bio, address, username, image_path } = user.info;

  return {
    userId: user._id,
    firstname,
    lastname,
    username,
    nickname,
    photo: image_path,
    lastactive: user.utime || user.updatedAt,
    views: `${nKformater(user.views || 0, 1)}`,
    sex,
    bio,
    address,
    since: user.createdAt,
  };
}

const ConvoUserITC = InputTypeComposer.create(`
 input 
 ConvoUser 
  { 
    userId: String!,
    counts: Int!
  }`);

// Get one user by userId
UserProfileTC.addResolver(
  new Resolver({
    name: 'byConvoUser',
    type: [ConvoUserPublic],
    kind: 'query',
    args: {
      users: {
        type: [ConvoUserITC],
      },
    },
    resolve: async ({ source, args, context, info }) => {
      const { users } = args;
      // console.log(users);
      let chatUser = {};
      try {
        chatUser = await users.map(async user => {
          const userx = await UserProfileModel.findById(user.userId).exec();
          // console.log(user.counts)
          return { counts: user.counts, ...parsePublicUser(userx) };
        });
        console.log('Got the user');
        return chatUser;
      } catch (error) {
        console.log(error);
        // Return empty array
        return chatUser;
      }
    },
  })
);

// View user by userId mutation
UserProfileTC.addResolver(
  new Resolver({
    name: 'viewUserMutation',
    type: UserPublicTC,
    kind: 'mutation',
    args: {
      userId: {
        type: GraphQLString,
      },
    },
    resolve: async ({ source, args, context, info }) => {
      const { userId } = args;
      // console.log(users);
      let user = {};
      try {
        user = await UserProfileModel.findById(userId).exec();

        // Add views to users profile
        user.views += 1;
        // TODO ADD VIEWER COUNT
        user.save((er, rawQuery) => {});

        if (!user) {
          throw new Error('User not found for viewing');
        }
        return parsePublicUser(user);
      } catch (error) {
        console.log(error);
        // Return empty array
        return user;
      }
    },
  })
);

// View user by userId query
UserProfileTC.addResolver(
  new Resolver({
    name: 'viewUser',
    type: UserPublicTC,
    kind: 'query',
    args: {
      userId: {
        type: GraphQLString,
      },
    },
    resolve: async ({ source, args, context, info }) => {
      const { userId } = args;
      // console.log(users);
      let user = {};
      try {
        user = await UserProfileModel.findById(userId).exec();

        if (!user) {
          throw new Error('User not found for viewing');
        }

        // Add views to users profile
        user.views += 1;

        await user.save();

        return parsePublicUser(user);
      } catch (error) {
        console.log(error);
        // Return empty array
        return null;
      }
    },
  })
);

// Search user by name
UserProfileTC.addResolver(
  new Resolver({
    name: 'searchUsers',
    type: [UserPublicTC],
    kind: 'query',
    args: {
      name: {
        type: GraphQLString,
      },
    },
    resolve: async ({ source, args, context, info }) => {
      // const { name } = args;
      const name = args.name || '';
      // console.log(users);
      let users = [];
      try {
        // user = await UserProfileModel.findById(userId).exec();

        users = await UserProfileModel.aggregate([
          {
            $project: {
              fullname: {
                $concat: [
                  { $toLower: '$info.firstname' },
                  ' ',
                  { $toLower: '$info.lastname' },
                  ' ',
                  { $toLower: '$info.username' },
                ],
              },
              info: 1,
              updatedAt: 1,
              utime: 1,
            },
          },
          //          { $match: { name: { $regex: `/${name}/i` } } }, /bob j/i
          { $match: { fullname: { $regex: `.*${name.toLowerCase()}.*` } } },
          {
            $sort: { updatedAt: -1 },
          },
        ])
          .limit(20)
          .exec();

        console.log('Users are', users.length);

        if (!users) {
          throw new Error('Users not');
        }

        users = users.map(us => {
          return parsePublicUser(us);
        });

        return users;
      } catch (error) {
        console.log(error);
        // Return empty array
        return users;
      }
    },
  })
);
