/**
 * GRAPH QL THREADS, USING MONGO
 * LIKE CONNECTION, Heavily relying on createdAt, direction.
 */
import { Resolver, TypeComposer } from 'graphql-compose';
import { typeThreadInput } from '../data-types';
import { GraphQLBoolean, GraphQLString } from 'graphql';

import { isEmpty } from 'lodash';

export const typePageInfo = TypeComposer.create(`
    type pageInfo {
      hasNext: Boolean!
      direction: String
    }
`);

const pageInfoType = TypeComposer.create({
  name: 'pageInfo',
  fields: {
    direction: GraphQLString,
    hasNext: GraphQLBoolean,
  },
});

export const ThreadResolver = data => {
  const { Model, ModelTC, itemsFormater, extraArgs, extraQuery, extraSort } = data;

  if (extraArgs) {
    pageInfoType.addFields({
      ...extraArgs,
    });
  }

  const resolver = new Resolver({
    name: 'thread',
    args: {
      ...typeThreadInput,
      ...extraArgs,
    },
    type: TypeComposer.create({
      name: `${ModelTC.getTypeName()}Thread`,
      fields: {
        pageInfo: pageInfoType,
        items: [ModelTC],
      },
    }),
    kind: 'query',
    resolve: async ({ source, args, context, info }) => {
      // console.log(args);
      // console.log(data);
      const start = new Date(args.start) || new Date();
      const direction = args.direction || 'old';
      // const { intentId } = args;
      //let limit = args.limit && args.limit > 24 ? args.limit : 24;
      let limit = args.limit || 12;

      let query = {};
      let sort = { createdAt: -1 };

      let pageInfo = {
        hasNext: false,
        direction,
      };

      let items = [];

      // We don't want to stress this server
      if (limit > 1000) {
        limit = 1000;
      }

      // Direction here, TIME ELAPSED
      if (direction === 'old') {
        query.createdAt = { $lte: start };
      } else {
        // Greater than the time provided
        query.createdAt = { $gte: start };
      }

      // Apply extra query if present
      if (extraQuery) {
        query = {
          ...query,
          ...extraQuery({ source, args: { ...args, direction, start }, context, info }),
        };
      }

      // Apply sort to Sorter if present
      if (extraSort) {
        sort = {
          // ...sort,
          ...extraSort({ source, args, context, info }),
        };
      }

      try {
        console.log('From threads query ', query);
        console.log('From threads sort', sort);
        // query the model now
        items = await Model.find(query)
          .sort(sort)
          .limit(limit + 1)
          .exec();

        console.log('items----------------------------------', items.length);
        // update pageInfo
        if (items && !isEmpty(items)) {
          console.log('items----------------------------------', items.length);
          // pageInfo.hasNext = items.length == limit + 1;

          // Remove last items used for detecting next
          if (items.length > limit) {
            pageInfo.hasNext = true;
            items.pop();
          }

          if (itemsFormater) {
            // Do we have a transformer?
            items = await itemsFormater(items);
          }
        }
      } catch (error) {
        console.log(error.message);
      }

      pageInfo = {
        ...pageInfo,
        ...args,
      };

      console.log('Page info-------------------', pageInfo);
      return { items, pageInfo };
    },
  });

  /**
   if (extraArgs) {
    resolver.addArgs({
      ...extraArgs,
    });
  }
   */

  return resolver;
};
