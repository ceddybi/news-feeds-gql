/* @flow */

// import type { Resolver } from '../gqc';
import jwt from 'jsonwebtoken';
import config from './config';

export function allowAuthenticatedOnly(resolvers) {
  const secureResolvers = {};
  Object.keys(resolvers).forEach(k => {
    secureResolvers[k] = resolvers[k].wrapResolve(next => rp => {
      let token = rp.context.headers.authorization;
      if (!token) {
        console.log('Token missing');
        throw new Error('401 - unauthorized');
      }

      token = token.replace('Bearer ', '');

      console.log(token);

      jwt.verify(token, config.jwtSecret, (err, decoded) => {
        if (err) {
          console.log('Invalid token');
          throw new Error('401 - unauthorized');
        } else {
          // console.log(decoded.foo); // bar
          console.log(decoded); // bar
          rp.context.userId = decoded._id;
          return next(rp);
        }
      });

      return next(rp);
    });
  });
  return secureResolvers;
}
