/* @flow */

// SINGLE SCHEMA ON SERVER
// import { GQC } from 'graphql-compose';

// MULTI SCHEMA MODE IN ONE SERVER
// create new GQC from ComposeStorage
import { ComposeStorage } from 'graphql-compose';
import composeWithRelay from 'graphql-compose-relay';

import { NewsArtTC, NewsArtModel } from './news/models/news-article';
import { NewsPubTC } from './news/models/news-publisher';

import { LiveTvTC } from './live/models/tv';
import { LiveRadioTC } from './live/models/radio';

import { CommentTC } from './reaction/models/cmts';
import { NiceTC } from './reaction/models/nice';
import { GraphQLString } from 'graphql';
import { allowAuthenticatedOnly } from './auth-qql';

import { UserProfileTC } from './user/models/user-profile';
import { ConvoTC } from './chat/models/convos';
import { ChatMsgTC } from './chat/models/chat-msgs';
import { CronJobTC } from './push/models/cron-jobs';
import { JobTC } from './push/models/jobs';
import { NotificationTC } from './push/models/notifications';
import { PushTC } from './push/models/push-model';

const GQC = new ComposeStorage();

composeWithRelay(GQC.rootQuery());

const ViewerTC = GQC.get('Viewer');
GQC.rootQuery().addFields({
  viewer: {
    type: ViewerTC.getType(),
    description: 'Data under client context',
    resolve: (obj, args, context) => {
      context.ceddy = {
        title: 'Hello',
      };
      return { obj, args, context };
    },
    args: {
      token: {
        type: GraphQLString,
      },
    },
  },
});

const ViewerFields = {
  // Live here, Read only no sensitivity
  LiveRadios: LiveRadioTC.getResolver('categoryCountry'),
  RadioById: LiveRadioTC.get('$findById'),
  LiveTvs: LiveTvTC.getResolver('categoryCountry'),
  TvById: LiveTvTC.get('$findById'),

  // News, Read only no sensitivity
  articlesLatest: NewsArtTC.getResolver('byArtDate'),

  // View article
  ViewArticle: NewsArtTC.getResolver('viewArticle'),

  ArticleViewQuery: NewsArtTC.getResolver('ArticleViewQuery'),
  PublisherConnection: NewsPubTC.getResolver('connection'),

  /** ...allowAuthenticatedOnly({
    PublisherTop5: NewsPubTC.getResolver('top5'),
  }), * */

  ArticleConnetion: NewsArtTC.getResolver('connection'),
  ArticleById: NewsArtTC.getResolver('byId'),
  ArticleThreads: NewsArtTC.getResolver('thread'),

  // Reactions sections here
  getComments: CommentTC.getResolver('byIntentId'),
  CommentsConnection: CommentTC.getResolver('connection'),
  CommentThreads: CommentTC.getResolver('thread'),
  getNix: NiceTC.getResolver('byIntentId'),

  // Users search
  viewUser: UserProfileTC.getResolver('viewUser'),
  searchUsers: UserProfileTC.getResolver('searchUsers'),
  UserProfileConnection: UserProfileTC.getResolver('connection'),
  UserProfileByName: UserProfileTC.getResolver('byName'),
  UserProfileById: UserProfileTC.getResolver('byId'),
  UserProfileByConvoUser: UserProfileTC.getResolver('byConvoUser'),

  // Chat
  ConvoConnection: ConvoTC.getResolver('connection'),
  ConvoThreads: ConvoTC.getResolver('thread'),
  // Check if conversation exists
  ConvoCheck: ConvoTC.getResolver('checkConvo'),

  ChatConnection: ChatMsgTC.getResolver('connection'),
  ChatThreads: ChatMsgTC.getResolver('thread'),

  // CronJobs, jobs, notifications
  findManyCronJobs: CronJobTC.get('$findMany'),
  findManyJobs: JobTC.get('$findMany'),
  findManyNotifications: NotificationTC.get('$findMany'),

  // CronJobs, jobs, notifications
  findOneCronJobs: CronJobTC.get('$findOne'),
  findOneJobs: JobTC.get('$findOne'),
  findOneNotifications: NotificationTC.get('$findOne'),
  findManyNotiTypos: NotificationTC.getResolver('notiTypo'),
  findManyPushs: PushTC.get('$findMany'),
  findOnePushs: PushTC.get('$findOne'),
};

ViewerTC.addFields(ViewerFields);

const CreatorTC = GQC.get('Viewer');
const creatorFields = {
  // View a user
  viewUser: UserProfileTC.getResolver('viewUser'),

  // Create a comment
  createComment: CommentTC.getResolver('createComment'),

  ArticleViewMutate: NewsArtTC.getResolver('ArticleViewMutate'),

  // Create a nice reaction
  createNice: NiceTC.get('$createOne'),

  // Creact a conversation
  createConvo: ConvoTC.getResolver('newConvo'),

  // Create a message || send a message
  createMessage: ChatMsgTC.getResolver('newChatMsg'),

  // Reads conversations,
  readConvo: ConvoTC.getResolver('readConvo'),

  // Create and update cron job with listners
  updateCronJob: CronJobTC.getResolver('updateCron'),

  // System relay methods
  createJobs: JobTC.get('$createOne'),
  updateJobs: JobTC.get('$updateOne'),
  createNotifications: NotificationTC.get('$createOne'),
  updateNotifications: NotificationTC.get('$updateOne'),

  // Create and update push with listeners
  updatePush: PushTC.getResolver('updatePush'),

};
CreatorTC.addFields(creatorFields);

GQC.rootMutation().addFields({
  creator: {
    type: CreatorTC.getType(),
    description: 'Creator and only creator',
    resolve: (obj, args, context) => {
      context.ceddy = {
        title: 'Hello',
      };
      return { obj, args, context };
    },
    args: {
      token: {
        type: GraphQLString,
        isRequired: true,
      },
    },
  },
});

export default GQC.buildSchema();
