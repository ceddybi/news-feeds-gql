const apn = require('apn');
const utf8 = require('utf8');

const http = require('https');

const key = 'AIzaSyDIJjVNYHoMgk1TFM8OmQtmyHMruFsMmys';

const deviceToken =
  'ecseFHKwuvc:APA91bGNM4UaLIfnFUbn7JyBRM1moR_hCCy5yvX6b0EDJQcBWF8GDNzEfLsxmdh1ieqYbxZSbip7mtHIWJAffFt1aYwxtY1rGLI1hDKHxff6QQLe02hYk8GVA2AD_iqlBPxqilNnGFjC';

function pushIt(du) {
  const reg = du.id;
  const push = du.pu;
  let respo;

  const payload = JSON.stringify({
    registration_ids: [deviceToken], // required
    collapse_key: push.collapse_key || 'your_collapse_key',
    notification: {
      title: push.title,
      body: push.message,
      sound: push.sound,
      click_action: push.click_action,
      icon: 'ic_l',
      color: '#4cb5ab',
    },
    data: push.data,
  });

  const headers = {
    Host: 'fcm.googleapis.com',
    Authorization: `key=${key}`,
    'Content-Type': 'application/json',
    'Content-Length': Buffer.byteLength(payload),
  };

  const post_options = {
    host: 'fcm.googleapis.com',
    port: 443,
    path: '/fcm/send',
    method: 'POST',
    headers,
  };

  return new Promise((resolve, reject) => {
    const post_req = http.request(post_options, response => {
      response.setEncoding('utf8');

      response.on('data', namedata => {
        respo = String(namedata);
      });
      response.on('end', () => {
        console.log(respo);
        resolve(respo);
      });

      response.on('error', err => {
        respo.error = err;
        reject(err);
        console.log(`On error ${err}`);
      });
    });

    post_req.on('error', e => {
      respo.error = e;
      reject(e);
      console.log(`On error ${e}`);
    });

    post_req.write(payload);
    post_req.end();
  });
}

pushIt({
  pu: {
    title: 'Hello',
    message: 'Nice one',
    click_action: '',
    sound: '',
    data: null,
  },
}).then(f => {
  console.log(f);
});
