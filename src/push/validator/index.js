import Joi from 'joi';

export default {
  getContent: {
    params: {
      path: Joi.string().required(),
    },
  },
  sendPush: {
    body: {
      typo: Joi.string().required(),
      content_id: Joi.string().required(),
      msg: Joi.string().required(),
      title: Joi.string().required(),
      sound: Joi.string(),
    },
  },
};
