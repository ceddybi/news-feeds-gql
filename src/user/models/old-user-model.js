import mongoose from 'mongoose';

const UserProfileSchema = new mongoose.Schema(
  {
    fcmToken: {type: String, default: ""},
    unix_time: {type: String, default: ""+ new Date().getTime()},
  },
  {
    collection: 'users',
    // timestamps: true,
  }
);

export const UserProfileModel = mongoose.model('User', UserProfileSchema);