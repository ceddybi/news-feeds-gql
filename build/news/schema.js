'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _graphqlCompose = require('graphql-compose');

var _graphqlComposeRelay = require('graphql-compose-relay');

var _graphqlComposeRelay2 = _interopRequireDefault(_graphqlComposeRelay);

var _newsArticle = require('./models/news-article');

var _newsPublisher = require('./models/news-publisher');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// SINGLE SCHEMA ON SERVER
// import { GQC } from 'graphql-compose';

// MULTI SCHEMA MODE IN ONE SERVER
// create new GQC from ComposeStorage
const GQC = new _graphqlCompose.ComposeStorage();

(0, _graphqlComposeRelay2.default)(GQC.rootQuery());

const ViewerTC = GQC.get('Viewer');
GQC.rootQuery().addFields({
  viewer: {
    type: ViewerTC.getType(),
    description: 'Data under client context',
    resolve: () => ({})
  }
});

const fields = {
  articlesLatest: _newsArticle.NewsArtTC.getResolver('byArtDate'),
  //publisherLatestArts: NewsPubTC.getResolver('byLatestArts'),
  PublisherConnection: _newsPublisher.NewsPubTC.getResolver('connection'),
  PublisherTop5: _newsPublisher.NewsPubTC.getResolver('top5'),
  ArticleConnetion: _newsArticle.NewsArtTC.getResolver('connection')
};

ViewerTC.addFields(fields);

GQC.rootMutation().addFields({
  createProduct: _newsArticle.NewsArtTC.get('$createOne'),
  removeProductById: _newsArticle.NewsArtTC.get('$removeById')
});

exports.default = GQC.buildSchema();