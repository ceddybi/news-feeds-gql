'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.allowAuthenticatedOnly = allowAuthenticatedOnly;

var _jsonwebtoken = require('jsonwebtoken');

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import type { Resolver } from '../gqc';
function allowAuthenticatedOnly(resolvers) {
  const secureResolvers = {};
  Object.keys(resolvers).forEach(k => {
    secureResolvers[k] = resolvers[k].wrapResolve(next => rp => {
      let token = rp.context.headers.authorization;
      if (!token) {
        console.log('Token missing');
        throw new Error('401 - unauthorized');
      }

      token = token.replace('Bearer ', '');

      console.log(token);

      _jsonwebtoken2.default.verify(token, _config2.default.jwtSecret, (err, decoded) => {
        if (err) {
          console.log('Invalid token');
          throw new Error('401 - unauthorized');
        } else {
          // console.log(decoded.foo); // bar
          console.log(decoded); // bar
          rp.context.userId = decoded._id;
          return next(rp);
        }
      });

      return next(rp);
    });
  });
  return secureResolvers;
}