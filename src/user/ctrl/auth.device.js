import jwt from 'jsonwebtoken';
import httpStatus from 'http-status';
import jwtDecode from 'jwt-decode';
import { isEmpty } from 'lodash';
import APIError from '../helpers/APIError';
import config from '../../config';
import { argonVerify } from '../../utils/passord-utils';
import { returnUser, returnMyUserObject } from '../../utils/user-data-types';
import { UserProfileModel } from '../models/user-profile';
import { UserDeviceModel, refreshTokenFromOld, uniDeviceSync } from '../models/user-device';

const androidRelease = {
  '1.4.1': {
    // name: '1.4.1',
    changelog: `The new version is faster and more reliable than ever, Go to Play store to get the update and find out what's new`,
    m: true,
    expired: true,
  },
};

const iosRelease = {};

function updateRelease({ typo, appversion = 'x' }) {
  if (typo === 'android') {
    return androidRelease[appversion];
  }
  return iosRelease[appversion];
}

/**
 *
 * @param {*} req
 * unique, refreshToken
 * @param {*} res
 * @param {*} next
 * @returns {*} {token} || 401 unauthorized
 *
 * Return { token }, error 401 unauthorized
 */
export async function tokenRefresh(req, res, next) {
  const { unique, refreshToken } = req.body;
  let userDevice;

  try {
    userDevice = await UserDeviceModel.findOne({
      'info.unique': unique,
      'auth.refreshToken': refreshToken,
    }).exec();

    if (userDevice) {
      // Not suspended;
      if (!userDevice.auth.suspended) {
        // refresh the token
        const refreshed = refreshTokenFromOld(userDevice.auth.token);

        // update device
        userDevice.auth = {
          ...userDevice.auth,
          ...refreshed,
        };

        // Save the device
        userDevice.save((e, saved) => {
          if (saved) {
            console.log('User device updated');
          } else {
            console.log('Failed to create device');
          }
        });
        return res.json(refreshed);
      }
      throw new Error('User device suspended');
    }

    throw new Error('User device not found');
  } catch (error) {
    console.log(error);

    // User needs to sign in again
    return res.status(401).end();
  }
}

export async function syncDevice(req, res, next) {
  let userDevice;
  const { userId, device } = req.body;
  let update;

  try {
    // Check if we have a session
    if (userId) {
      userDevice = await uniDeviceSync({
        userDevice: { ...device, userId },
      });
    } else {
      // Going anonymouse
      userDevice = await uniDeviceSync({ userDevice: { ...device } });
    }

    if (userDevice) {
      // Android versions check here
      update = updateRelease({ appversion: device.appversion, typo: userDevice.typo });
      // Else return the normal object
      return res.json({
        update: update || { expired: false },
        device: userDevice,
        status: 200,
        message: 'Success',
      });
    }

    throw new Error('User device not found');
  } catch (error) {
    console.log(error);
    return res.json({ message: error.message, status: 400 });
  }
}

/**
 * Method to refresh the user object
 * @param {*} req
 * @param {*} res
 * @param {*} next
 *
 */
export async function syncRefreshUser(req, res, next) {
  let user;
  const { userId } = req.body;
  let userToreturn;
  try {
    console.log('User refresh');
    if (!userId) {
      throw new Error('User is not authorized');
    }

    user = await UserProfileModel.findById(userId).exec();

    user.utime = new Date();

    if (!user) {
      throw new Error('User is not authorized');
    }
    // user.updatedAt = new Date;
    userToreturn = returnMyUserObject(user);
    // await user.save();
    await new Promise((resolve, reject) => {
      UserProfileModel.update({ _id: user._id }, user, (err, raw) => {
        if (err) {
          return reject(err);
        }
        return resolve(raw);
      });
    });
    console.log(userToreturn);
    return res.json({ status: 200, user: userToreturn });
  } catch (error) {
    console.log(error);
    return res.json({ status: 400 });
  }
}
