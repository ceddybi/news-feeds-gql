'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getRelatedUploads = exports.UploadFileModel = exports.UploadsSchema = undefined;

// const NewsPubFilterArticlesModel = mongoose.model('pubtop5', NewsPubFilterArticlsSchema);
let getRelatedUploads = exports.getRelatedUploads = (() => {
  var _ref = _asyncToGenerator(function* (id) {
    let uploads = [];
    try {
      uploads = yield UploadFileModel.find({
        related: {
          $elemMatch: { ref: id }
        }
      }).exec();

      return uploads;
    } catch (error) {
      console.log(error);
      return [];
    }
  });

  return function getRelatedUploads(_x) {
    return _ref.apply(this, arguments);
  };
})();

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            * Created by vuga on 9/22/17.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            */


const UploadsSchema = exports.UploadsSchema = new _mongoose2.default.Schema({
  name: { type: String, default: '' },
  hash: { type: String, default: '' },
  ext: { type: String, default: '' },
  mime: { type: String, default: '' },
  size: { type: String, default: '' },
  url: { type: String, default: '' },
  provider: { type: String, default: '' },
  related: [{
    ref: { type: _mongoose2.default.Schema.Types.ObjectId },
    kind: { type: String, default: '' },
    field: { type: String, default: '' }
  }]
}, {
  timestamps: true,
  collection: 'upload_file'
});

const UploadFileModel = exports.UploadFileModel = _mongoose2.default.model('Upload_file', UploadsSchema);