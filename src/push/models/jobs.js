import mongoose, { Schema } from 'mongoose';
import composeWithMongoose from 'graphql-compose-mongoose';
import composeWithRelay from 'graphql-compose-relay';

const JobSchema = new Schema(
  {
    name: { type: String, default: '' },
    timezone: { type: String, default: '' },
    utime: { type: String, default: Date.now },

    notificationId: { type: Schema.Types.ObjectId, ref: 'Notification' }, // Notification
    cronJobId: { type: Schema.Types.ObjectId, ref: 'CronJob' }, // Notification

    // status
    status: { type: String, default: '' },
  },

  {
    collection: 'jobs',
    timestamps: true,
  }
);

export const JobModel = mongoose.model('Job', JobSchema);
export const JobTC = composeWithMongoose(JobModel);
