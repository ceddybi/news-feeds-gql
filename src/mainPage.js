/* @flow */

import qs from 'querystring';

const examplesMeta = [];

export function addToMainPage(example: any) {
  examplesMeta.push(example);
}

export function mainPage() {
  return `
    <html>
      <head>
        <title>GraphQL-Compose examples</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
      </head>
      <body>
        <div class="container">
          <h1>GraphQL-Compose examples</h1>
          
          <br /><br /><br />
          <h3>Source code:</h3>
          <a href="https://github.com/nodkz/graphql-compose-examples" target="_blank">https://github.com/nodkz/graphql-compose-examples</a>

          <h3>Used packages:</h3>
          <a href="https://github.com/nodkz/graphql-compose" target="_blank">https://github.com/nodkz/graphql-compose</a>
          <br/>
          <a href="https://github.com/nodkz/graphql-compose-mongoose" target="_blank">https://github.com/nodkz/graphql-compose-mongoose</a>
          <br/>
          <a href="https://github.com/nodkz/graphql-compose-relay" target="_blank">https://github.com/nodkz/graphql-compose-relay</a>
          <br/>
          <a href="https://github.com/nodkz/graphql-compose-connection" target="_blank">https://github.com/nodkz/graphql-compose-connection</a>
          <br/>
          <a href="https://github.com/nodkz/graphql-compose-elasticsearch" target="_blank">https://github.com/nodkz/graphql-compose-elasticsearch</a>
          <br/>
          <a href="https://github.com/graphql-compose/graphql-compose-aws" target="_blank">https://github.com/graphql-compose/graphql-compose-aws</a>
        </div>

        <br /><br /><br /><br /><br />
      </body>
    </html>
  `;
}
