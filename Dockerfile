FROM mhart/alpine-node:8.11.2 AS buildVIO
WORKDIR /srv
ADD . .
RUN apk --no-cache add --virtual native-deps \
  g++ gcc libgcc libstdc++ linux-headers make python && \
  npm install --quiet node-gyp -g &&\
  npm install --quiet && \
  apk del native-deps
RUN npm run build
ENV MONGODB_URI=mongodb://vuga-admin:thebiggerthebill@vugaio-shard-00-00-jez7h.gcp.mongodb.net:27017,vugaio-shard-00-01-jez7h.gcp.mongodb.net:27017,vugaio-shard-00-02-jez7h.gcp.mongodb.net:27017/rtv?ssl=true&replicaSet=VugaIO-shard-0&authSource=admin
EXPOSE 3000
CMD node build/index.js