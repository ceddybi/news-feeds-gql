import mongoose from 'mongoose';
import _, { size } from 'lodash';

const UserDevice = mongoose.model('UserDevice');
const TvModel = mongoose.model('Tv');
const RadioModel = mongoose.model('Radio');

const http = require('https');

const keyAndroid = 'AIzaSyAnrVE3xfJceR2CbEGzsKNrBBaOF1_h5cU';
const keyIos = 'AIzaSyDIJjVNYHoMgk1TFM8OmQtmyHMruFsMmys';

let count = 0;

export async function AsyncPush({ ids, push, clientType }) {
  // const pus = push;
  // var r = res;
  const arrSize = size(ids); // number of groups
  console.log(`Ids chunks = ${arrSize}`);

  let pushNow;
  // If zero return now
  if (arrSize <= 0) {
    return { done: true };
  }

  const toBeUsed = ids[0];
  const d = {
    ids: toBeUsed,
    push,
    clientType,
  };
  console.log(`Group # ${count}`);

  try {
    pushNow = await pushIt(d);

    if (pushNow) {
      ids.splice(0, 1);
      count += 1;

      if (size(ids) > 0) {
        // We have more pushes so let's continue looping
        console.log(`Next is ${count}`);
        return await AsyncPush({ ids, push, clientType}); // run it again
      }
      console.log(`Finished sending push for ${clientType}`);
      return { done: true };
    }
    return { done: true };
  } catch (error) {
    console.log(error.message);
    return { done: false };
  }
}

export function pushIt({ ids, push, clientType }) {
  let respo;
  const key = clientType === 'ios' ? keyIos : keyAndroid;

  const payload = JSON.stringify({
    registration_ids: ids, // required
    collapse_key: push.collapse_key || 'your_collapse_key',
    notification: {
      title: push.title,
      body: push.message,
      sound: push.sound,
      click_action: push.click_action,
      icon: 'ic_l',
      color: '#ffffff',
    },
    data: push.data,
  });

  const headers = {
    Host: 'fcm.googleapis.com',
    Authorization: `key=${key}`,
    'Content-Type': 'application/json',
    'Content-Length': Buffer.byteLength(payload),
  };

  const post_options = {
    host: 'fcm.googleapis.com',
    port: 443,
    path: '/fcm/send',
    method: 'POST',
    headers,
  };

  return new Promise((resolve, reject) => {
    const post_req = http.request(post_options, response => {
      response.setEncoding('utf8');

      response.on('data', namedata => {
        respo = String(namedata);
      });
      response.on('end', () => {
        console.log(respo);
        resolve(respo);
      });

      response.on('error', err => {
        // respo.error = err;
        console.log(`On error ${err}`);
        reject(err);
      });
    });

    post_req.on('error', e => {
      // respo.error = e;
      // reject(e);
      console.log(`On error ${e}`);
      reject(e);
    });

    post_req.write(payload);
    post_req.end();
  });
}

/**
 * For custom push
 * */
export async function sendCustomPush(query, pushObject) {
  let resu;

  let d;
  let ac;
  let push;
  let chk;
  let ids;

  const { title, msg, sound, click_action, data } = pushObject;

  try {
    resu = await UserDevice.find(query)
      .lean()
      .exec();
    console.log(`User devices = ${_.size(resu)}`);
    // console.log("Req typo = " + typo);
    ids = _.map(resu, 'info.pushtoken');
    console.log(`IDs = ${_.size(ids)}`);
    chk = _.chunk(ids, 1000);
    console.log(`Chunks = ${_.size(chk)}`);
    // push = {};
    // ac = req.body.typo.toUpperCase();
    push = {
      title,
      message: msg,
      sound: sound || 'vuga_zing',
      click_action: click_action || 'APP',
      collapse_key: title,
      data,
    };

    console.log('Push ================ ');
    console.log(push);
    LoopArrayGroups(chk, push);
  } catch (error) {
    console.log(error);
  }
}

export async function getDataTypo(d) {
  const typo = d.typo;
  const id = d.typoId;
  let tv;
  let radio;
  console.log(`getDataTypo = ${typo} ${id}`);

  switch (typo) {
    case 'tv':
      tv = await TvModel.findById(id).exec();
      if (tv) {
        return {
          typo: 'tv',
          id: tv._id,
          link: tv.link,
          image_path: tv.image_path,
          title: tv.title,
          countryCode: tv.countryCode,
          isWebView: tv.isWebView,
          // obj: tv,
        };
      }
      return null;

    case 'radio':
      radio = await RadioModel.findById(id).exec();
      if (radio) {
        return {
          typo: 'radio',
          id: radio._id,
          link: radio.link,
          image_path: radio.image_path,
          title: radio.title,
          countryCode: radio.countryCode,
          // obj: radio,
        };
      }
      return null;

    default:
      // const f = ;
      return {
        id,
        typo,
      };
  }
}
