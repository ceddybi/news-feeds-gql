import { UserProfileModel } from '../models/user-profile';
import { ConvoModel } from '../../chat/models/convos';
import { get, find, cloneDeep } from 'lodash';

/**
 *
 * @param {*} param0
 * type = add, see/read
 */
export async function addNremoveInbox({ userId, type, convoId }) {
  let user;
  let convo;
  try {
    user = await UserProfileModel.findById(userId);
    if (!user) {
      throw { message: 'User not found for adding counts to inbox' };
    }

    convo = await ConvoModel.findById(convoId);
    if (!convo) {
      throw { message: 'Conversation not found for adding counts to inbox' };
    }

    if (type === 'add') {
      user.inbox.counts += 1;
    } else {
      // If it's below 0 or equal to zero
      if (user.inbox.counts <= 0) {
        user.inbox.counts = 0;
      } else {
        user.inbox.counts -= 1;
      }
    }

    await user.save();
    console.log(`Added users counter ${user.inbox}`);

    // If we have convoId add it to the convoUser

    convo.users = convo.users.map(convoUser => {
      if (convoUser.userId === userId) {
        if (type === 'add') {
          convoUser.counts += 1;
        } else {
          // If it's below 0 or equal to zero
          if (convoUser.counts <= 0) {
            convoUser.counts = 0;
          } else {
            convoUser.counts -= 1;
          }
        }
        console.log(`Added users counter in convo Object ${convoUser}`);
        // return convoUser;
      }
      return convoUser;
    });

    await convo.save();

    return { done: true };
  } catch (error) {
    console.log(error);
    return { done: false };
  }
}

/** Utility to add chat counts to the chat box  */
async function addCountsTochatBox({ user, convoId, type }) {

  console.log('User chat box before ', user.chatbox);
  try {
    let chatbox = user.chatbox;
    let currentConvo = find(chatbox, { convoId });
    console.log('CurrentConvo', currentConvo);
    console.log('UserChatBox', user.chatbox);

    // If we have the convo in the user object
    if (type === 'add') {
      // this chatbox doesn't exist
      if (!currentConvo) {
        currentConvo = {
          convoId,
          counts: 1,
          utime: new Date(),
        };
        chatbox.push(currentConvo);
      } else {
        // update the exsting one with an added count
        currentConvo.utime = new Date(); // Updated time
        currentConvo.counts += 1;

        // Update the chatbox
        chatbox = chatbox.map(ch => {
          if (ch.convoId === convoId) {
            return currentConvo;
          }
          return ch;
        });
      }
    } else {
      // Remove
      if (currentConvo) {
        if (currentConvo.counts <= 0) {
          // Already null remove it
          chatbox.filter(cb => cb.convoId !== convoId);
        } else {
          // Only if it great than 0
          currentConvo.counts -= 1;
          // Update the user
          chatbox = chatbox.map(ch => {
            if (ch.convoId === convoId) {
              return currentConvo;
            }
            return ch;
          });
        }
      }
    }

    user.chatbox = chatbox;

    console.log('User chat box after ', user.chatbox);
    await new Promise((res, rej) =>
      UserProfileModel.update({ _id: user._id }, user, { upsert: true }, (err, raw) => {
        if (!err) {
          return res(raw);
        }
        return rej(err);
      })
    );
    return user;
  } catch (er) {
    console.log(er);
    return null;
  }
}
/**
 *
 * @param {*} param0
 * type = add, see/read
 */
export async function chatboxCounter({ userId, type, convoId }) {
  let user;
  let convo;
  let updated;
  try {
    user = await UserProfileModel.findById(userId);
    if (!user) {
      throw { message: 'User not found for adding counts to inbox' };
    }

    convo = await ConvoModel.findById(convoId);
    if (!convo) {
      throw { message: 'Conversation not found for adding counts to inbox' };
    }

    user = await addCountsTochatBox({ userId, convoId, user, type });
    if (!user) {
      throw { message: 'User failed to update chatbox' };
    }
    console.log(`-------------------CHATBOX ${user.chatbox}`);

    // If we have convoId add it to the convoUser
    convo.users = convo.users.map(convoUser => {
      if (convoUser.userId === userId) {
        if (type === 'add') {
          convoUser.counts += 1;
        } else {
          // Remove
          // Just nullfy this
          convoUser.counts = 0;
        }
        // console.log(`Added users counter in convo Object ${convoUser}`);
        // return convoUser;
      }
      return convoUser;
    });

    await convo.save();

    return { done: true };
  } catch (error) {
    console.log(error);
    return { done: false };
  }
}
