'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.saveUniversalFile = undefined;

/** Save any file to the cloud s3
@param  file_path , file_name, file_exe, file_file_type, aws_path/folder_&_name
* */

let saveUniversalFile = exports.saveUniversalFile = (() => {
  var _ref = _asyncToGenerator(function* ({ filepath, mime, fileName }) {
    let fileInf;
    let s3Object;
    let url;

    try {
      fileInf = yield new Promise(function (resolve, reject) {
        _fs2.default.stat(filepath, function (err, info) {
          if (err) {
            return reject(err);
          }
          return resolve(info);
        });
      });

      const bodyStrea = _fs2.default.createReadStream(filepath);

      const awsPath = `u-bin/${fileName}`;

      console.log('Uploading to AWS', { filepath, mime, fileName });
      const param = {
        Bucket: 'vuga-01',
        Key: awsPath,
        Body: bodyStrea,
        ACL: 'public-read',
        ContentEncoding: 'base64',
        ContentType: mime,
        ContentLength: fileInf.size
      };
      s3Object = yield new Promise(function (resolve, reject) {
        s3.putObject(param, function (err, data) {
          if (err) {
            return reject(err);
          }
          console.log(data);
          return resolve(data);
        });
      });

      url = `${_config.AWS_STORAGE_URL}${awsPath}`;
      console.log('Done uploading to s3', url);
      return url;
    } catch (error) {
      console.log(error);
      return null;
    }
  });

  return function saveUniversalFile(_x) {
    return _ref.apply(this, arguments);
  };
})();

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _awsSdk = require('aws-sdk');

var _awsSdk2 = _interopRequireDefault(_awsSdk);

var _config = require('../../config');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; } // var mkdirp = require('mkdirp');

// import { AwsConfig } from '../../utils/config';

_awsSdk2.default.config = require('./AwsConfig');

const s3 = new _awsSdk2.default.S3();