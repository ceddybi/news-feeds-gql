'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ChatMsgRecord = exports.ChatMsgTC = exports.ChatMsgModel = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; /**
                                                                                                                                                                                                                                                                   * Created by vuga on 9/22/17.
                                                                                                                                                                                                                                                                   */


var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _graphqlComposeMongoose = require('graphql-compose-mongoose');

var _graphqlComposeMongoose2 = _interopRequireDefault(_graphqlComposeMongoose);

var _graphqlComposeRelay = require('graphql-compose-relay');

var _graphqlComposeRelay2 = _interopRequireDefault(_graphqlComposeRelay);

var _graphqlCompose = require('graphql-compose');

var _graphql = require('graphql');

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _dataTypes = require('../../utils/data-types');

var _userProfile = require('../../user/models/user-profile');

var _convos = require('./convos');

var _gqlThreads = require('../../utils/gql-threads');

var _pushUtils = require('../../push/helpers/push-utils');

var _userProfile2 = require('../../user/effects/user.profile.effects');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

const ChatMsgSchema = new _mongoose2.default.Schema(_extends({}, _dataTypes.dataAvailability, {

  // User sending the message
  userId: { type: String, default: '' },

  // Conversation ID
  convoId: { type: String, default: '' },
  sent: { type: Boolean, default: true },
  seen: { type: Boolean, default: true },

  // Text content
  message: {
    text: { type: String, default: '' },

    // Number of times it has been edited
    edits: [{
      text: { type: String, default: '' },
      utime: { type: String, default: '' }
    }]
  },

  // Media attachments
  attachments: [{
    // type/subType
    mimeType: { type: String, default: '' },

    // Optional Name of this file
    name: { type: String, default: '' },

    // Optional caption of this file
    caption: { type: String, default: '' },

    // Url link to this data
    src: { type: String, default: '' }
  }]
}), {
  collection: 'chatmsgs',
  timestamps: true
});

const ChatMsgModel = exports.ChatMsgModel = _mongoose2.default.model('ChatMsg', ChatMsgSchema);
const ChatMsgTC = exports.ChatMsgTC = (0, _graphqlComposeRelay2.default)((0, _graphqlComposeMongoose2.default)(ChatMsgModel));

const ChatMsgRecord = exports.ChatMsgRecord = _graphqlCompose.TypeComposer.create({
  name: 'newChatMsg',
  fields: {
    recordId: {
      type: _graphql.GraphQLString,
      description: 'Created document ID'
    },
    record: {
      type: ChatMsgTC,
      description: 'Chat Item created'
    }
  }
});

// newChatMsg
ChatMsgTC.addResolver(new _graphqlCompose.Resolver({
  name: 'newChatMsg',
  type: ChatMsgRecord,
  kind: 'mutation',
  args: {
    userId: {
      type: _graphql.GraphQLString
    },
    convoId: {
      type: _graphql.GraphQLString
    },
    userTo: {
      type: _graphql.GraphQLString
    },
    message: {
      type: _graphql.GraphQLString
    }
  },
  resolve: (() => {
    var _ref = _asyncToGenerator(function* ({ source, args, context, info }) {
      const userId = args.userId,
            userTo = args.userTo,
            message = args.message,
            convoId = args.convoId;

      let userSender;
      let userReceiver;
      let convo;
      let chatMsg;
      console.log(args);
      try {
        // Must Have convoId,
        // Else throw
        convo = yield _convos.ConvoModel.findById(convoId).exec();

        if (!convo) {
          throw new Error('Conversation not found');
        }
        // Check if users exist
        userSender = yield _userProfile.UserProfileModel.findById(userId).exec();
        userReceiver = yield _userProfile.UserProfileModel.findById(userTo).exec();

        console.log(`Message from ${userSender.info.firstname}`);
        console.log(`Message to ${userReceiver.info.firstname}`);
        // create convo if not exists

        // Conversation not found
        /** if (!convo) {
          throw new Error('Conversation not found');
        }* */

        chatMsg = new ChatMsgModel({
          convoId: convo._id,
          userId,
          message: {
            text: message
          }
        });

        // Save convo
        yield chatMsg.save();

        // TODO save chat counts
        // Save the last message
        convo.lastChatId = chatMsg._id;

        // Activate the conversation.
        convo.isActivated = true;

        // Update utime now
        convo.utime = new Date();
        yield convo.save();

        console.log('Saved the message');

        /** Message Effects here no need to await */
        const userObjects = [];

        // Send new push to receiver
        (0, _pushUtils.pushNewChatMessage)({
          userId,
          userTo,
          convoId,
          message
        });
        // Add counter to receiver
        (0, _userProfile2.addNremoveInbox)({ convoId: convo._id, userId: userTo, type: 'add' });

        (0, _userProfile2.chatboxCounter)({ userId: userTo, type: 'add', convoId: convo._id });

        return { recordId: chatMsg._id, record: chatMsg };
      } catch (error) {
        console.log(error);

        // Return empty array
        return convo;
      }
    });

    return function resolve(_x) {
      return _ref.apply(this, arguments);
    };
  })()
}));

// Connection by convoId
ChatMsgTC.setResolver('connection', ChatMsgTC.getResolver('connection').addFilterArg({
  name: 'byConvoId',
  type: `input byConvoId {
        convoId: String!
      }`,
  query: (rawQuery, value, resolveParams) => {
    // eslint-disable-line
    console.log(value);
    // console.log(resolveParams);
    // TODO GOT the context
    const convoId = value.convoId;

    rawQuery.convoId = convoId;
  }
}));

// Chat threads, by convoId = intentId
const ChatThreadsResolver = (0, _gqlThreads.ThreadResolver)({
  ModelTC: ChatMsgTC,
  extraArgs: {
    convoId: _graphql.GraphQLString,
    userId: _graphql.GraphQLString // UserId id to be used to mark read!
  },
  extraQuery: ({ source, args, context, info }) => {
    const convoId = args.convoId,
          userId = args.userId;
    // Remove seen messages
    // addNremoveInbox({ userId, convoId, type: 'remove' });

    return {
      convoId
    };
  },
  Model: ChatMsgModel
});
ChatMsgTC.addResolver(ChatThreadsResolver);