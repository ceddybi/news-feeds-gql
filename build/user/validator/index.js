'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _joi = require('joi');

var _joi2 = _interopRequireDefault(_joi);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const device = {
  unique: _joi2.default.string().required(),
  clientType: _joi2.default.string().required(),
  pushtoken: _joi2.default.string(),
  osid: _joi2.default.string(),
  appversion: _joi2.default.string()
};

exports.default = {
  // POST /user/auth/login
  login: {
    body: {
      username: _joi2.default.string().required(),
      password: _joi2.default.string().required(),
      device
    }
  },

  // POST /user/auth/login
  loginByPhone: {
    body: {
      phone: _joi2.default.string().required(),
      device
    }
  },

  checkUserByPhone: {
    body: {
      phone: _joi2.default.string().required()
    }
  },

  profileImage: {
    query: {
      id: _joi2.default.string().required()
    }
  },

  refreshToken: {
    body: {
      unique: _joi2.default.string().required(),
      refreshToken: _joi2.default.string().required()
    }
  },
  deviceSync: {
    body: { device }
  },
  userRefresh: {
    body: {
      userId: _joi2.default.string().required()
    }
  },
  checkPhoneEmail: {
    body: {
      email: _joi2.default.string().required(),
      phone: _joi2.default.string().required()
    }
  },
  checkUsername: {
    body: {
      username: _joi2.default.string().required()
    }
  },
  resetPasswordRequest: {
    body: {
      phone: _joi2.default.string().required()
    }
  },
  resetPassword: {
    body: {
      password: _joi2.default.string().required(),
      userId: _joi2.default.string().required()
    }
  },
  changePassword: {
    body: {
      password: _joi2.default.string().required(),
      userId: _joi2.default.string().required()
    }
  },
  register: {
    body: {
      lastname: _joi2.default.string().required(),
      firstname: _joi2.default.string().required(),
      username: _joi2.default.string().required(),
      sex: _joi2.default.string().required(),
      email: _joi2.default.string().required(),
      phone: _joi2.default.string().required(),
      password: _joi2.default.string().required(),
      device
    }
  },
  updateProfilePhoto: {
    body: {
      image_path: _joi2.default.string().required(),
      nickname: _joi2.default.string().required()
    }
  },
  updateProfile: {
    body: {
      userId: _joi2.default.string().required(),
      info: {
        lastname: _joi2.default.string(),
        firstname: _joi2.default.string(),
        username: _joi2.default.string()
        // nickname: Joi.string(),
        // address: Joi.string(),
        // bio: Joi.string(),
      }
    }
  },
  updatePassword: {
    body: {
      oldPassword: _joi2.default.string().required(),
      newPassword: _joi2.default.string().required()
    }
  },

  base64Image: {
    body: {
      imageBinary: _joi2.default.string().required(),
      imageType: _joi2.default.string().required(),
      userId: _joi2.default.string().required(),
      fileName: _joi2.default.string().required()
    }
  }
};