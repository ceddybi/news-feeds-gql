// var mkdirp = require('mkdirp');

import fs from 'fs';
import path from 'path';
import AWS from 'aws-sdk';
import { AWS_STORAGE_URL } from '../../config';
// import { AwsConfig } from '../../utils/config';

AWS.config = require('./AwsConfig');

const s3 = new AWS.S3();

/** Save any file to the cloud s3
@param  file_path , file_name, file_exe, file_file_type, aws_path/folder_&_name
* */

export async function saveUniversalFile({ filepath, mime, fileName }) {
  let fileInf;
  let s3Object;
  let url;

  try {
    fileInf = await new Promise((resolve, reject) => {
      fs.stat(filepath, (err, info) => {
        if (err) {
          return reject(err);
        }
        return resolve(info);
      });
    });

    const bodyStrea = fs.createReadStream(filepath);

    const awsPath = `u-bin/${fileName}`;

    console.log('Uploading to AWS', { filepath, mime, fileName });
    const param = {
      Bucket: 'vuga-01',
      Key: awsPath,
      Body: bodyStrea,
      ACL: 'public-read',
      ContentEncoding: 'base64',
      ContentType: mime,
      ContentLength: fileInf.size,
    };
    s3Object = await new Promise((resolve, reject) => {
      s3.putObject(param, (err, data) => {
        if (err) {
          return reject(err);
        }
        console.log(data);
        return resolve(data);
      });
    });

    url = `${AWS_STORAGE_URL}${awsPath}`;
    console.log('Done uploading to s3', url);
    return url;
  } catch (error) {
    console.log(error);
    return null;
  }
}
