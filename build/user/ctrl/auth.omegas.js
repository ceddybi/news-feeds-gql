'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

/** STATIC UTILS for getting region info */
let getRegions = exports.getRegions = (() => {
  var _ref = _asyncToGenerator(function* (req, res, next) {
    console.log('-----------------------------Regions');
    return res.json({
      status: 200,
      regions: {
        articles: [
        // { name: 'Global', value: 'GLOBAL' },
        { name: 'Rwanda', value: 'RW' }, { name: 'Kenya', value: 'KE' }, { name: 'Uganda', value: 'UG' }],
        tv: [],
        radios: []
      }
    });
  });

  return function getRegions(_x, _x2, _x3) {
    return _ref.apply(this, arguments);
  };
})();

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }